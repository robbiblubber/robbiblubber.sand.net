﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Robbiblubber.Sand.Model;



namespace Robbiblubber.Sand.Test
{
    /// <summary>This class represents a course.</summary>
    public class Course: Producible, IProducible
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets the class name.</summary>
        public virtual string Name
        {
            get { return _GetValue<string>("Name"); }
            set { _SetValue("Name", value); }
        }


        /// <summary>Gets or sets the class teacher.</summary>
        public virtual Teacher Teacher
        {
            get { return Factory.Produce<Teacher>(_GetValue<string>("Teacher")); }
            set { _SetValue("Teacher", value.ID); }
        }


        /// <summary>Gets a list of students in this class.</summary>
        public virtual IMutableProducibleList<Student> Students
        {
            get { return _GetRelation<Student>("Students"); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return ("course: " + Name);
        }
    }
}
