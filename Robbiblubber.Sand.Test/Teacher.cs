﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robbiblubber.Sand.Model;

namespace Robbiblubber.Sand.Test
{
    /// <summary>This class represents a teacher.</summary>
    public class Teacher: Person, IProducible
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets the teacher's hire date.</summary>
        public DateTime HireDate
        {
            get { return _GetValue<DateTime>("HireDate"); }
            set { _SetValue("HireDate", value); }
        }


        /// <summary>Gets or sets the teacher's salary.</summary>
        public int Salary
        {
            get { return _GetValue<int>("Salary"); }
            set { _SetValue("Salary", value); }
        }


        public IMutableProducibleList<Course> Courses
        {
            get { return _GetRelation<Course>("Courses"); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return (Name + ", " + FirstName + "; " + (Gender == Gender.FEMALE ? "f" : "m") + "; born: " + BirthDate.ToString("yyyy-mm-dd") + "; hired: " + HireDate.ToString("yyyy-mm-dd") + "; salary: " + Salary.ToString());
        }
    }
}
