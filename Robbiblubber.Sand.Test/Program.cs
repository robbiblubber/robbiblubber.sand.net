﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Robbiblubber.Sand.Journalization;
using Robbiblubber.Sand.Security;
using Robbiblubber.Util.Library;

namespace Robbiblubber.Sand.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Framework.Initialize();

            Context.Current = Context.Create("root", "test");

            IProducibleList<Student> res = Factory.All<Student>(m => m.Gender == Gender.FEMALE);
            res.OrderBy(m => m.Name);

            foreach(Student i in res.OrderBy(m => m.Name))
            {
                Console.WriteLine(i.ToString());
            }

            /*
            PrintStudentCourses();
            Console.WriteLine();

            PrintCourseStudents();
            Console.WriteLine();
            */

            Console.ReadLine();
        }


        static void FillCourse(Course c)
        {

        }


        static void PrintStudentCourses()
        {
            foreach(Student i in Factory.All<Student>())
            {
                Console.WriteLine(i.FirstName + " " + i.Name);

                foreach(Course j in i.Courses)
                {
                    Console.WriteLine("  -> " + j.Name + "(" + j.Teacher.FirstName + " " + j.Teacher.Name + ")");
                }
            }
        }


        static void PrintCourseStudents()
        {
            foreach(Course j in Factory.All<Course>())
            {
                Console.WriteLine(j.Name + "(" + j.Teacher.FirstName + " " + j.Teacher.Name + ")");

                foreach(Student i in j.Students)
                {
                    Console.WriteLine("  -> " + i.FirstName + " " + i.Name);
                }
            }
        }

        static void CreateCourses()
        {
            IProducibleList<Teacher> teachers = Factory.All<Teacher>();

            for(int i = 1; i < 21; i++)
            {
                Course c = new Course();

                c.Name = "Special " + i.ToString();
                c.Teacher = teachers.Random();
                c.Save();
            }
        }


        static void CreateClasses()
        {
            IProducibleList<Teacher> teachers = Factory.All<Teacher>();

            for(int i = 2; i < 12; i++)
            {
                Class c = new Class();

                c.Name = "Class " + i.ToString();
                c.Teacher = teachers.Random();
                c.Save();
            }
        }


        static void CreateStudents()
        {
            string[] girls = Read(@"C:\home\projects\Robbiblubber.Sand.NET\test\girls.txt");
            string[] boys  = Read(@"C:\home\projects\Robbiblubber.Sand.NET\test\boys.txt");
            string[] names = Read(@"C:\home\projects\Robbiblubber.Sand.NET\test\names.txt");

            Random rnd = new Random();

            IProducibleList<Class> classes = Factory.All<Class>();

            for(int i = 0; i < 60; i++)
            {
                Student s = new Student();
                
                if((rnd.Next() % 2) == 0)
                {
                    s.Gender = Gender.MALE;
                    s.FirstName = boys[rnd.Next(0, boys.Length -1)];
                }
                else
                {
                    s.Gender = Gender.FEMALE;
                    s.FirstName = girls[rnd.Next(0, girls.Length -1)];
                }
                s.Name = names[rnd.Next(0, names.Length -1)];

                while(true)
                {
                    try
                    {
                        s.BirthDate = new DateTime(rnd.Next(1995, 2010), rnd.Next(1, 12), rnd.Next(1, 31));
                        break;
                    }
                    catch(Exception) {}
                }
                s.Grade = rnd.Next(1, 5);
                s.Class = classes.Random();

                s.Save();
            }
        }


        static string[] Read(string file)
        {
            return File.ReadAllText(file).Replace("\r\n", "\n").Split('\n');
        }
    }
}
