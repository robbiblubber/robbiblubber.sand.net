﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robbiblubber.Sand.Test
{
    /// <summary>This class represents a car.</summary>
    public class Car: Vehicle
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return (Color + " car, " + Power.ToString() + " HP.");
        }
    }
}
