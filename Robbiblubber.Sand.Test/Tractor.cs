﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robbiblubber.Sand.Test
{
    public class Tractor: Vehicle
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the number of scoops on this tractor.</summary>
        public int Scoops
        {
            get { return _GetValue<int>("Scoops"); }
            set { _SetValue("Scoops", value); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return (Color + " tractor, " + Scoops.ToString() + " scoops.");
        }
    }
}
