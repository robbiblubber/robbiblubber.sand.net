﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Robbiblubber.Sand.Test
{
    public abstract class Vehicle: Producible
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the vehicle power.</summary>
        public int Power
        {
            get { return _GetValue<int>("Power"); }
            set { _SetValue("Power", value); }
        }


        /// <summary>Gets or sets the number of seats for the vehicle.</summary>
        public int Seats
        {
            get { return _GetValue<int>("Seats"); }
            set { _SetValue("Seats", value); }
        }


        /// <summary>Gets or sets the vehicle color.</summary>
        public string Color
        {
            get { return _GetValue<string>("Color"); }
            set { _SetValue("Color", value); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return (Color + " vehicle, " + Power.ToString() + " HP.");
        }
    }
}
