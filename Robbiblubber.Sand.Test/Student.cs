﻿using System;



namespace Robbiblubber.Sand.Test
{
    /// <summary>This class represents a student.</summary>
    public class Student: Person, IProducible
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets the student's grade.</summary>
        public int Grade
        {
            get { return _GetValue<int>("Grade"); }
            set { _SetValue("Grade", value); }
        }


        /// <summary>Gets the student's courses</summary>
        public IMutableProducibleList<Course> Courses
        {
            get { return _GetRelation<Course>("Courses"); }
        }


        /// <summary>Gets the student's class.</summary>
        public Class Class
        {
            get { return Factory.Produce<Class>(_GetValue<string>("Class")); }
            set { _SetValue("Class", value.ID); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return (Name + ", " + FirstName + "; " + (Gender == Gender.FEMALE ? "f" : "m") + "; born: " + BirthDate.ToString("yyyy-mm-dd") + "; grade: " + Grade.ToString());
        }
    }
}
