﻿using System;

using Robbiblubber.Sand.Model;



namespace Robbiblubber.Sand.Test
{
    /// <summary>This class represents a person.</summary>
    public abstract class Person: Producible, IProducible
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the teacher's name.</summary>
        public virtual string Name
        {
            get { return _GetValue<string>("Name"); }
            set { _SetValue("Name", value); }
        }


        /// <summary>Gets or sets the teachers's first name.</summary>
        public virtual string FirstName
        {
            get { return _GetValue<string>("FirstName"); }
            set { _SetValue("FirstName", value); }
        }


        /// <summary>Gets or sets the teacher's gender.</summary>
        public virtual Gender Gender
        {
            get { return _GetValue<Gender>("Gender"); }
            set { _SetValue("Gender", value); }
        }


        /// <summary>Gets or sets the teacher's birth date.</summary>
        public virtual DateTime BirthDate
        {
            get { return _GetValue<DateTime>("BirthDate"); }
            set { _SetValue("BirthDate", value); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return (Name + ", " + FirstName + "; " + (Gender == Gender.FEMALE ? "f" : "m") + "; born: " + BirthDate.ToString("yyyy-mm-dd"));
        }
    }



    /// <summary>This enum defines a person's gender.</summary>
    public enum Gender: int
    {
        MALE = 0, FEMALE = 1
    }
}
