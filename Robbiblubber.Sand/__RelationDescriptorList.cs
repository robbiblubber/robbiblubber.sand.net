﻿using System;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements a relation descriptor list.</summary>
    public sealed class __RelationDescriptorList: __DescriptorList<__RelationDescriptor>
    {}
}
