﻿using System;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>Descriptors implement this interface.</summary>
    public interface __IDescriptor
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the descriptor key.</summary>
        /// <returns>Key.</returns>
        string GetKey();
    }
}
