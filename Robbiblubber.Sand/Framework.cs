﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robbiblubber.Sand.Journalization.Base;
using Robbiblubber.Sand.Model.Base;

namespace Robbiblubber.Sand
{
    /// <summary>This class provides basic framework functionalit.</summary>
    public static class Framework
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Initializes the framework.</summary>
        public static void Initialize()
        {
            if(__FrameworkManager.Instance is __IAttachable) { ((__IAttachable) __FrameworkManager.Instance).Attach(); }
            if(__Factory.Instance is __IAttachable) { ((__IAttachable) __Factory.Instance).Attach(); }
            if(__Cache.Instance is __IAttachable) { ((__IAttachable) __Cache.Instance).Attach(); }
            if(__JournalWriter.Instance is __IAttachable) { ((__IAttachable) __JournalWriter.Instance).Attach(); }
        }


        /// <summary>Terminates the framework.</summary>
        public static void Terminate()
        {
            if(__FrameworkManager.Instance is __IAttachable) { ((__IAttachable) __FrameworkManager.Instance).Detach(); }
            if(__Factory.Instance is __IAttachable) { ((__IAttachable) __Factory.Instance).Detach(); }
            if(__Cache.Instance is __IAttachable) { ((__IAttachable) __Cache.Instance).Detach(); }
            if(__JournalWriter.Instance is __IAttachable) { ((__IAttachable) __JournalWriter.Instance).Detach(); }
        }
    }
}
