﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Robbiblubber.Util.Library;


namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements a generic descriptor list.</summary>
    /// <typeparam name="T">Descriptor type.</typeparam>
    public abstract class __DescriptorList<T>: IEnumerable<T> where T: __IDescriptor
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>List items.</summary>
        protected Dictionary<string, T> _Items = new Dictionary<string, T>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the item with the specified key.</summary>
        /// <param name="key">Key.</param>
        /// <returns>Item.</returns>
        public virtual T this[string key]
        {
            get
            {
                if((typeof(T) == typeof(__EntityDescriptor)) && key.Contains('.'))
                {
                    key = key.Substring(0, key.IndexOf('.'));
                }
                
                return _Items[key];
            }
        }


        /// <summary>Gets the item with the specified index.</summary>
        /// <param name="n">Index.</param>
        /// <returns>Item.</returns>
        public virtual T this[int n]
        {
            get { return _Items.Values.ElementAt(n); }
        }


        /// <summary>Gets the number of items in this list.</summary>
        public virtual int Count
        {
            get { return _Items.Count; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an item to the list.</summary>
        /// <param name="item">Item.</param>
        public virtual void Add(T item)
        {
            _Items.Add(item.GetKey(), item);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<T>                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }
    }
}
