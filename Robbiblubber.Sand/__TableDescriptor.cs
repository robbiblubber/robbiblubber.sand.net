﻿using System;
using System.Collections.Generic;

using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements a table descriptor.</summary>
    public sealed class __TableDescriptor: __IDescriptor, ISQLTable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="entity">Entity.</param>
        /// <param name="tableName">Table name.</param>
        /// <param name="alias">Table alias.</param>
        /// <param name="pk">Primary key.</param>
        /// <param name="root">Root flag.</param>
        internal __TableDescriptor(__EntityDescriptor entity, string tableName, string alias, string pk, bool root)
        {
            Entity = entity;
            TableName = tableName;
            TableAlias = alias;
            PrimaryKey = pk;
            Root = root;

            Fields = new Dictionary<string, __FieldDescriptor>();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent entity for this table descriptor.</summary>
        public __EntityDescriptor Entity { get; private set; }
        

        /// <summary>Gets the table primary key.</summary>
        public string PrimaryKey { get; private set; }


        /// <summary>Gets a value indicating if the table is the root table for the entity.</summary>
        public bool Root { get; private set; }


        /// <summary>Gets the fields of this table.</summary>
        public Dictionary<string, __FieldDescriptor> Fields { get; private set; }


        /// <summary>Gets the table join condition.</summary>
        public string JoinCondition
        {
            get { return (Entity.Tables.RootTable.TableAlias + "." + Entity.Tables.RootTable.PrimaryKey + " = " + TableAlias + "." + PrimaryKey); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISQLTable                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the table name.</summary>
        public string TableName { get; private set; }


        /// <summary>Gets the table alias name.</summary>
        public string TableAlias { get; private set; }


        /// <summary>Gets the table expression.</summary>
        public string TableExpression
        {
            get { return (string.IsNullOrEmpty(TableAlias) ? TableName : (TableName + Entity.Provider.Parser.AsOperator + TableAlias)); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IDescriptor                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the descriptor key.</summary>
        /// <returns>Key.</returns>
        string __IDescriptor.GetKey()
        {
            return TableName;
        }
    }
}
