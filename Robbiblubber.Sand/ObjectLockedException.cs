﻿using System;

using Robbiblubber.Sand.Model;



namespace Robbiblubber.Sand.Exceptions
{
    /// <summary>This class implements an exception that occurs when an operation could not be performed on an locked object.</summary>
    public class ObjectLockedException: Exception
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="lock">Lock.</param>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">The exception that caused the exception.</param>
        public ObjectLockedException(Lock @lock, string message, Exception innerException): base(message, innerException)
        {
            Lock = @lock;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="lock">Lock.</param>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">The exception that caused the exception.</param>
        public ObjectLockedException(Lock @lock, string message) : this(@lock, message, null)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="lock">Lock.</param>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">The exception that caused the exception.</param>
        public ObjectLockedException(Lock @lock): this(@lock, "The object is locked.", null)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">The exception that caused the exception.</param>
        public ObjectLockedException(string message, Exception innerException): base(message, innerException)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        public ObjectLockedException(string message): base(message)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        public ObjectLockedException(): base("The object is locked.")
        {}


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the lock object.</summary>
        public Lock Lock
        {
            get; protected set;
        } = null;
    }
}
