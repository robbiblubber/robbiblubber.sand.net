﻿using System;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This enumeration defines superclass modes.</summary>
    public enum __ClassType
    {
        /// <summary>The class is a leaf class.</summary>
        LEAF = 0,
        /// <summary>The class is a base class with a table.</summary>
        MATERIAL = 1,
        /// <summary>The class is a base class without a table.</summary>
        VIRTUAL = 2,
        /// <summary>The class is a base class of classes that share a single table.</summary>
        INTEGRATED = 4
    }
}
