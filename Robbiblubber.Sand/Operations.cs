﻿using System;
using System.Collections;
using System.Collections.Generic;

using Robbiblubber.Sand.Model.Base;
using Robbiblubber.Sand.Security.Exceptions;



namespace Robbiblubber.Sand.Security
{
    /// <summary>This interface provides methods to determine privileges required to perform operations on an object.</summary>
    public sealed class Operations: IEnumerable<Op>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parent object.</summary>
        private IProducible _Parent;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parent">Parent object.</param>
        internal Operations(IProducible parent)
        {
            _Parent = parent;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets an array of operations defined for this object.</summary>
        public Op[] Ops
        {
            get { return ((__IFactoryTarget) _Parent).EntityDescriptor.OpDescriptor.DefinedOps; }
        }


        /// <summary>Gets the privilege required to perform a given operation on this object.</summary>
        /// <param name="op">Operation.</param>
        /// <returns>Privilege.</returns>
        public Privilege this[Op op]
        {
            get { return Requires(op); }
        }


        /// <summary>Gets if a given operation is allowed for a specific context.</summary>
        /// <param name="op">Operation.</param>
        /// <param name="context">Context.</param>
        /// <returns>Returns TRUE if the operation is allowed, otherwise returns FALSE.</returns>
        public bool this[Op op, Context context]
        {
            get { return Assess(op, context); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns the privilege required to perform a given operation on this object.</summary>
        /// <param name="op">Operation.</param>
        /// <returns>Privilege.</returns>
        public Privilege Requires(Op op)
        {
            return ((__IFactoryTarget) _Parent).EntityDescriptor.OpDescriptor.Requires(_Parent, op);
        }


        /// <summary>Returns if a given operation is allowed for a specific context.</summary>
        /// <param name="op">Operation.</param>
        /// <param name="context">Context.</param>
        /// <returns>Returns TRUE if the operation is allowed, otherwise returns FALSE.</returns>
        public bool Assess(Op op, Context context)
        {
            return context.Holds(Requires(op));
        }


        /// <summary>Returns if a given operation is allowed for a specific context.</summary>
        /// <param name="op">Operation.</param>
        /// <returns>Returns TRUE if the operation is allowed, otherwise returns FALSE.</returns>
        public bool Assess(Op op)
        {
            return Assess(op, Context.Current);
        }


        /// <summary>Checks if a given operation is allowed for a specific context.</summary>
        /// <param name="op">Operation.</param>
        /// <param name="context">Context.</param>
        /// <exception cref="InsufficientPrivilegesException">Thrown if the context lacks privileges for the desired operation.</exception>
        public void Probe(Op op, Context context)
        {
            Privilege p = Requires(op);

            if(!context.Holds(p))
            {
                throw new InsufficientPrivilegesException(p, _Parent, op);
            }
        }


        /// <summary>Checks if a given operation is allowed for a specific context.</summary>
        /// <param name="op">Operation.</param>
        /// <exception cref="InsufficientPrivilegesException">Thrown if the context lacks privileges for the desired operation.</exception>
        public void Probe(Op op)
        {
            Probe(op, Context.Current);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerator<Op>                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through a collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<Op> IEnumerable<Op>.GetEnumerator()
        {
            return ((__IFactoryTarget) _Parent).EntityDescriptor.OpDescriptor.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerator                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through a collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((__IFactoryTarget) _Parent).EntityDescriptor.OpDescriptor.GetEnumerator();
        }
    }
}
