﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>Factory list targets implement this interface.</summary>
    public interface __IFactoryListTarget
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a list of items as currently defined.</summary>
        IList<IProducible> Items { get; }


        /// <summary>Gets or sets a list of items as currently persistent.</summary>
        IList<IProducible> PersistentItems { get; set; }
        
        
        /// <summary>Gets or sets the list index.</summary>
        IDictionary<string, IProducible> Index { get; set; }


        /// <summary>Gets the relation descriptor.</summary>
        __RelationDescriptor Relation { get; }


        /// <summary>Gets the parent object.</summary>
        IProducible Parent { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Loads list items if needed.</summary>
        void Load();
    }
}
