﻿using System;



namespace Robbiblubber.Sand.Model.Base
{
    public static class __FieldAccessExtensions
    {
        /// <summary>Returns a constant string for a field access modifier.</summary>
        /// <param name="f">Field.</param>
        public static string ToConstant(this __FieldAccess f)
        {
            switch(f)
            {
                case __FieldAccess.READ: return "READ";
                case __FieldAccess.WRITE: return "WRITE";
                case __FieldAccess.READ_WRITE: return "READ_WRITE";
            }

            return "NONE";
        }


        /// <summary>Returns a field access modifier for a constant string.</summary>
        /// <param name="c">String constant.</param>
        public static __FieldAccess ToFieldAccess(this string c)
        {
            switch(c.ToUpper())
            {
                case "READ": case "R": return __FieldAccess.READ;
                case "WRITE": case "W": return __FieldAccess.WRITE;
                case "READ_WRITE": case "READWRITE": case "RW": case "": return __FieldAccess.READ_WRITE;
            }

            return __FieldAccess.NONE;
        }
    }
}
