﻿using System;
using System.Data;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements a relation descriptor that holds metadata for a relation.</summary>
    public abstract class __RelationDescriptor: __IDescriptor
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Target entity ID.</summary>
        protected string _TargetEntityID;

        /// <summary>Target entity.</summary>
        protected __EntityDescriptor _TargetEntity = null;

        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="entity">Parent entity.</param>
        /// <param name="section">ddp section.</param>
        public __RelationDescriptor(__EntityDescriptor entity, DdpSection section)
        {
            Entity = entity;
            Name = section.GetString("name");
            Editable = section.GetBoolean("editable");
            _TargetEntityID = section.GetString("target");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the relation field name.</summary>
        public string Name { get; protected set; }


        /// <summary>Gets if the relation is editable.</summary>
        public bool Editable { get; protected set; }


        /// <summary>Gets the parent entity for this relation.</summary>
        public __EntityDescriptor Entity { get; protected set; }


        /// <summary>Gets the target entity for this relation.</summary>
        public __EntityDescriptor TargetEntity
        {
            get
            {
                if(_TargetEntity == null) { _TargetEntity = __Factory.Instance.Entities[_TargetEntityID]; }
                return _TargetEntity;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a SQL query for this relation for a given producible object.</summary>
        /// <param name="p">Producible object.</param>
        /// <returns>SQL query.</returns>
        public abstract ISQLQuery CreateQuery(IProducible p);



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IDescriptor                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the descriptor key.</summary>
        /// <returns>Key.</returns>
        string __IDescriptor.GetKey()
        {
            return Name;
        }
    }
}
