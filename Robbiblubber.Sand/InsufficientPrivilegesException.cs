﻿using System;



namespace Robbiblubber.Sand.Security.Exceptions
{
    /// <summary>This class implements an exception that occurs when an operation is attempted in a context that doesn't hold the required privilege.</summary>
    public class InsufficientPrivilegesException: SecurityException
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">The exception that caused the exception.</param>
        public InsufficientPrivilegesException(string message, Exception innerException): base(message, innerException)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">The exception that caused the exception.</param>
        public InsufficientPrivilegesException(string message): base(message, null)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">The exception that caused the exception.</param>
        public InsufficientPrivilegesException(): base("Insufficient privileges.")
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="p">Privilege.</param>
        public InsufficientPrivilegesException(Privilege p): base("Insufficient privileges. Operation requires \"" + p.Name + "\" privilege.")
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="p">Privilege.</param>
        /// <param name="obj">Object.</param>
        /// <param name="op">Operation.</param>
        public InsufficientPrivilegesException(Privilege p, IProducible obj, Op op) : base("Insufficient privileges. Operation \"" + op.Description + "\" on object [" + obj.ID + "] requires \"" + p.Name + "\" privilege.")
        {}
    }
}
