﻿using System;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Sand.Model.Base;



namespace Robbiblubber.Sand.Journalization
{
    /// <summary>This class provides journalization settings.</summary>
    public sealed class Settings
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        internal Settings()
        {
            List<Journal.Severity> sev = new List<Journal.Severity>();
            List<Journal.Operation> ops = new List<Journal.Operation>();

            string[] data = __Repository.Instance.RepositoryFile.Sections["journal"].GetStringArray("severities", "ABOVE_NOTIFICATION");
            foreach(string i in data)
            {
                switch(i.ToUpper().Replace(" ", "_"))
                {
                    case "DEBUG": case "-1":
                        _Append(sev, Journal.Severity.DEBUG); break;
                    case "INFORMATION": case "0":
                        _Append(sev, Journal.Severity.INFORMATION); break;
                    case "NOTIFICATION": case "1":
                        _Append(sev, Journal.Severity.NOTIFICATION); break;
                    case "WARNING": case "2":
                        _Append(sev, Journal.Severity.WARNING); break;
                    case "ERROR": case "3":
                        _Append(sev, Journal.Severity.ERROR); break;
                    case "CRITICAL": case "4":
                        _Append(sev, Journal.Severity.CRITICAL); break;
                    case "ALERT": case "5":
                        _Append(sev, Journal.Severity.ALERT); break;
                    case "EMERGENCY": case "6":
                        _Append(sev, Journal.Severity.EMERGENCY); break;
                    case "ALL": case "-1_AND_ABOVE": case "DEBUG_AND_ABOVE":
                        _Append(sev, Journal.Severity.DEBUG, Journal.Severity.INFORMATION, Journal.Severity.NOTIFICATION, Journal.Severity.WARNING, Journal.Severity.ERROR, Journal.Severity.CRITICAL, Journal.Severity.ALERT, Journal.Severity.EMERGENCY); break;
                    case "ABOVE_-1": case "ABOVE_DEBUG": case "0_AND_ABOVE": case "INFORMATION_AND_ABOVE":
                        _Append(sev, Journal.Severity.INFORMATION, Journal.Severity.NOTIFICATION, Journal.Severity.WARNING, Journal.Severity.ERROR, Journal.Severity.CRITICAL, Journal.Severity.ALERT, Journal.Severity.EMERGENCY); break;
                    case "ABOVE_0": case "ABOVE_INFORMATION": case "1_AND_ABOVE": case "NOTIFICATION_AND_ABOVE":
                        _Append(sev, Journal.Severity.NOTIFICATION, Journal.Severity.WARNING, Journal.Severity.ERROR, Journal.Severity.CRITICAL, Journal.Severity.ALERT, Journal.Severity.EMERGENCY); break;
                    case "ABOVE_1": case "ABOVE_NOTIFICATION": case "2_AND_ABOVE": case "WARNING_AND_ABOVE":
                        _Append(sev, Journal.Severity.WARNING, Journal.Severity.ERROR, Journal.Severity.CRITICAL, Journal.Severity.ALERT, Journal.Severity.EMERGENCY); break;
                    case "ABOVE_2": case "ABOVE_WARNING": case "3_AND_ABOVE": case "ERROR_AND_ABOVE":
                        _Append(sev, Journal.Severity.ERROR, Journal.Severity.CRITICAL, Journal.Severity.ALERT, Journal.Severity.EMERGENCY); break;
                    case "ABOVE_3": case "ABOVE_ERROR": case "4_AND_ABOVE": case "CRITICAL_AND_ABOVE":
                        _Append(sev, Journal.Severity.CRITICAL, Journal.Severity.ALERT, Journal.Severity.EMERGENCY); break;
                    case "ABOVE_4": case "ABOVE_CRITICAL": case "5_AND_ABOVE": case "ALERT_AND_ABOVE":
                        _Append(sev, Journal.Severity.ALERT, Journal.Severity.EMERGENCY); break;
                    case "ABOVE_5": case "ABOVE_ALERT": case "6_AND_ABOVE": case "EMERGENCY_AND_ABOVE":
                        _Append(sev, Journal.Severity.EMERGENCY); break;
                }
            }
            Severities = sev.ToArray();

            data = __Repository.Instance.RepositoryFile.Sections["journal"].GetStringArray("operations", "ALL");
            foreach(string i in data)
            {

                if(i.ToUpper() == "ALL")
                {
                    foreach(Journal.Operation j in Enum.GetValues(typeof(Journal.Operation))) { _Append(ops, j); }
                }
                else if((i.ToUpper().Replace(" ", "_") == "ONLY_DEFINED") || (i.ToUpper() == "DEFINED"))
                {
                    foreach(Journal.Operation j in Enum.GetValues(typeof(Journal.Operation))) { if(j != Journal.Operation.UNDEFINED) _Append(ops, j); }
                }
                else foreach(string j in Enum.GetNames(typeof(Journal.Operation)))
                {
                    if(j.ToUpper() == i.ToUpper()) { _Append(ops, (Journal.Operation) Enum.Parse(typeof(Journal.Operation), j, true)); }
                }
            }
            Operations = ops.ToArray();

            Debug = Severities.Contains(Journal.Severity.DEBUG);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets an array of operations to include into the journal.</summary>
        public Journal.Operation[] Operations
        {
            get; private set;
        }


        /// <summary>Gets an array of operations to include into the journal.</summary>
        public Journal.Severity[] Severities
        {
            get; private set;
        }


        /// <summary>Gets if debug logging is activated.</summary>
        public bool Debug
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Appends severity levels to a list.</summary>
        /// <param name="list">List.</param>
        /// <param name="values">Severity levels.</param>
        private static void _Append(List<Journal.Severity> list, params Journal.Severity[] values)
        {
            foreach(Journal.Severity i in values)
            {
                if(!list.Contains(i)) { list.Add(i); }
            }
        }


        /// <summary>Appends operation flags to a list.</summary>
        /// <param name="list">List.</param>
        /// <param name="values">Operation flags.</param>
        private static void _Append(List<Journal.Operation> list, params Journal.Operation[] values)
        {
            foreach(Journal.Operation i in values)
            {
                if(!list.Contains(i)) { list.Add(i); }
            }
        }
    }
}
