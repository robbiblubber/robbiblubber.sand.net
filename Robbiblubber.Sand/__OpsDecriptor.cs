﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Sand.Security;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class stores information about operations on an entity.</summary>
    public sealed class __OpDecriptor: IEnumerable<Op>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Defined ops.</summary>
        private Op[] _DefinedOps = null;
        
        /// <summary>Privilege defined for certain operations on the parent entity.</summary>
        private Dictionary<Op, string> _OpPrivileges;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="entity">Entity descriptor.</param>
        /// <param name="section">ddp Section.</param>
        public __OpDecriptor(__EntityDescriptor entity)
        {
            Entity = entity;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Loads operation definitions from database.</summary>
        private void _Load()
        {
            if(_OpPrivileges == null)
            {
                _OpPrivileges = __Factory.Instance.LoadOps(Entity);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent entity.</summary>
        public __EntityDescriptor Entity
        {
            get; private set;
        }


        /// <summary>Gets the ops defined for this entity.</summary>
        public Op[] DefinedOps
        {
            get
            {
                _Load();
                if(_DefinedOps == null) { _DefinedOps = _OpPrivileges.Keys.ToArray(); }

                return _DefinedOps;
            }
        }


        /// <summary>Returns the privilege required for a given operation on a given object.</summary>
        /// <param name="p">Producible object.</param>
        /// <param name="op">Operation.</param>
        /// <returns>Privilege.</returns>
        /// <remarks>Returns NULL if no privilege is required for the operation.</remarks>
        public Privilege Requires(IProducible p, Op op)
        {
            _Load();
            
            if(_OpPrivileges.ContainsKey(op))
            {
                if(_OpPrivileges[op] == "*")
                {
                    return __Factory.Instance.GetObjectPrivilege(p, op);
                }

                return (Privilege) __Factory.Instance.ProduceObject(_OpPrivileges[op]);
            }

            return null;
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerator<Op>                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through a collection.</summary>
        /// <returns>Enumerator.</returns>
        public IEnumerator<Op> GetEnumerator()
        {
            return _OpPrivileges.Keys.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerator                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through a collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _OpPrivileges.Keys.GetEnumerator();
        }
    }
}
