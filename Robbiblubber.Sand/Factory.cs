﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Robbiblubber.Sand.Model.Base;



namespace Robbiblubber.Sand
{
    /// <summary>This class provides methods for producing objects.</summary>
    public static class Factory
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Returns an object with a given ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Producible object.</returns>
        public static IProducible Produce(string id)
        {
            return __Factory.Instance.ProduceObject(id);
        }


        /// <summary>Returns an object with a given ID.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="id">ID.</param>
        /// <returns>Producible object.</returns>
        public static T Produce<T>(string id) where T: IProducible
        {
            return (T) __Factory.Instance.ProduceObject(id);
        }


        /// <summary>Produces a single instance from an object comparison.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="expression">Expression.</param>
        /// <returns>Instance. Returns NULL when no object matches.</returns>
        public static T Produce<T>(Expression<Func<T, bool>> expression) where T: IProducible
        {
            return __Factory.Instance.ProduceObject(expression);
        }


        /// <summary>Returns a list of all instances of a producible type.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <returns>List of producible objects.</returns>
        public static IProducibleList<T> All<T>() where T: IProducible
        {
            return new ProducibleList<T>(false);
        }


        /// <summary>Returns a list of all instances of a producible type that match a given expression.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <returns>List of producible objects.</returns>
        public static IProducibleList<T> All<T>(Expression<Func<T, bool>> expression) where T: IProducible
        {
            return new ProducibleList<T>(expression);
        }
    }
}
