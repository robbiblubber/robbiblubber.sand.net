﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>Attachable system objects implement this interface.</summary>
    public interface __IAttachable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Attaches the object to the system.</summary>
        void Attach();


        /// <summary>Detaches the object from the system.</summary>
        void Detach();
    }
}
