﻿using System;



namespace Robbiblubber.Sand.Security
{
    /// <summary>Objects representing a session implement this interface.</summary>
    public interface ISession
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the session token for this session.</summary>
        string Token { get; }


        /// <summary>Gets the user object for this session.</summary>
        User User { get; }


        /// <summary>Gets the session start time.</summary>
        DateTime StartTime { get; }


        /// <summary>Gets the session end time.</summary>
        DateTime EndTime { get; }


        /// <summary>Gets the session user name.</summary>
        string UserName { get; }


        /// <summary>Gets the session data for this session.</summary>
        string Data { get; }


        /// <summary>Gets a value that determines if the session is active.</summary>
        bool Active { get; }
    }
}
