﻿using System;

using Robbiblubber.Sand.Model;



namespace Robbiblubber.Sand.Security
{
    /// <summary>This is the abstract base class for system master data.</summary>
    public abstract class SystemObject: Producible, ISystemObject, IProducible
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISystemObject                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets or sets the object name.</summary>
        public string Name
        {
            get { return _GetValue<string>("Name"); }
            set { _SetValue("Name", value); }
        }


        /// <summary>Gets or sets the object description.</summary>
        public string Description
        {
            get { return _GetValue<string>("Description"); }
            set { _SetValue("Description", value); }
        }


        /// <summary>Gets or sets the object parent logical unit.</summary>
        public LogicalUnit LogicalUnit
        {
            get { return _GetValue<LogicalUnit>("LogicalUnit"); }
            set { _SetValue("LogicalUnit", value); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IDisplayable                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the object's display name.</summary>
        public virtual string DisplayName
        {
            get { return Name; }
        }
    }
}
