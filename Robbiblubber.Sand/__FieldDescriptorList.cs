﻿using System;
using System.Collections.Generic;
using System.Linq;

using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements a field descriptor list.</summary>
    public sealed class __FieldDescriptorList: __DescriptorList<__FieldDescriptor>, IEnumerable<__FieldDescriptor>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>ID field.</summary>
        private __FieldDescriptor _IDField = null;

        /// <summary>Modification time field.</summary>
        private __FieldDescriptor _TimeModified = null;

        /// <summary>Creation context field.</summary>
        private __FieldDescriptor _CreatedBy = null;

        /// <summary>Modification context field.</summary>
        private __FieldDescriptor _ModifiedBy = null;

        /// <summary>Field descriptor array.</summary>
        private __FieldDescriptor[] _Array = null;

        /// <summary>Column names array.</summary>
        private string[] _ColumnNames = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the ID field.</summary>
        public __FieldDescriptor IDField
        {
            get
            {
                if(_IDField == null)
                {
                    foreach(__FieldDescriptor i in _Items.Values)
                    {
                        if(i.ContentType == __ContentType.ID) { _IDField = i; break; }
                    }
                }

                return _IDField;
            }
        }


        /// <summary>Gets the modification time field.</summary>
        public __FieldDescriptor TimeModified
        {
            get
            {
                if(_TimeModified == null)
                {
                    foreach(__FieldDescriptor i in _Items.Values)
                    {
                        if(i.ContentType == __ContentType.JTIME) { _TimeModified = i; break; }
                    }
                }

                return _TimeModified;
            }
        }


        /// <summary>Gets the creation context field.</summary>
        public __FieldDescriptor CreatedBy
        {
            get
            {
                if(_CreatedBy == null)
                {
                    foreach(__FieldDescriptor i in _Items.Values)
                    {
                        if(i.ContentType == __ContentType.JCREATED) { _CreatedBy = i; break; }
                    }
                }

                return _CreatedBy;
            }
        }


        /// <summary>Gets the modfication context field.</summary>
        public __FieldDescriptor ModifiedBy
        {
            get
            {
                if(_ModifiedBy == null)
                {
                    foreach(__FieldDescriptor i in _Items.Values)
                    {
                        if(i.ContentType == __ContentType.JMODIFIED) { _ModifiedBy = i; break; }
                    }
                }

                return _ModifiedBy;
            }
        }


        /// <summary>Gets the fields as an array.</summary>
        public __FieldDescriptor[] Array
        {
            get
            {
                if(_Array == null) { _Array = _Items.Values.ToArray(); }

                return _Array;
            }
        }


        /// <summary>Gets an array of column names.</summary>
        public string[] ColumnNames
        {
            get
            {
                if(_ColumnNames == null)
                {
                    _ColumnNames = new string[_Items.Count];

                    int n = 0;
                    foreach(__FieldDescriptor i in _Items.Values) { _ColumnNames[n++] = i.ColumnName; }
                }

                return _ColumnNames;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns if the list contains a field.</summary>
        /// <param name="field">Field.</param>
        /// <returns>Returns TRUE if the list contains the field, otherwise returns FALSE.</returns>
        public bool Contains(__FieldDescriptor field)
        {
            return _Items.ContainsKey(field.FieldName);
        }


        /// <summary>Returns if the list contains a field of the given name.</summary>
        /// <param name="fieldName">Field name.</param>
        /// <returns>Returns TRUE if the list contains the field, otherwise returns FALSE.</returns>
        public bool Contains(string fieldName)
        {
            return _Items.ContainsKey(fieldName);
        }


        /// <summary>Returns if the list contains a field of the given content type.</summary>
        /// <param name="type">Content type.</param>
        /// <returns>Returns TRUE if the list contains at least one field of that type, otherwise returns FALSE:</returns>
        public bool Contains(__ContentType type)
        {
            foreach(__FieldDescriptor i in _Items.Values)
            {
                if(i.ContentType == type) return true;
            }

            return false;
        }


        /// <summary>Gets a field by its column name.</summary>
        /// <param name="columnName">Column name.</param>
        /// <returns>Field.</returns>
        public __FieldDescriptor ByColumnName(string columnName)
        {
            foreach(__FieldDescriptor i in _Items.Values)
            {
                if(i.ColumnName == columnName) { return i; }
            }

            return null;
        }


        /// <summary>Selects the fields from this list in a query.</summary>
        /// <param name="query">SQL query.</param>
        public void SelectFields(ISQLQuery query)
        {
            foreach(__FieldDescriptor i in _Items.Values)
            {
                query.Select(i);
            }
        }
    }
}
