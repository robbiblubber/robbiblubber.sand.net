﻿using System;
using System.Data;
using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class represents field data.</summary>
    public sealed class __FieldData
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Field value.</summary>
        private object _Value;

        /// <summary>Virgin flag.</summary>
        private bool _Virgin;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="list">Parent field list.</param>
        /// <param name="descriptor">Field descriptor.</param>
        public __FieldData(__FieldList list, __FieldDescriptor descriptor)
        {
            List = list;
            Descriptor = descriptor;
            Virgin = false;

            if((Descriptor.DataType == SQLDataType.BOOLEAN) || Descriptor.DataType == SQLDataType.BOOLEAN_INTEGER)
            {
                _Value = false;
            }
            else if((Descriptor.DataType == SQLDataType.INTEGER) || (Descriptor.DataType == SQLDataType.INT16) || (Descriptor.DataType == SQLDataType.INT32) || (Descriptor.DataType == SQLDataType.INT64))
            {
                _Value = 0;
            }
            else if(Descriptor.DataType == SQLDataType.TIMESTAMP)
            {
                _Value = DateTime.MinValue;
            }
            else { _Value = null; }            
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="list">Parent field list.</param>
        /// <param name="descriptor">Field descriptor.</param>
        /// <param name="value">Field data.</param>
        public __FieldData(__FieldList list, __FieldDescriptor descriptor, object value)
        {
            List = list;
            Descriptor = descriptor;
            _Virgin = true;
            _Value = value;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent field list.</summary>
        public __FieldList List { get; private set; }


        /// <summary>Gets the field descriptor.</summary>
        public __FieldDescriptor Descriptor { get; private set; }


        /// <summary>Gets or sets if the field is unmodified.</summary>
        public bool Virgin
        {
            get { return _Virgin; }
            set
            {
                if(_Virgin = value)
                {
                    if(!List.Virgin) { List._CheckVirginity(); }
                }
                else { List.Virgin = false; }
            }
        }


        /// <summary>Gets or sets the field value.</summary>
        public object Value
        {
            get { return _Value; }
            set
            {
                if(value != _Value)
                {
                    List.Parent.Lock();

                    Virgin = false;
                    _Value = value;
                }
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Reads data from a database reader.</summary>
        /// <param name="re">Reader.</param>
        /// <param name="n">Index.</param>
        public void Read(IDataReader re)
        {
            int n = re.GetOrdinal(Descriptor.ColumnAlias);
            Virgin = true;

            if(Descriptor.DataType == SQLDataType.BOOLEAN)
            {
                if(re.IsDBNull(n)) { Value = false; return; }
                _Value = re.GetBoolean(n);
            }
            else if(Descriptor.DataType == SQLDataType.BOOLEAN_INTEGER)
            {
                if(re.IsDBNull(n)) { Value = false; return; }
                _Value = (Convert.ToInt32(re.GetValue(n)) != 0);
            }
            else if((Descriptor.DataType == SQLDataType.INTEGER) || (Descriptor.DataType == SQLDataType.INT32))
            {
                if(re.IsDBNull(n)) { Value = 0; return; }
                _Value = Convert.ToInt32(re.GetValue(n));
            }
            else if(Descriptor.DataType == SQLDataType.INT16)
            {
                if(re.IsDBNull(n)) { Value = ((short) 0); return; }
                _Value = Convert.ToInt16(re.GetValue(n));
            }
            else if(Descriptor.DataType == SQLDataType.INT64)
            {
                if(re.IsDBNull(n)) { Value = ((long) 0); return; }
                _Value = Convert.ToInt64(re.GetValue(n));
            }
            else if((Descriptor.DataType == SQLDataType.OBJECT_ID) || (Descriptor.DataType == SQLDataType.STRING) || (Descriptor.DataType == SQLDataType.TEXT))
            {
                if(re.IsDBNull(n)) { Value = null; return; }
                _Value = re.GetString(n);
            }
            else if(Descriptor.DataType == SQLDataType.TIMESTAMP)
            {
                if(re.IsDBNull(n)) { Value = DateTime.MinValue; return; }
                _Value = re.GetDateTime(n);
            }
        }
    }
}
