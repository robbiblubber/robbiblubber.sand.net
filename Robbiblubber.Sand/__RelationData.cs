﻿using System;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class represents relation data.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public sealed class __RelationData
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Data.</summary>
        private __IFactoryListTarget _Data = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="list">Parent field list.</param>
        /// <param name="descriptor">Field descriptor.</param>
        public __RelationData(__RelationList list, __RelationDescriptor descriptor)
        {
            List = list;
            Descriptor = descriptor;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent field list.</summary>
        public __RelationList List { get; private set; }


        /// <summary>Gets the field descriptor.</summary>
        public __RelationDescriptor Descriptor { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public members                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the list for the relation.</summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>List.</returns>
        public IProducibleList<T> GetReadonlyList<T>() where T : IProducible
        {
            if(_Data == null) { _Data = new ProducibleList<T>(List.Parent, Descriptor); }

            return (IProducibleList<T>) _Data;
        }


        /// <summary>Gets the list for the relation.</summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>List.</returns>
        public IMutableProducibleList<T> GetList<T>() where T: IProducible
        {
            if(_Data == null) { _Data = new MutableProducibleList<T>(List.Parent, Descriptor); }

            return (IMutableProducibleList<T>) _Data;
        }


        /// <summary>Gets the list for the relation.</summary>
        /// <returns>List. Returns NULL if the list has not been created yet.</returns>
        public __IFactoryListTarget GetList()
        {
            return _Data;
        }
    }
}
