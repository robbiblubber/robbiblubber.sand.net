﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Robbiblubber.Sand.Model.Base;
using Robbiblubber.Sand.Security;



namespace Robbiblubber.Sand.Model
{
    /// <summary>This class represents a system lock.</summary>
    public sealed class Lock
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal members                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Thread ID.</summary>
        internal int _ThreadID = -1;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="session">Session.</param>
        public Lock(IProducible obj, Session session)
        {
            Object = obj;
            Session = session;

            if(session is Context) {  _ThreadID = Thread.CurrentThread.ManagedThreadId; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the locked object.</summary>
        public IProducible Object
        {
            get; internal set;
        }

                
        /// <summary>Context.</summary>
        public ISession Session
        {
            get; internal set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Releases the lock.</summary>
        /// <param name="context">Context.</param>
        /// <param name="force">Allows administrative contextes to forcefully remove locks held by other user contextes.</param>
        public void Release(Context context, bool force = false)
        {
            __Factory.Instance.ReleaseLock(Object, context, force);
        }


        /// <summary>Releases the lock.</summary>
        /// <param name="force">Allows administrative contextes to forcefully remove locks held by other user contextes.</param>
        public void Release(bool force = false)
        {
            Release(Context.Current, force);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Releases orphan and timed out locks.</summary>
        public static void CleanUp()
        {
            __Factory.Instance.CleanUpLocks();
        }
    }
}
