﻿using System;



namespace Robbiblubber.Sand
{
    /// <summary>Classes that support refresh implement this interface.</summary>
    public interface IRefreshable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes the object.</summary>
        void Refresh();
    }
}
