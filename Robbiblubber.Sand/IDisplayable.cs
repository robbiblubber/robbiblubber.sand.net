﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Sand.Security
{
    /// <summary>Displayable objects implement this interface.</summary>
    public interface IDisplayable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the object's display name.</summary>
        string DisplayName { get; }
    }
}
