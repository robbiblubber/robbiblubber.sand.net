﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

using Robbiblubber.Sand.Model.Base;
using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand
{
    /// <summary>This class implements a basic list of producible objects.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public class ProducibleList<T>: IProducibleList<T>, IEnumerable<T>, IQueryable<T>, IRefreshable, __IFactoryListTarget where T: IProducible
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Relation.</summary>
        protected __RelationDescriptor _Relation = null;
        
        /// <summary>Query.</summary>
        protected internal ISQLQuery _Query = null;

        /// <summary>List items.</summary>
        protected internal IList<IProducible> _Items = null;

        /// <summary>Item index.</summary>
        protected IDictionary<string, IProducible> _Index = null;

        /// <summary>List item entity.</summary>
        protected __EntityDescriptor _Entity = null;

        /// <summary>Query provider.</summary>
        protected __QueryProvider<T> _Provider = null;

        /// <summary>Expression.</summary>
        protected internal Expression _Expression = null;

        /// <summary>Original items list.</summary>
        protected IList<IProducible> _PersistentItems = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public ProducibleList(): this(true)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="expression">Expression.</param>
        public ProducibleList(Expression<Func<T, bool>> expression)
        {
            _Entity = __Factory.Instance.Entities[typeof(T)];
            _Query = __Factory.Instance.Entities[typeof(T)].ExpressionParser.Parse(expression);
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="items">Items.</param>
        public ProducibleList(IEnumerable<T> items)
        {
            _Items = new List<IProducible>((IEnumerable<IProducible>) items);
            _Query = null;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="items">Items.</param>
        protected internal ProducibleList(IDictionary<string, IProducible> items)
        {
            _Items = new List<IProducible>(items.Values);
            _Index = items;
            _Query = null;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="empty">Determines if the instance is empty.</param>
        protected internal ProducibleList(bool empty)
        {
            _Entity = __Factory.Instance.Entities[typeof(T)];

            if(empty)
            {
                _Query = null;
                _Items = new List<IProducible>();
            } else
            {
                _Query = _Entity.CreateQuery();
            }
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="query">SQL query.</param>
        protected internal ProducibleList(ISQLQuery query)
        {
            _Entity = __Factory.Instance.Entities[typeof(T)];
            _Query = query;
        }


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="relation">Relation descriptor.</param>
        protected internal ProducibleList(IProducible parent, __RelationDescriptor relation)
        {
            Parent = parent;

            _Entity = relation.Entity;
            _Relation = relation;

            _Query = relation.CreateQuery(Parent);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent object of this list.</summary>
        public IProducible Parent { get; protected set; }
        

        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads list items.</summary>
        protected virtual void _Load()
        {
            if(_Items == null) { _Items = __Factory.Instance.Populate<T>(_Query); }
        }


        /// <summary>Creates an index for this list.</summary>
        protected virtual void _CreateIndex()
        {
            _Load();

            if(_Index == null)
            {
                _Index = new Dictionary<string, IProducible>();

                foreach(T i in _Items)
                {
                    try
                    {
                        _Index.Add(i.ID, i);
                    } 
                    catch(Exception) {}
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProducibleList<T>                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the number of items in this list.</summary>
        public virtual int Count
        {
            get { return _Items.Count; }
        }


        /// <summary>Gets the object with a given index from the list.</summary>
        /// <param name="n">Index.</param>
        /// <returns>Producible object.</returns>
        public virtual T this[int n]
        {
            get
            {
                _Load();
                return (T) _Items[n];
            }
        }


        /// <summary>Gets the object with a given ID from the list.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Producible object. Returns NULL if the list doesn't contain an object with the given ID.</returns>
        public virtual T this[string id]
        {
            get
            {
                _CreateIndex();

                return (T) _Index[id];
            }
        }


        /// <summary>Returns a value indicating if the list contains an object.</summary>
        /// <param name="item">Object.</param>
        /// <returns>Returns TRUE if the list contains the item, otherwise returns FALSE.</returns>
        public virtual bool Contains(T item)
        {
            _Load();
            return _Items.Contains(item);
        }


        /// <summary>Returns a value indicating if the list contains an object with the specified ID.</summary>
        /// <param name="item">Object.</param>
        /// <returns>Returns TRUE if the list contains the item, otherwise returns FALSE.</returns>
        public virtual bool Contains(string id)
        {
            _CreateIndex();

            return _Index.ContainsKey(id);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IRefreshable                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Refreshes this instance.</summary>
        public void Refresh()
        {
            if(_Query != null)
            {
                _Items = null;
                _Index = null;
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IFactoryListTarget                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a list of items as currently defined.</summary>
        IList<IProducible> __IFactoryListTarget.Items
        {
            get
            {
                _Load();
                return _Items;
            }
        }


        /// <summary>Gets a list of removed items.</summary>
        IList<IProducible> __IFactoryListTarget.PersistentItems
        {
            get { return _PersistentItems; }
            set { _PersistentItems = value; }
        }


        /// <summary>Gets or sets the list index.</summary>
        IDictionary<string, IProducible> __IFactoryListTarget.Index
        {
            get { return _Index; }
            set { _Index = value; }
        }


        /// <summary>Gets the relation descriptor.</summary>
        __RelationDescriptor __IFactoryListTarget.Relation
        {
            get { return _Relation; }
        }


        /// <summary>Loads list items if needed.</summary>
        void __IFactoryListTarget.Load()
        {
            _Load();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<T>                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through a collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            _Load();

            foreach(IProducible i in _Items) { yield return (T) i; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through a collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            _Load();
            return _Items.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IQueryable                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the expression tree associated with this instance.</summary>
        Expression IQueryable.Expression
        {
            get
            {
                if(_Expression == null) { _Expression = Expression.Constant(this); }
                return _Expression;
            }
        }

        /// <summary>Gets the type of elements that is returned when the expression tree is executed.</summary>
        Type IQueryable.ElementType
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        /// <summary>Gets the query provider that is associated with this data source.</summary>
        IQueryProvider IQueryable.Provider
        {
            get
            {
                if(_Provider == null) { _Provider = new __QueryProvider<T>(this); }

                return _Provider;
            }
        }
    }
}
