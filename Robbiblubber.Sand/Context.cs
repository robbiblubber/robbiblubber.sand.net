﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Robbiblubber.Sand.Model.Base;
using Robbiblubber.Util.Library;



namespace Robbiblubber.Sand.Security
{
    /// <summary>This class represents an active user session.</summary>
    public sealed class Context: Session, ISession, IProducible, IAtom
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static members                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Current context.</summary>
        private static Context _Current = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Context privileges.</summary>
        private IProducibleList<Privilege> _Privileges = null;

        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        internal Context()
        {
            Token = "0." + StringOp.Unique(Math.Min(52, Math.Max(24, __Factory.Instance.Entities["0"].Fields.IDField.Length - 2)));
            Active = false;
            UserName = "anonymous";
            Data = ("un=" + Environment.UserName + "; dn=" + Environment.UserDomainName + "; mn=" + Environment.MachineName + "; os=" + Environment.OSVersion + "; cl=" + Environment.CommandLine + ";");
            User = null;
            StartTime = DateTime.Now;
            EndTime = DateTime.MaxValue;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the current context.</summary>
        public static Context Current
        {
            get
            {
                if(_Current == null) { _Current = CreateAnonymous(); }

                return _Current;
            }
            set { _Current = value; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates an anonymous context.</summary>
        /// <returns>Context.</returns>
        public static Context CreateAnonymous()
        {
            return new Context();
        }


        /// <summary>Creates an authenticed context with user name and password.</summary>
        /// <param name="userName">User name.</param>
        /// <param name="password">Password.</param>
        /// <returns>Context.</returns>
        /// <remarks>Returns NULL when credentials could not be verified.</remarks>
        public static Context Create(string userName, string password)
        {
            return __Factory.Instance.CreateContext(userName, password);
        }


        /// <summary>Creates an authenticed context with system credentials.</summary>
        /// <returns>Context.</returns>
        /// <remarks>Returns NULL when credentials could not be verified.</remarks>
        public static Context Create()
        {
            return __Factory.Instance.CreateContext();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets if the context is anonymous.</summary>
        public bool Anonymous
        {
            get { return User == null; }
        }


        /// <summary>Gets a list of privileges held by this context.</summary>
        public IProducibleList<Privilege> Privileges
        {
            get
            {
                if(_Privileges == null) { _Privileges = __Factory.Instance.GetPrivileges(User); }

                return _Privileges;
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Returns if a context holds a given privilege.</summary>
        /// <param name="p">Privilege.</param>
        /// <returns>Returns TRUE if the context holds the privilege, otherwise returns FALSE.</returns>
        public bool Holds(Privilege p)
        {
            if(p == null) return true;

            return Privileges.Contains(p);
        }
    }
}
