﻿using System;
using System.Data;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements a relation descriptor that holds metadata for a 1:n relation.</summary>
    public sealed class __1ToNRelationDescriptor: __RelationDescriptor, __IDescriptor
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Target column.</summary>
        private string _TargetColumn;

        /// <summary>Target field.</summary>
        private __FieldDescriptor _TargetField = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="entity">Parent entity.</param>
        /// <param name="section">ddp section.</param>
        public __1ToNRelationDescriptor(__EntityDescriptor entity, DdpSection section): base(entity, section)
        {
            _TargetColumn = section.GetString("column");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent entity for this relation.</summary>
        public __FieldDescriptor TargetField
        {
            get
            {
                if(_TargetField == null) { _TargetField = TargetEntity.Fields.ByColumnName(_TargetColumn); }
                return _TargetField;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] __RelationDescriptor                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a SQL query for this relation for a given producible object.</summary>
        /// <param name="p">Producible object.</param>
        /// <returns>SQL query.</returns>
        public override ISQLQuery CreateQuery(IProducible p)
        {
            return TargetEntity.CreateQuery().Where(TargetField.ColumnExpression + " = " + TargetEntity.Provider.Parser.ToBindVariableName("id")).AddParameter(TargetEntity.Provider.Parser.ToBindVariableName("id"), p.ID);
        }
    }
}
