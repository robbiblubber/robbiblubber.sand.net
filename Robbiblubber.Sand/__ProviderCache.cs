﻿using System;
using System.Collections.Generic;
using System.IO;

using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class provides a cache for database providers.</summary>
    public sealed class __ProviderCache
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>List items.</summary>
        private Dictionary<string, IProvider> _Items = new Dictionary<string, IProvider>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="repository">Repository.</param>
        internal __ProviderCache()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets a provider by its name.</summary>
        /// <param name="name">Provider name.</param>
        /// <returns>Provider.</returns>
        public IProvider this[string name]
        {
            get
            {
                if(string.IsNullOrWhiteSpace(name)) { name = "default"; }

                if(!_Items.ContainsKey(name))
                {
                    IProvider p = SQLProvider.GetProvider(File.ReadAllText(__Repository.Instance.Location + "\\" + name + ".provider"));
                    if(p != null)
                    {
                        p.Connect();
                        _Items.Add(name, p);
                    }
                }

                return _Items[name];
            }
        }
    }
}
