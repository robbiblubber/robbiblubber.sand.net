﻿using System;



namespace Robbiblubber.Sand.Security
{
    /// <summary>System objects implement this interface.</summary>
    public interface ISystemObject: IProducible, IDisplayable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the object name.</summary>
        string Name { get; set; }


        /// <summary>Gets or sets the object description.</summary>
        string Description { get; set; }


        /// <summary>Gets or sets the parent logical unit.</summary>
        LogicalUnit LogicalUnit { get; set; }
    }
}
