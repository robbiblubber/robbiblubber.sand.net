﻿using System;
using System.Collections.Generic;
using System.Linq;



namespace Robbiblubber.Sand
{
    /// <summary>Producible object lists that support adding/removing items implement this interface.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public interface IMutableProducibleList<T>: IProducibleList<T>, IEnumerable<T>, IQueryable<T>, IRefreshable where T: IProducible
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an item to the list.</summary>
        /// <param name="item">Item.</param>
        void Add(T item);


        /// <summary>Removes an item from the list.</summary>
        /// <param name="item">Item.</param>
        void Remove(T item);


        /// <summary>Removes an item from the list.</summary>
        /// <param name="id">Item ID.</param>
        void Remove(string id);


        /// <summary>Removes all items from the list.</summary>
        void Clear();
    }
}
