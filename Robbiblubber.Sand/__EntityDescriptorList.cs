﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Robbiblubber.Util.Library;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements an entity descriptor list.</summary>
    public sealed class __EntityDescriptorList: __DescriptorList<__EntityDescriptor>, IEnumerable<__EntityDescriptor>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Type-based item dictionary.</summary>
        private Dictionary<Type, __EntityDescriptor> _ItemsByType = new Dictionary<Type, __EntityDescriptor>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the entity descriptor for the type.</summary>
        /// <param name="t">Type.</param>
        /// <returns>Entity descriptor.</returns>
        public __EntityDescriptor this[Type t]
        {
            get
            {
                try
                {
                    return _ItemsByType[t];
                }
                catch(Exception) {}

                foreach(__EntityDescriptor i in _Items.Values)
                {
                    if(i.EntityType == t)
                    {
                        _ItemsByType.Add(t, i);
                        return i;
                    }
                }

                throw new InvalidOperationException("No entity descriptor found for type \"" + t.FullName + "\".");
            }
        }
    }
}
