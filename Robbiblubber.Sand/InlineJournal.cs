﻿using System;

using Robbiblubber.Sand.Model.Base;
using Robbiblubber.Sand.Security;



namespace Robbiblubber.Sand.Journalization
{
    /// <summary>This class represents the inline journal information for an obejct.</summary>
    public sealed class InlineJournal
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parent object.</summary>
        private IProducible _Parent;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parent">Parent object.</param>
        public InlineJournal(IProducible parent)
        {
            _Parent = parent;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the modification timestamp for the object.</summary>
        public DateTime TimeModified
        {
            get { return (DateTime) ((__IFactoryTarget) _Parent).Fields["TimeModified"].Value; }
        }


        /// <summary>Gets the session that created the object.</summary>
        public ISession CreatedBy
        {
            get { return (ISession) __Factory.Instance.ProduceObject((string) ((__IFactoryTarget) _Parent).Fields["CreatedBy"].Value); }
        }


        /// <summary>Gets the session that modified the object last.</summary>
        public ISession ModifiedBy
        {
            get { return (ISession) __Factory.Instance.ProduceObject((string) ((__IFactoryTarget) _Parent).Fields["ModifiedBy"].Value); }
        }
    }
}
