﻿using System;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This enum defines field access modes.</summary>
    public enum __FieldAccess: int
    {
        NONE = 0,
        READ = 1,
        WRITE = 2,
        READ_WRITE = 3
    }
}
