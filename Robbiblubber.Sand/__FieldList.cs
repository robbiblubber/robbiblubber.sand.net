﻿using System;

using System.Collections;
using System.Collections.Generic;
using System.Data;

using Robbiblubber.Sand.Security;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class represents a field list.</summary>
    public sealed class __FieldList: IEnumerable<__FieldData>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>List items.</summary>
        private Dictionary<string, __FieldData> _Items;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="p">Parent object.</param>
        public __FieldList(Producible p)
        {
            Parent = p;
            _Items = new Dictionary<string, __FieldData>();

            foreach(__FieldDescriptor i in ((__IFactoryTarget) p).EntityDescriptor.Fields)
            {
                __FieldData f = new __FieldData(this, i);
                _Items.Add(i.FieldName, f);
                if(i.ContentType == __ContentType.ID) { IDField = f; }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets a field by its name.</summary>
        /// <param name="fieldName">Field name.</param>
        /// <returns>Field data.</returns>
        public __FieldData this[string fieldName]
        {
            get { return _Items[fieldName]; }
        }


        /// <summary>Gets the ID field.</summary>
        public __FieldData IDField { get; private set; }


        /// <summary>Gets the parent object for this list.</summary>
        public Producible Parent { get; private set; }


        /// <summary>Gets the entity descriptor.</summary>
        public __EntityDescriptor EntityDescriptor
        {
            get { return ((__IFactoryTarget) Parent).EntityDescriptor; }
        }


        /// <summary>Gets if the fields in this list are unmodified.</summary>
        public bool Virgin { get; internal set; } = true;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Reads data from a database reader.</summary>
        /// <param name="re">Reader.</param>
        public void Read(IDataReader re)
        {
            Virgin = true;
            
            foreach(__FieldData i in _Items.Values)
            {
                i.Read(re);
            }
        }


        /// <summary>Updates entity journal.</summary>
        public void Update(Context context)
        {
            foreach(__FieldData i in _Items.Values)
            {
                if(i.Descriptor.ContentType == __ContentType.JTIME)
                {
                    i.Value = DateTime.Now;
                }
                else if(i.Descriptor.ContentType == __ContentType.JMODIFIED)
                {
                    i.Value = context.Token;
                }
                else if(i.Descriptor.ContentType == __ContentType.JCREATED)
                {
                    if(i.Value == null) { i.Value = context.Token; }
                }
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal methods                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Checks member fields and evaluates the <see cref="Virgin"/> flag accordingly.</summary>
        internal void _CheckVirginity()
        {
            foreach(__FieldData i in _Items.Values)
            {
                if(!i.Virgin) { Virgin = false; return; }
            }

            Virgin = true;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<__FieldData>                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<__FieldData> IEnumerable<__FieldData>.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }
    }
}
