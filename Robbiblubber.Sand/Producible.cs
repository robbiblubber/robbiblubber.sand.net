﻿using System;

using Robbiblubber.Sand.Journalization;
using Robbiblubber.Sand.Model.Base;
using Robbiblubber.Sand.Security;
using Robbiblubber.Sand.Model;



namespace Robbiblubber.Sand
{
    /// <summary>Producible object base class.</summary>
    public abstract class Producible: IAtom, IRefreshable, IProducible, ISynchronizable, __IFactoryTarget
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Object ID field.</summary>
        protected __FieldData _ID = null;

        /// <summary>Entity descriptor.</summary>
        protected __EntityDescriptor _Entity;

        /// <summary>Member fields list.</summary>
        protected __FieldList _Fields;

        /// <summary>Member relations list.</summary>
        protected __RelationList _Relations;

        /// <summary>Lock object.</summary>
        protected Lock _Lock = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public Producible()
        {
            _Entity = __Factory.Instance.Entities[GetType()];
            _Fields = new __FieldList(this);
            _Relations = new __RelationList(this);

            Ops = new Operations(this);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets a field value.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="fieldName"></param>
        /// <returns>Field value.</returns>
        protected virtual T _GetValue<T>(string fieldName)
        {
            return (T) _Fields[fieldName].Value;
        }


        /// <summary>Sets a field value.</summary>
        /// <param name="fieldName">Field name.</param>
        /// <param name="value">Field value.</param>
        protected virtual void _SetValue(string fieldName, object value)
        {
            _Fields[fieldName].Value = value;
        }


        /// <summary>Gets a relation list.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="fieldName">Field name.</param>
        /// <returns>Relation list.</returns>
        protected virtual IProducibleList<T> _GetReadonlyRelation<T>(string fieldName) where T: IProducible
        {
            return _Relations[fieldName].GetReadonlyList<T>();
        }


        /// <summary>Gets a relation list.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="fieldName">Field name.</param>
        /// <returns>Relation list.</returns>
        protected virtual IMutableProducibleList<T> _GetRelation<T>(string fieldName) where T : IProducible
        {
            return _Relations[fieldName].GetList<T>();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IAtom                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the object ID.</summary>
        public string ID
        {
            get
            {
                return (string) _Fields.IDField.Value;
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IRefreshable                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Refreshes the object.</summary>
        public void Refresh()
        {
            // TODO: implement refresh
            throw new NotImplementedException();
        }

        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProducible                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the operations for this object.</summary>
        public virtual Operations Ops
        {
            get; protected set;
        }


        /// <summary>Gets the inline journal for this object.</summary>
        public virtual InlineJournal InlineJournal
        {
            get { return new InlineJournal(this); }
        }


        public IProducible Clone()
        {
            // TODO: implement clone
            throw new NotImplementedException();
        }

        public void Delete(Context context)
        {
            // TODO: implement delete
            throw new NotImplementedException();
        }

        public void Delete()
        {
            throw new NotImplementedException();
        }


        /// <summary>Locks the object for a given context.</summary>
        /// <param name="context">Context.</param>
        /// <exception cref="ObjectLockedException">Thrown when the object is already locked.</exception>
        public void Lock(Context context)
        {
            __Factory.Instance.CreateLock(this, context);
        }



        /// <summary>Locks the object.</summary>
        /// <exception cref="ObjectLockedException">Thrown when the object is already locked.</exception>
        public void Lock()
        {
            Lock(Context.Current);
        }


        /// <summary>Returns the lock object for this instance.</summary>
        public Lock GetLock()
        {
            return __Factory.Instance.GetLock(this);
        }


        /// <summary>Saves the object.</summary>
        /// <param name="context">Context.</param>
        /// <exception cref="InsufficientPrivilegesException">Thrown if the context lacks privileges for the desired operation.</exception>
        public void Save(Context context)
        {
            __Factory.Instance.SaveObject(this, context);
        }


        /// <summary>Saves the object.</summary>
        /// <exception cref="InsufficientPrivilegesException">Thrown if the context lacks privileges for the desired operation.</exception>
        public void Save()
        {
            Save(Context.Current);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISynchronizable                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the synchronization particle for this instance.</summary>
        object ISynchronizable.__Sync
        {
            get;
        } = new object();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IFactoryTarget                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the factory target entity descriptor.</summary>
        __EntityDescriptor __IFactoryTarget.EntityDescriptor
        {
            get { return _Entity; }
        }


        /// <summary>Gets the fields list.</summary>
        __FieldList __IFactoryTarget.Fields
        {
            get { return _Fields; }
        }


        /// <summary>Gets the relations list.</summary>
        __RelationList __IFactoryTarget.Relations
        {
            get { return _Relations; }
        }


        /// <summary>Gets or sets the lock object for this instance.</summary>
        Lock __IFactoryTarget.LockObject
        {
            get { return _Lock; }
            set { _Lock = value; }
        }
    }
}
