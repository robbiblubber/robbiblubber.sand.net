﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;

using Robbiblubber.Sand.Exceptions;
using Robbiblubber.Sand.Journalization;
using Robbiblubber.Sand.Security;
using Robbiblubber.Sand.Security.Exceptions;
using Robbiblubber.Util.Library;
using Robbiblubber.Util.Library.Coding;
using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class provides a factory instance.</summary>
    public class __Factory: IFactory
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Time of last cache refresh.</summary>
        protected DateTime _LastRead = DateTime.Now;

        /// <summary>New change command.</summary>
        protected string _NewChangeCmd;

        /// <summary>Get change command.</summary>
        protected string _GetChangeCmd;

        /// <summary>Purge change command.</summary>
        protected string _PurgeChangeCmd;

        /// <summary>Change bind variable name.</summary>
        protected string _ChangeBv;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        protected __Factory()
        {
            Instance = this;

            Providers = new __ProviderCache();
            SystemProvider = Providers[__Repository.Instance.RepositoryFile.Sections["factory"].GetString("provider")];

            _LoadEntities();
            _PrepareChangeCommands();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the active factory instance.</summary>
        public static IFactory Instance
        {
            get; protected set;
        } = _CreateInstance();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a factory instance.</summary>
        /// <returns>Instance.</returns>
        private static IFactory _CreateInstance()
        {
            IFactory rval = (IFactory) Activator.CreateInstance(Type.GetType(__Repository.Instance.FactoryClassName), true);
            if(rval == null)
            {
                throw new TypeLoadException("Unable to load factory class \"" + __Repository.Instance.FactoryClassName + "\".");
            }
            
            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected methods                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Loads entities from files.</summary>
        protected virtual void _LoadEntities()
        {
            Entities = new __EntityDescriptorList();

            switch(__Repository.Instance.EntityFileMode)
            {
                case "SINGLE":
                    _LoadEntities(Ddp.Load("entities"));
                    break;
                case "MULTIPLE":
                    foreach(string i in Directory.GetFiles(__Repository.Instance.Location, "*.entity"))
                    {
                        foreach(DdpSection j in Ddp.Load(i).Sections) { _LoadEntities(j); }
                    }
                    break;
                case "DATABASE":
                    /* TODO: implement database entity definition loading */
                    break;
            }
        }


        /// <summary>Prepares SQL for changes.</summary>
        protected virtual void _PrepareChangeCommands()
        {
            __EntityDescriptor entity = Entities["Change*"];
            int timeout = __Repository.Instance.RepositoryFile.Sections["cache"].GetInteger("timeout", 240);

            _ChangeBv = entity.Provider.Parser.ToBindVariableName("chv");
            _NewChangeCmd = "INSERT INTO " + entity.Tables.RootTable.TableName + " (" + entity.Fields["Object"].ColumnName + ", " + entity.Fields["TimeModified"].ColumnName +
                            ") VALUES (" + _ChangeBv + ", " + entity.Provider.Parser.CurrentTimestampFunction + ")";
            _GetChangeCmd = "SELECT " + entity.Fields["Object"].ColumnName + " FROM " + entity.Tables.RootTable.TableName +
                            " WHERE " + entity.Fields["TimeModified"].ColumnName + " >= " + _ChangeBv;
            _PurgeChangeCmd = "DELETE FROM " + entity.Tables.RootTable + " WHERE " + entity.Fields["TimeModified"] + " < " +
                              entity.Provider.Parser.AddTimeInterval(entity.Provider.Parser.CurrentTimestampFunction, new TimeSpan(0, -timeout, 0));
        }


        /// <summary>Loads entity definitions from a single file.</summary>
        /// <param name="sec">ddp section.</param>
        protected virtual void _LoadEntities(DdpSection sec)
        {
            __EntityDescriptor d = new __EntityDescriptor(sec);
            Entities.Add(d);
        }


        /// <summary>Registers a context for a user.</summary>
        /// <param name="user">User.</param>
        /// <returns>Context.</returns>
        protected virtual Context _RegisterContext(User user)
        {
            Context rval = new Context();
            rval.User = user;
            rval.UserName = user.Name;
            rval.Active = true;

            __EntityDescriptor entity = Entities["0"];
            IDbCommand cmd = entity.Provider.CreateCommand();

            cmd.CommandText = "INSERT INTO " + entity.Tables[0].TableName + " (" +
                                               entity.Fields["Token"].ColumnName + ", " +
                                               entity.Fields["Active"].ColumnName + ", " +
                                               entity.Fields["UserName"].ColumnName + ", " +
                                               entity.Fields["Data"].ColumnName + ", " +
                                               entity.Fields["User"].ColumnName + ", " +
                                               entity.Fields["StartTime"].ColumnName + ", " +
                                               entity.Fields["EndTime"].ColumnName + ", " +
                                               entity.Fields["TimeModified"].ColumnName + ") VALUES (" +
                                               entity.Fields["Token"].BindVariableName + ", 1, " +
                                               entity.Fields["UserName"].BindVariableName + ", " +
                                               entity.Fields["Data"].BindVariableName + ", " +
                                               entity.Fields["User"].BindVariableName + ", " +
                                               entity.Fields["StartTime"].BindVariableName + ", NULL, " + entity.Provider.Parser.CurrentTimestampFunction + ")";

            cmd.AddParameter(entity.Fields["Token"].BindVariableName, rval.Token);
            cmd.AddParameter(entity.Fields["UserName"].BindVariableName, rval.UserName);
            cmd.AddParameter(entity.Fields["Data"].BindVariableName, rval.Data);
            cmd.AddParameter(entity.Fields["User"].BindVariableName, rval.User.ID);
            cmd.AddParameter(entity.Fields["StartTime"].BindVariableName, rval.StartTime);

            cmd.ExecuteNonQuery();

            Journal.Write(user, rval, Journal.Severity.INFORMATION, Journal.Operation.AUTHENTICATION, "User \"" + user.DisplayName + "\" logged in.", user.Name, rval.Data, "", rval);
            return rval;
        }


        /// <summary>Executes a command and populates the privilege list.</summary>
        /// <param name="cmd">Database command.</param>
        /// <param name="data">Result list.</param>
        protected virtual void _ProcessPrivileges(IDbCommand cmd, Dictionary<string, IProducible> data)
        {
            IDataReader re = cmd.ExecuteReader();
            Privilege i;

            while(re.Read())
            {
                i = ProduceObject<Privilege>(re);
                if(!data.ContainsKey(i.ID)) { data.Add(i.ID, i); }
            }
            re.Close();
            Disposal.Dispose(re, cmd);
        }


        /// <summary>Gets privileges assigned to an object.</summary>
        /// <param name="id">Parent ID.</param>
        /// <param name="data">Result list.</param>
        protected virtual void _RecursePrivileges(string id, Dictionary<string, IProducible> data)
        {
            __EntityDescriptor prvEntity = Entities[typeof(Privilege)];
            __EntityDescriptor relEntity = null;

            if(id.StartsWith("24."))
            {
                relEntity = Entities["UserObject*"];
            }
            else if(id.StartsWith("22."))
            {
                relEntity = Entities["RoleObject*"];
            }
            else if(id.StartsWith("23."))
            {
                relEntity = Entities["GroupObject*"];
            }

            ISQLQuery query = prvEntity.Provider.Parser.CreateQuery().From(prvEntity.Tables.RootTable)
                                                                     .Join(relEntity.Tables.RootTable, prvEntity.Fields.IDField.ColumnExpression + " = " + relEntity.Fields["Object"].ColumnExpression)
                                                                     .Select(prvEntity.Fields.Array)
                                                                     .Where(relEntity.Fields.IDField.ColumnExpression + " = " + relEntity.Fields.IDField.BindVariableName);
            IDbCommand cmd = prvEntity.Provider.CreateCommand(query.Text);
            cmd.AddParameter(relEntity.Fields.IDField.BindVariableName, id);
            _ProcessPrivileges(cmd, data);

            query = relEntity.Provider.Parser.CreateQuery().From(relEntity.Tables.RootTable)
                                                           .Select(relEntity.Fields["Object"])
                                                           .Where(relEntity.Fields.IDField.ColumnExpression + " = " + relEntity.Fields.IDField.BindVariableName)
                                                           .And(relEntity.Fields["Object"].ColumnExpression + " LIKE " + relEntity.Fields["Object"].BindVariableName);
            cmd = relEntity.Provider.CreateCommand(query.Text);
            cmd.AddParameter(relEntity.Fields.IDField.BindVariableName, id);
            cmd.AddParameter(relEntity.Fields["Object"].BindVariableName, "22.%");

            List<string> roles = new List<string>();
            IDataReader re = cmd.ExecuteReader();
            while(re.Read()) { roles.Add(re.GetString(0)); }
            re.Close();
            Disposal.Recycle(cmd, re);

            foreach(string i in roles) _RecursePrivileges(i, data);
        }


        /// <summary>Gets group memberships for an object.</summary>
        /// <param name="id">Parent ID.</param>
        /// <param name="data">Result list.</param>
        protected virtual void _RecurseGroups(string id, List<string> data)
        {
            __EntityDescriptor relEntity = Entities["GroupMember*"];

            ISQLQuery query = relEntity.Provider.Parser.CreateQuery().From(relEntity.Tables.RootTable)
                                                                     .Select(relEntity.Fields.IDField)
                                                                     .Where(relEntity.Fields["Member"].ColumnExpression + " = " + relEntity.Fields["Member"].BindVariableName);
            IDbCommand cmd = relEntity.Provider.CreateCommand(query.Text);
            cmd.AddParameter(relEntity.Fields["Member"].BindVariableName, id);

            List<string> groups = new List<string>();
            IDataReader re = cmd.ExecuteReader();
            while(re.Read())
            {
                if(!data.Contains(re.GetString(0)))
                {
                    data.Add(re.GetString(0));
                    groups.Add(re.GetString(0));
                }
            }
            re.Close();
            Disposal.Recycle(cmd, re);

            foreach(string i in groups) _RecurseGroups(i, data);
        }


        /// <summary>Writes a change to database.</summary>
        /// <param name="id">ID.</param>
        protected virtual void _WriteChange(string id)
        {
            IDbCommand cmd = SystemProvider.CreateCommand(_NewChangeCmd);
            cmd.AddParameter(_ChangeBv, id);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }


        /// <summary>Saves changes to a relation.</summary>
        /// <param name="rel">Relation.</param>
        /// <param name="context">Context.</param>
        protected virtual void _SaveRelation(__RelationData rel, Context context)
        {
            if(rel.GetList() == null) return;
            if(rel.GetList().PersistentItems == null) return;
            
            foreach(IProducible i in rel.GetList().Items)
            {
                if(!rel.GetList().PersistentItems.Contains(i))
                {
                    if(rel.Descriptor is __1ToNRelationDescriptor)
                    {
                        IDbCommand cmd = ((__IFactoryTarget) i).EntityDescriptor.Provider.CreateCommand();

                        cmd.CommandText = "UPDATE " + ((__IFactoryTarget) i).EntityDescriptor.Tables.RootTable.TableName + " SET " +
                                          ((__1ToNRelationDescriptor) rel.Descriptor).TargetField.ColumnName + " = " + ((__1ToNRelationDescriptor) rel.Descriptor).TargetField.BindVariableName;

                        ((__IFactoryTarget) i).Fields[((__1ToNRelationDescriptor) rel.Descriptor).TargetField.FieldName].Value = rel.List.Parent.ID;
                        ((__IFactoryTarget) i).Fields[((__1ToNRelationDescriptor) rel.Descriptor).TargetField.FieldName].Virgin = true;

                        cmd.AddParameter(((__1ToNRelationDescriptor) rel.Descriptor).TargetField.BindVariableName, 
                                         (string) ((__IFactoryTarget) i).Fields[((__1ToNRelationDescriptor) rel.Descriptor).TargetField.FieldName].Value);

                        if(((__IFactoryTarget) i).EntityDescriptor.Fields.Contains(__ContentType.JTIME))
                        {
                            cmd.CommandText += (", " + ((__IFactoryTarget) i).EntityDescriptor.Fields.TimeModified.ColumnName + " = " + ((__IFactoryTarget) i).EntityDescriptor.Provider.Parser.CurrentTimestampFunction);
                        }
                        if(((__IFactoryTarget) i).EntityDescriptor.Fields.Contains(__ContentType.JMODIFIED))
                        {
                            cmd.CommandText += (", " + ((__IFactoryTarget) i).EntityDescriptor.Fields.ModifiedBy.ColumnName + " = " + ((__IFactoryTarget) i).EntityDescriptor.Fields.ModifiedBy.BindVariableName);
                            cmd.AddParameter(((__IFactoryTarget) i).EntityDescriptor.Fields.ModifiedBy.BindVariableName, context.Token);
                        }

                        cmd.CommandText += (" WHERE " + ((__IFactoryTarget) i).EntityDescriptor.Fields.IDField.ColumnName + " = " + ((__IFactoryTarget) i).EntityDescriptor.Fields.IDField.BindVariableName);
                        cmd.AddParameter(((__IFactoryTarget) i).EntityDescriptor.Fields.IDField.BindVariableName, i.ID);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();

                        // Todo: set virgin on TargetField...
                        if(((__IFactoryTarget) i).Fields.Virgin) ReleaseLock(i, context);
                    }
                    else
                    {
                        IDbCommand cmd = ((__IFactoryTarget) rel.GetList().Parent).EntityDescriptor.Provider.CreateCommand();

                        cmd.CommandText = "INSERT INTO " + ((__MToNRelationDescriptor) rel.Descriptor).RelationTable.TableName +
                                          "(" + ((__MToNRelationDescriptor) rel.Descriptor).SourceColumn.ColumnName + ", " +
                                          ((__MToNRelationDescriptor) rel.Descriptor).DestinationColumn.ColumnName + ") VALUES (" +
                                          ((__IFactoryTarget) rel.GetList().Parent).EntityDescriptor.Provider.Parser.ToBindVariableName("parent") + ", " +
                                          ((__IFactoryTarget) rel.GetList().Parent).EntityDescriptor.Provider.Parser.ToBindVariableName("child") + ")";
                        cmd.AddParameter(((__IFactoryTarget) rel.GetList().Parent).EntityDescriptor.Provider.Parser.ToBindVariableName("parent"), rel.GetList().Parent.ID);
                        cmd.AddParameter(((__IFactoryTarget) rel.GetList().Parent).EntityDescriptor.Provider.Parser.ToBindVariableName("parent"), i.ID);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                }
            }

            foreach(IProducible i in rel.GetList().PersistentItems)
            {
                if(!rel.GetList().Items.Contains(i))
                {
                    if(rel.Descriptor is __MToNRelationDescriptor)
                    {
                        IDbCommand cmd = ((__IFactoryTarget) rel.GetList().Parent).EntityDescriptor.Provider.CreateCommand();

                        cmd.CommandText = "DELETE FROM " + ((__MToNRelationDescriptor) rel.Descriptor).RelationTable.TableName +
                                          " WHERE " + ((__MToNRelationDescriptor) rel.Descriptor).SourceColumn.ColumnName + " = " +
                                          ((__IFactoryTarget) rel.GetList().Parent).EntityDescriptor.Provider.Parser.ToBindVariableName("parent") + " AND " +
                                          ((__MToNRelationDescriptor) rel.Descriptor).DestinationColumn.ColumnName + " = " +
                                          ((__IFactoryTarget) rel.GetList().Parent).EntityDescriptor.Provider.Parser.ToBindVariableName("child");
                        cmd.AddParameter(((__IFactoryTarget) rel.GetList().Parent).EntityDescriptor.Provider.Parser.ToBindVariableName("parent"), rel.GetList().Parent.ID);
                        cmd.AddParameter(((__IFactoryTarget) rel.GetList().Parent).EntityDescriptor.Provider.Parser.ToBindVariableName("parent"), i.ID);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                    }
                }
            }

            rel.GetList().PersistentItems = null;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IFactory                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the system provider.</summary>
        public virtual IProvider SystemProvider { get; protected set; } = null;


        /// <summary>Gets a list of entity descriptors.</summary>
        public virtual __EntityDescriptorList Entities { get; protected set; } = null;


        /// <summary>Gets the provider cache.</summary>
        public virtual __ProviderCache Providers { get; protected set; } = null;


        /// <summary>Gets the IDs of recently changed objects.</summary>
        /// <returns>IDs of changed objects.</returns>
        public virtual IEnumerable<string> GetChanges()
        {
            DateTime lastRead = DateTime.Now;
            List<string> rval = new List<string>();

            IDbCommand cmd = SystemProvider.CreateCommand(_GetChangeCmd);
            cmd.AddParameter(_ChangeBv, _LastRead);
            _LastRead = lastRead;

            IDataReader re = cmd.ExecuteReader();
            while(re.Read())
            {
                rval.Add(re.GetString(0));
            }
            re.Close();
            Disposal.Dispose(re, cmd);

            return rval;
        }


        /// <summary>Removes timed out entries from change table.</summary>
        public virtual void PurgeChange()
        {
            IDbCommand cmd = SystemProvider.CreateCommand(_PurgeChangeCmd);
            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }


        /// <summary>Loads database data into an object.</summary>
        /// <param name="p">Producible object.</param>
        /// <param name="re">Database reader.</param>
        /// <param name="force">Forces refresh for modified objects.</param>
        public virtual void RefreshObject(IProducible p, IDataReader re, bool force)
        {
            if(force || ((__IFactoryTarget) p).Fields.Virgin)
            {
                ((__IFactoryTarget) p).Fields.Read(re);
            }
        }


        /// <summary>Produces an object.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Producible object.</returns>
        public virtual IProducible ProduceObject(string id)
        {
            if(string.IsNullOrWhiteSpace(id)) { return null; }

            IProducible rval = __Cache.Instance.Retrieve(id);

            if(rval == null)
            {
                __EntityDescriptor entity = Entities[id];

                IDbCommand cmd = entity.Provider.CreateCommand();

                ISQLQuery query = entity.CreateQuery();
                query.And(entity.Fields.IDField.ColumnName + " = " + entity.Provider.Parser.ToBindVariableName("id"));

                cmd.CommandText = query.ToString();
                cmd.AddParameter(entity.Provider.Parser.ToBindVariableName("id"), id);

                IDataReader re = cmd.ExecuteReader();
                if(re.Read())
                {
                    rval = entity.Spawn(re);
                }
                re.Close();

                Disposal.Dispose(re, cmd);
            }

            return rval;
        }


        /// <summary>Produces an object from an SQL query.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="query">SQL Query.</param>
        /// <returns>Producible object.</returns>
        public virtual T ProduceObject<T>(ISQLQuery query)
        {
            __EntityDescriptor entity = Entities[typeof(T)];
            IProducible rval = null;

            IDbCommand cmd = entity.Provider.CreateCommand();

            cmd.CommandText = query.ToString();
            cmd.AddParameters(query.Parameters);

            IDataReader re = cmd.ExecuteReader();
            if(re.Read())
            {
                rval = (IProducible) ProduceObject<T>(re);
            }
            re.Close();

            Disposal.Dispose(re, cmd);

            return (T) rval;
        }


        /// <summary>Produces an object from a database reader.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="re">Database reader.</param>
        /// <returns>Producible object.</returns>
        public virtual T ProduceObject<T>(IDataReader re)
        {
            __EntityDescriptor entity = Entities[typeof(T)];
            IProducible rval = __Cache.Instance.Retrieve(re.GetString(entity.Fields.IDField.ColumnAlias));

            if(rval == null)
            {
                rval = entity.Spawn(re);
            }

            return (T) rval;
        }


        /// <summary>Produces a single instance from an object comparison.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="expression">Expression.</param>
        /// <returns>Instance. Returns NULL when no object matches.</returns>
        public virtual T ProduceObject<T>(Expression<Func<T, bool>> expression) where T: IProducible
        {
            __EntityDescriptor entity = Entities[typeof(T)];

            IDbCommand cmd = entity.Provider.CreateCommand();
            IProducible rval = null;

            ISQLQuery query = entity.ExpressionParser.Parse(expression);
            cmd.CommandText = query.ToString();
            cmd.AddParameters(query.Parameters);

            IDataReader re = cmd.ExecuteReader();
            if(re.Read())
            {
                rval = ProduceObject<T>(re);
            }
            re.Close();

            Disposal.Dispose(re, cmd);

            return (T) rval;
        }


        /// <summary>Produces a list of objects from a SQL query.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="query">SQL query.</param>
        /// <returns>List.</returns>
        public virtual List<IProducible> Populate<T>(ISQLQuery query) where T: IProducible
        {
            List<IProducible> rval = new List<IProducible>();

            IDbCommand cmd = Entities[typeof(T)].Provider.CreateCommand(query.ToString());
            cmd.AddParameters(query.Parameters);

            IDataReader re = cmd.ExecuteReader();

            while(re.Read())
            {
                rval.Add(ProduceObject<T>(re));
            }
            Disposal.Dispose(re, cmd);

            return rval;
        }


        /// <summary>Sets a user password.</summary>
        /// <param name="user">User.</param>
        /// <param name="password">New password.</param>
        /// <param name="context">Context.</param>
        /// <exception cref="InsufficientPrivilegesException">Thrown if the context lacks privileges for the desired operation.</exception>
        public virtual void SetPassword(User user, string password, Context context)
        {
            if(user != context.User)
            {
                user.Ops.Probe(Op.WRITE, context);
            }

            __EntityDescriptor entity = Entities[typeof(User)];
            string salt = StringOp.Random(entity.Fields["Salt"].Length);

            IDbCommand cmd = entity.Provider.CreateCommand();
            cmd.CommandText = "UPDATE " + entity.Tables.RootTable.TableName + " SET " + entity.Fields["Salt"].ColumnName + " = " + entity.Fields["Salt"].BindVariableName + ", " +
                                                                                        entity.Fields["Password"].ColumnName + " = " + entity.Fields["Password"].BindVariableName +
                                                                            " WHERE " + entity.Fields.IDField.ColumnName + " = " + entity.Fields.IDField.BindVariableName;
            cmd.AddParameter(entity.Fields["Salt"].BindVariableName, salt);
            cmd.AddParameter(entity.Fields["Password"].BindVariableName, SHA256.GetHash(salt + password));
            cmd.AddParameter(entity.Fields.IDField.BindVariableName, user.ID);

            cmd.ExecuteNonQuery();
            cmd.Dispose();

            TouchObject(user, context);

            Journal.Write(user, Journal.Severity.INFORMATION, Journal.Operation.MODIFY, "Password changed.", user.Name, session: context);
        }


        /// <summary>Unsets a user password.</summary>
        /// <param name="user">User.</param>
        /// <param name="context">Context.</param>
        public virtual void UnsetPassword(User user, Context context)
        {
            if(user != context.User)
            {
                user.Ops.Probe(Op.WRITE, context);
            }

            __EntityDescriptor entity = Entities[typeof(User)];

            IDbCommand cmd = entity.Provider.CreateCommand();
            cmd.CommandText = "UPDATE " + entity.Tables.RootTable.TableExpression + " SET " + entity.Fields["Salt"].ColumnExpression + " = NULL," +
                                                                                              entity.Fields["Password"].ColumnExpression + " = NULL" + entity.Fields["Password"].BindVariableName +
                                                                                    " WHERE " + entity.Fields.IDField.ColumnExpression + " = " + entity.Fields.IDField.BindVariableName;
            cmd.AddParameter(entity.Fields.IDField.BindVariableName, user.ID);

            cmd.ExecuteNonQuery();
            cmd.Dispose();

            TouchObject(user, context);

            Journal.Write(user, Journal.Severity.INFORMATION, Journal.Operation.MODIFY, "Password unsert.", user.Name, session: context);
        }


        /// <summary>Creates an authenticed context with user name and password.</summary>
        /// <param name="userName">User name.</param>
        /// <param name="password">Password.</param>
        /// <returns>Context.</returns>
        /// <remarks>Returns NULL when credentials could not be verified.</remarks>
        public virtual Context CreateContext(string userName, string password)
        {
            Context rval = null;
            __EntityDescriptor userEntity = Entities[typeof(User)];
            ISQLQuery q = userEntity.CreateQuery().Where(userEntity.Fields["Name"].ColumnExpression + " = " + userEntity.Fields["Name"].BindVariableName);
            q.AddParameter(userEntity.Fields["Name"].BindVariableName, userName);

            IDbCommand cmd = userEntity.Provider.CreateCommand(q.Text);
            cmd.AddParameters(q.Parameters);

            IDataReader re = cmd.ExecuteReader();

            if(re.Read())
            {
                if(re.GetString(userEntity.Fields["Password"].ColumnAlias) == SHA256.GetHash(re.GetString(userEntity.Fields["Salt"].ColumnAlias) + password))
                {
                    rval = _RegisterContext(ProduceObject<User>(re));
                }
                else { Journal.Write(Journal.Severity.WARNING, Journal.Operation.AUTHENTICATION, "User \"" + userName + "\" failed to log in.", userName); }
            }
            re.Close();
            Disposal.Dispose(re, cmd);

            return rval;
        }


        /// <summary>Creates an authenticed context with system credentials.</summary>
        /// <returns>Context.</returns>
        /// <remarks>Returns NULL when credentials could not be verified.</remarks>
        public virtual Context CreateContext()
        {
            Context rval = null;
            __EntityDescriptor userEntity = Entities[typeof(User)];
            ISQLQuery q = userEntity.CreateQuery().Where(userEntity.Provider.Parser.Lowercase(userEntity.Fields["OSUserName"].ColumnExpression) + " = " + userEntity.Fields["OSUserName"].BindVariableName)
                                                  .And(userEntity.Provider.Parser.Lowercase(userEntity.Fields["OSDomain"].ColumnExpression) + " = " + userEntity.Fields["OSDomain"].BindVariableName);
            q.AddParameter(userEntity.Fields["OSUserName"].BindVariableName, Environment.UserName.ToLower());
            q.AddParameter(userEntity.Fields["OSDomain"].BindVariableName, Environment.UserDomainName.ToLower());

            IDbCommand cmd = userEntity.Provider.CreateCommand(q.Text);
            cmd.AddParameters(q.Parameters);

            IDataReader re = cmd.ExecuteReader();

            if(re.Read())
            {
                rval = _RegisterContext(ProduceObject<User>(re));
            }
            else { Journal.Write(Journal.Severity.WARNING, Journal.Operation.AUTHENTICATION, "OS user \"" + Environment.UserName + "@" + Environment.UserDomainName + "\" failed to log in.", Environment.UserName); }

            re.Close();
            Disposal.Dispose(re, cmd);

            return rval;
        }


        /// <summary>Saves an object.</summary>
        /// <param name="p">Producible object.</param>
        /// <param name="context">Context.</param>
        /// <exception cref="InsufficientPrivilegesException">Thrown if the context lacks privileges for the desired operation.</exception>
        public virtual void SaveObject(IProducible p, Context context)
        {
            // TODO: check, check out journal
            p.Ops.Probe(Op.WRITE, context);

            if(!((__IFactoryTarget) p).Fields.Virgin)
            {
                ((__IFactoryTarget) p).Fields.Update(context);

                if(p.ID == null)
                {
                    ((__IFactoryTarget) p).Fields["ID"].Value = ((__IFactoryTarget) p).EntityDescriptor.EntityID + "." +
                                                                ((__IFactoryTarget) p).EntityDescriptor.Provider.NextValue(((__IFactoryTarget) p).EntityDescriptor.SequenceName).ToAlpha();

                    foreach(__TableDescriptor i in ((__IFactoryTarget) p).EntityDescriptor.Tables)
                    {
                        IDbCommand cmd = null;

                        StringBuilder cols = new StringBuilder();
                        StringBuilder vars = new StringBuilder();
                        bool first = true;
                        foreach(__FieldData j in ((__IFactoryTarget) p).Fields)
                        {
                            if(j.Descriptor.Table != i) continue;
                            if(first)
                            {
                                first = false;
                                cmd = ((__IFactoryTarget) p).EntityDescriptor.Provider.CreateCommand("INSERT INTO " + i.TableName);
                            }
                            else
                            {
                                cols.Append(", ");
                                vars.Append(", ");
                            }

                            cols.Append(j.Descriptor.ColumnName);
                            vars.Append(j.Descriptor.BindVariableName);
                            cmd.AddParameter(j.Descriptor.BindVariableName, j.Value);
                        }

                        if(!first)
                        {
                            if(!i.Root)
                            {
                                cols.Append(", ");
                                cols.Append(i.PrimaryKey);
                                vars.Append(", ");
                                vars.Append(((__IFactoryTarget) p).EntityDescriptor.Provider.Parser.ToBindVariableName("id"));
                                cmd.AddParameter(((__IFactoryTarget) p).EntityDescriptor.Provider.Parser.ToBindVariableName("id"), p.ID);
                            }
                            cmd.CommandText += "(" + cols.ToString() + ") VALUES (" + vars.ToString() + ")";
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();
                        }

                        Journal.Write(p, Journal.Severity.NOTIFICATION, Journal.Operation.CREATE, "Object created.", session: context);
                    }
                }
                else
                {
                    foreach(__TableDescriptor i in ((__IFactoryTarget) p).EntityDescriptor.Tables)
                    {
                        IDbCommand cmd = null;

                        StringBuilder vars = new StringBuilder();
                        StringBuilder data = new StringBuilder();
                        bool first = true;

                        foreach(__FieldData j in ((__IFactoryTarget) p).Fields)
                        {
                            if(j.Virgin || (j.Descriptor.Table != i)) continue;
                            if(first)
                            {
                                first = false;
                                cmd = ((__IFactoryTarget) p).EntityDescriptor.Provider.CreateCommand("UPDATE " + i.TableName + " SET ");
                            }
                            else
                            {
                                vars.Append(", ");
                                data.Append(", ");
                            }

                            data.Append(j.Descriptor.FieldName + "=" + j.Value.ToString());
                            vars.Append(j.Descriptor.ColumnName + " = " + j.Descriptor.BindVariableName);
                            cmd.AddParameter(j.Descriptor.BindVariableName, j.Value);
                        }

                        if(!first)
                        {
                            cmd.CommandText += vars.ToString() + " WHERE " + i.PrimaryKey + " = " + ((__IFactoryTarget) p).EntityDescriptor.Provider.Parser.ToBindVariableName("id");
                            cmd.AddParameter(((__IFactoryTarget) p).EntityDescriptor.Provider.Parser.ToBindVariableName("id"), p.ID);
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();

                            Journal.Write(p, Journal.Severity.NOTIFICATION, Journal.Operation.MODIFY, "Object modified.", data: data.ToString(), session: context);
                        }
                    }
                }
            }

            foreach(__RelationData i in ((__IFactoryTarget) p).Relations)
            {
                _SaveRelation(i, context);
            }

            ReleaseLock(p, context, false);
            _WriteChange(p.ID);
        }


        /// <summary>Marks an object as modified.</summary>
        /// <param name="p">Producible object.</param>
        /// <param name="context">Context.</param>
        public virtual void TouchObject(IProducible p, Context context)
        {
            __EntityDescriptor entity = Entities[p.GetType()];

            IDbCommand cmd = entity.Provider.CreateCommand("UPDATE " + entity.Tables.RootTable + " SET " + entity.Fields["TimeModified"].ColumnName + " = " + entity.Provider.Parser.CurrentTimestampFunction + ", " +
                                                           entity.Fields["ModifiedBy"].ColumnName + " = " + entity.Fields["ModifiedBy"].BindVariableName +
                                                           " WHERE " + entity.Fields.IDField.ColumnName + " = " + entity.Fields.IDField.BindVariableName);
            cmd.AddParameter(entity.Fields["ModifiedBy"].BindVariableName, context.Token);
            cmd.AddParameter(entity.Fields.IDField.BindVariableName, p.ID);

            cmd.ExecuteNonQuery();
            cmd.Dispose();

            _WriteChange(p.ID);
        }


        /// <summary>Creates a lock on an object.</summary>
        /// <param name="p">Producible object.</param>
        /// <param name="context">Context.</param>
        /// <exception cref="ObjectLockedException">Thrown when the object is already locked.</exception>
        public virtual void CreateLock(IProducible p, Context context)
        {
            if(p.ID != null)
            {
                Lock l = GetLock(p);

                if(l.Session != null)
                {
                    if(l._ThreadID == Thread.CurrentThread.ManagedThreadId) return;
                    if(l.Session == context)
                    {
                        l._ThreadID = Thread.CurrentThread.ManagedThreadId;
                        return;
                    }

                    ReleaseLock(p, context);
                }

                __EntityDescriptor entity = Entities[typeof(Lock)];
                IDbCommand cmd = entity.Provider.CreateCommand("INSERT INTO " + entity.Tables.RootTable.TableName + " (" + entity.Fields["Object"].ColumnName + ", " + entity.Fields["Session"].ColumnName + ", " + entity.Fields["TimeModified"].ColumnName +
                                                                                                       ") VALUES (" + entity.Fields["Object"].BindVariableName + ", " + entity.Fields["Session"].BindVariableName + ", " + entity.Provider.Parser.CurrentTimestampFunction + ")");
                cmd.AddParameter(entity.Fields["Object"].BindVariableName, p.ID);
                cmd.AddParameter(entity.Fields["Session"].BindVariableName, context.Token);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }

            ((__IFactoryTarget) p).LockObject = new Lock(p, context);
        }


        /// <summary>Releases a lock on an object.</summary>
        /// <param name="p">Producible object.</param>
        /// <param name="context">Context.</param>
        /// <param name="force">Allows administrative contextes to forcefully remove locks held by other user contextes.</param>
        /// <exception cref="ObjectLockedException">Thrown when the object couldn't be unlocked.</exception>
        public virtual void ReleaseLock(IProducible p, Context context, bool force = false)
        {
            CleanUpLocks();
            Lock l = GetLock(p);

            if(l.Session != null)
            {
                if(l.Session == context) // TODO: || (force && admin)
                {
                    __EntityDescriptor entity = Entities[typeof(Lock)];
                    IDbCommand cmd = entity.Provider.CreateCommand("DELETE FROM " + entity.Tables.RootTable.TableName + " WHERE " + entity.Fields["Object"].ColumnName + " = " + entity.Fields["Object"].BindVariableName);
                    cmd.AddParameter(entity.Fields["Object"].BindVariableName, p.ID);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }
                else
                {
                    throw new ObjectLockedException(l);
                }
            }
        }


        /// <summary>Gets a lock object that represents a lock on a specific object.</summary>
        /// <param name="p">Producible object</param>
        /// <returns>Lock object.</returns>
        public virtual Lock GetLock(IProducible p)
        {
            if(((__IFactoryTarget) p).LockObject != null) { return ((__IFactoryTarget) p).LockObject; }

            __EntityDescriptor entity = Entities[typeof(Lock)];
            IDbCommand cmd = entity.Provider.CreateCommand("SELECT " + entity.Fields["Session"].ColumnName + " FROM " + entity.Tables.RootTable.TableName +
                                                           " WHERE " + entity.Fields["Object"].ColumnName + " = " + entity.Fields["Object"].BindVariableName);
            cmd.AddParameter(entity.Fields["Object"].BindVariableName, p.ID);
            IDataReader re = cmd.ExecuteReader();
            Session session = null;

            if(re.Read())
            {
                session = Factory.Produce<Session>(re.GetString(0));
                if(session == null) { session = new Session(re.GetString(0)); }
            }
            re.Close();
            Disposal.Dispose(re, cmd);

            return new Lock(p, session);
        }


        /// <summary>Removes timed-out contextes from the system.</summary>
        public virtual void PurgeSessions()
        {
            __EntityDescriptor entity = Entities["0"];

            IDbCommand cmd = entity.Provider.CreateCommand("DELETE FROM " + entity.Tables.RootTable.TableName + " WHERE " + entity.Fields["TimeModified"].ColumnName + " < " +
                             entity.Provider.Parser.AddTimeInterval(entity.Provider.Parser.CurrentTimestampFunction, new TimeSpan(0, -Variable.ByName("system.sessions.timeout").IntegerValue, 0)));
            int n = cmd.ExecuteNonQuery();
            cmd.Dispose();

            if(n > 0)
            {
                Journal.Write(Journal.Severity.INFORMATION, Journal.Operation.GENERAL, n.ToString() + " sessions closed due to timeout.", session: Factory.Produce<Session>("0.0"));
            }
            CleanUpLocks();
        }


        /// <summary>Releases orphan and timed out locks.</summary>
        public virtual void CleanUpLocks()
        {
            int t = -(__Repository.Instance.RepositoryFile.Sections["locks"].GetInteger("timeout", 20));

            __EntityDescriptor lockEntity = Entities[typeof(Lock)];
            __EntityDescriptor sessionEntity = Entities["0"];
            IDbCommand cmd = lockEntity.Provider.CreateCommand("DELETE FROM " + lockEntity.Tables.RootTable.TableName + " WHERE " + lockEntity.Fields["TimeModified"].ColumnName + " < " + lockEntity.Provider.Parser.AddTimeInterval(lockEntity.Provider.Parser.CurrentTimestampFunction, new TimeSpan(0, t, 0)));
            cmd.ExecuteNonQuery();
            cmd.Dispose();

            cmd = lockEntity.Provider.CreateCommand("DELETE FROM " + lockEntity.Tables.RootTable.TableName + " WHERE " + lockEntity.Fields["Session"].ColumnName + " NOT IN (" +
                                                "SELECT " + sessionEntity.Fields.IDField.ColumnName + " FROM " + sessionEntity.Tables.RootTable.TableName + ")");
            int n = cmd.ExecuteNonQuery();
            Disposal.Recycle(cmd);

            if((n > 0) && Journal.Settings.Debug)
            {
                Journal.Write(Journal.Severity.DEBUG, Journal.Operation.GENERAL, n.ToString() + " orphan locks released.");
            }
        }


        /// <summary>Loads operation descriptions from database.</summary>
        /// <param name="entity">Entity descriptor.</param>
        /// <returns>Operations dictionary.</returns>
        public virtual Dictionary<Op, string> LoadOps(__EntityDescriptor entity)
        {
            Dictionary<Op, string> rval = new Dictionary<Op, string>();
            __EntityDescriptor opEntity = Entities[typeof(Op)];

            ISQLQuery q = opEntity.CreateQuery().From(opEntity.Tables.RootTable).Select(opEntity.Fields["OpCode"], opEntity.Fields["Privilege"]).Where(opEntity.Fields["Object"].ColumnExpression + " = " + opEntity.Provider.Parser.ToBindVariableName("id"));
            IDbCommand cmd = opEntity.Provider.CreateCommand(q.Text);
            cmd.AddParameter(opEntity.Provider.Parser.ToBindVariableName("id"), entity.EntityID);
            IDataReader re = cmd.ExecuteReader();

            while(re.Read())
            {
                rval.Add(Op.FromString(re.GetString(opEntity.Fields["OpCode"].ColumnAlias)), re.GetString(opEntity.Fields["Privilege"].ColumnAlias));
            }
            re.Close();
            Disposal.Dispose(re, cmd);

            q = opEntity.CreateQuery().From(opEntity.Tables.RootTable).Select(opEntity.Fields["OpCode"]).Distinct().Where(opEntity.Fields["Object"].ColumnExpression + " LIKE " + opEntity.Provider.Parser.ToBindVariableName("id"));
            cmd = opEntity.Provider.CreateCommand(q.Text);
            cmd.AddParameter(opEntity.Provider.Parser.ToBindVariableName("id"), entity.EntityID + ".%");
            re = cmd.ExecuteReader();

            while(re.Read())
            {
                rval.Add(Op.FromString(re.GetString(0)), "*");
            }
            re.Close();
            Disposal.Dispose(re, cmd);

            return rval;
        }


        /// <summary>Returns the required privilege for a specific operation on an object.</summary>
        /// <param name="p">Producible object.</param>
        /// <param name="op">Operation.</param>
        /// <returns>Privilege.</returns>
        public virtual Privilege GetObjectPrivilege(IProducible p, Op op)
        {
            Privilege rval = null;
            __EntityDescriptor opEntity = Entities[typeof(Op)];

            ISQLQuery q = opEntity.CreateQuery().From(opEntity.Tables.RootTable).Select(opEntity.Fields["OpCode"]).Distinct().Where(opEntity.Fields["Object"].ColumnExpression + " = " + opEntity.Provider.Parser.ToBindVariableName("id"));
            IDbCommand cmd = opEntity.Provider.CreateCommand(q.Text);
            cmd.AddParameter(opEntity.Provider.Parser.ToBindVariableName("id"), p.ID);
            IDataReader re = cmd.ExecuteReader();

            if(re.Read())
            {
                rval = (Privilege) ProduceObject(re.GetString(0));
            }
            re.Close();
            Disposal.Dispose(re, cmd);

            return rval;
        }


        /// <summary>Returns all privileges held by a user object.</summary>
        /// <param name="u">User.</param>
        /// <returns>List of privileges.</returns>
        public virtual IProducibleList<Privilege> GetPrivileges(User u)
        {
            Dictionary<string, IProducible> rval = new Dictionary<string, IProducible>();
            List<string> groups = new List<string>();

            _RecursePrivileges(u.ID, rval);
            _RecurseGroups(u.ID, groups);
            foreach(string i in groups) _RecursePrivileges(i, rval);

            return new ProducibleList<Privilege>(rval);
        }


        /// <summary>Gets a user variable.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="user">User.</param>
        /// <param name="variable">Variable.</param>
        /// <param name="context">Context.</param>
        /// <returns>Variable value.</returns>
        public virtual T GetUserVariable<T>(User user, Variable variable, Context context)
        {
            if(user != context.User) { user.Ops.Probe(Op.READ, context); }
            if(variable.Global) { return variable.GetValue<T>(); }

            string rval = variable.StringValue;
            __EntityDescriptor entity = Entities["UserVariable*"];
            ISQLQuery query = entity.Provider.Parser.CreateQuery().From(entity.Tables.RootTable).Select(entity.Fields["Value"])
                                                                  .Where(entity.Fields["User"].ColumnExpression + " = " + entity.Fields["User"].BindVariableName)
                                                                  .And(entity.Fields["Variable"].ColumnExpression + " = " + entity.Fields["Variable"].BindVariableName);
            query.AddParameter(entity.Fields["User"].BindVariableName, user.ID)
                 .AddParameter(entity.Fields["Variable"].BindVariableName, variable.ID);

            IDbCommand cmd = entity.Provider.CreateCommand(query);

            IDataReader re = cmd.ExecuteReader();
            if(re.Read())
            {
                rval = re.GetString(0);
            }
            re.Close();
            Disposal.Dispose(re, cmd);

            if(typeof(T) == typeof(string)) { return (T) Convert.ChangeType(rval, typeof(T)); }
            if(typeof(T) == typeof(bool)) { return (T) Convert.ChangeType(rval.ToBoolean(), typeof(T)); }
            if(typeof(T) == typeof(int)) { return (T) Convert.ChangeType(rval.ToInteger(), typeof(T)); }
            if(typeof(T) == typeof(DateTime)) { return (T) Convert.ChangeType(rval.ParseTimestamp(), typeof(T)); }

            throw new InvalidCastException("Variable implementation does not support type \"" + typeof(T).Name + "\".");
        }


        /// <summary>Sets a user variable.</summary>
        /// <param name="user">User.</param>
        /// <param name="variable">Variable.</param>
        /// <param name="value">Value.</param>
        /// <param name="context">Context.</param>
        public virtual void SetUserVariable(User user, Variable variable, object value, Context context)
        {
            if(user != context.User) { user.Ops.Probe(Op.WRITE, context); }
            if(variable.Global) { throw new InvalidOperationException("Global variables do not support user level values."); }

            __EntityDescriptor entity = Entities["UserVariable*"];

            if((value is string) || (value is bool) || (value is int) || (value is DateTime))
            {
                Journal.Write(user, variable, Journal.Severity.INFORMATION, Journal.Operation.MODIFY, "User variable \"" + variable.Name + "\" set.", user.Name + ", " + variable.Name, "value=" + value.ToString() + ";", "", context);

                string strv = null;
                if(value is string) { strv = (string) value; }
                if(value is bool) { strv = ((bool) value).ToBooleanString(); }
                if(value is int) { strv = ((int) value).ToString(); }
                if(value is DateTime) { strv = ((DateTime) value).ToTimestamp(); }

                IDbCommand cmd = entity.Provider.CreateCommand();
                cmd.CommandText = "UPDATE " + entity.Tables.RootTable.TableName + " SET " + entity.Fields["Value"].ColumnName + " = " + entity.Fields["Value"].BindVariableName +
                                  "WHERE " + entity.Fields["User"].ColumnName + " = " + entity.Fields["User"].BindVariableName +
                                  "AND " + entity.Fields["Variable"].ColumnName + " = " + entity.Fields["Variable"].BindVariableName;
                cmd.AddParameter(entity.Fields["Value"].BindVariableName, strv);
                cmd.AddParameter(entity.Fields["User"].BindVariableName, user.ID);
                cmd.AddParameter(entity.Fields["Variable"].BindVariableName, variable.ID);

                int n = cmd.ExecuteNonQuery();

                if(n < 1)
                {
                    cmd.Dispose();
                    cmd = entity.Provider.CreateCommand();

                    cmd.CommandText = "INSERT INTO " + entity.Tables.RootTable.TableName + " (" + entity.Fields["User"].ColumnName + ", " +
                                                                                                  entity.Fields["Variable"].ColumnName + ", " +
                                                                                                  entity.Fields["Value"].ColumnName + ") VALUES (" +
                                                                                                  entity.Fields["User"].BindVariableName + ", " +
                                                                                                  entity.Fields["Variable"].BindVariableName + ", " +
                                                                                                  entity.Fields["Value"].BindVariableName + ")";
                    cmd.AddParameter(entity.Fields["User"].BindVariableName, user.ID);
                    cmd.AddParameter(entity.Fields["Variable"].BindVariableName, variable.ID);
                    cmd.AddParameter(entity.Fields["Value"].BindVariableName, strv);

                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                }

                TouchObject(user, context);
            }
            else { throw new InvalidCastException("Variable implementation does not support type \"" + value.GetType().Name + "\"."); }
        }


        /// <summary>Writes a journal entry.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="referenced">Referenced object.</param>
        /// <param name="severity">Severity level.</param>
        /// <param name="operation">Operation flag.</param>
        /// <param name="flags">Additional flags.</param>
        /// <param name="reserved">Reserved flags.</param>
        /// <param name="message">Journal entry message.</param>
        /// <param name="tags">Journal entry tags.</param>
        /// <param name="data">Journal entry data.</param>
        /// <param name="additional">Additional information.</param>
        /// <param name="session">Session.</param>
        public virtual void WriteEntry(IProducible obj, IProducible referenced, Journal.Severity severity, Journal.Operation operation, int flags, int reserved, string message, string tags, string data, string additional, ISession session)
        {
            if(!Journal.Settings.Severities.Contains(severity)) return;
            if(!Journal.Settings.Operations.Contains(operation)) return;

            __EntityDescriptor entity = Entities[typeof(JournalEntry)];

            IDbCommand cmd = entity.Provider.CreateCommand();
            cmd.CommandText = "INSERT INTO " + entity.Tables.RootTable.TableName + " (" + entity.Fields["Object"].ColumnName + ", " +
                                                                                          entity.Fields["ReferencedObject"].ColumnName + ", " +
                                                                                          entity.Fields["Severity"].ColumnName + ", " +
                                                                                          entity.Fields["Operation"].ColumnName + ", " +
                                                                                          entity.Fields["Flags"].ColumnName + ", " +
                                                                                          entity.Fields["Reserved"].ColumnName + ", " +
                                                                                          entity.Fields["Message"].ColumnName + ", " +
                                                                                          entity.Fields["Tags"].ColumnName + ", " +
                                                                                          entity.Fields["Data"].ColumnName + ", " +
                                                                                          entity.Fields["AdditionalInformation"].ColumnName + ", " +
                                                                                          entity.Fields["Session"].ColumnName + ", " +
                                                                                          entity.Fields["Time"].ColumnName + ") VALUES (" +
                                                                                          entity.Fields["Object"].BindVariableName + ", " +
                                                                                          entity.Fields["ReferencedObject"].BindVariableName + ", " +
                                                                                          entity.Fields["Severity"].BindVariableName + ", " +
                                                                                          entity.Fields["Operation"].BindVariableName + ", " +
                                                                                          entity.Fields["Flags"].BindVariableName + ", " +
                                                                                          entity.Fields["Reserved"].BindVariableName + ", " +
                                                                                          entity.Fields["Message"].BindVariableName + ", " +
                                                                                          entity.Fields["Tags"].BindVariableName + ", " +
                                                                                          entity.Fields["Data"].BindVariableName + ", " +
                                                                                          entity.Fields["AdditionalInformation"].BindVariableName + ", " +
                                                                                          entity.Fields["Session"].BindVariableName + ", " +
                                                                                          entity.Provider.Parser.CurrentTimestampFunction + ")";
            cmd.AddParameter(entity.Fields["Object"].BindVariableName, obj?.ID);
            cmd.AddParameter(entity.Fields["ReferencedObject"].BindVariableName, referenced?.ID);
            cmd.AddParameter(entity.Fields["Severity"].BindVariableName, (int) severity);
            cmd.AddParameter(entity.Fields["Operation"].BindVariableName, (int) operation);
            cmd.AddParameter(entity.Fields["Flags"].BindVariableName, flags);
            cmd.AddParameter(entity.Fields["Reserved"].BindVariableName, reserved);
            cmd.AddParameter(entity.Fields["Message"].BindVariableName, message);
            cmd.AddParameter(entity.Fields["Tags"].BindVariableName, tags);
            cmd.AddParameter(entity.Fields["Data"].BindVariableName, data);
            cmd.AddParameter(entity.Fields["AdditionalInformation"].BindVariableName, additional);
            cmd.AddParameter(entity.Fields["Session"].BindVariableName, session);

            cmd.ExecuteNonQuery();
            cmd.Dispose();
        }


        /// <summary>Gets a set of journal entries.</summary>
        /// <param name="exp">Expression.</param>
        /// <returns>Journal entries.</returns>
        public virtual IEnumerable<JournalEntry> GetEntries(Expression<Func<JournalEntry, bool>> exp)
        {
            __EntityDescriptor entity = Entities[typeof(JournalEntry)];

            List<JournalEntry> rval = new List<JournalEntry>();

            IDbCommand cmd = entity.Provider.CreateCommand(entity.ExpressionParser.Parse(exp));
            IDataReader re = cmd.ExecuteReader();

            while(re.Read())
            {
                JournalEntry m = new JournalEntry();

                m.Object = Instance.ProduceObject(re.GetString(entity.Fields["Object"].ColumnAlias));
                m.ReferencedObject = Instance.ProduceObject(re.GetString(entity.Fields["ReferencedObject"].ColumnAlias));
                m.Severity = (Journal.Severity) re.GetInteger(entity.Fields["Severity"].ColumnAlias);
                m.Operation = (Journal.Operation) re.GetInteger(entity.Fields["Operation"].ColumnAlias);
                m.Flags = re.GetInteger(entity.Fields["Flags"].ColumnAlias);
                m.Reserved = re.GetInteger(entity.Fields["Reserved"].ColumnAlias);
                m.Message = re.GetString(entity.Fields["Message"].ColumnAlias);
                m.Tags = re.GetString(entity.Fields["Tags"].ColumnAlias);
                m.Data = re.GetString(entity.Fields["Data"].ColumnAlias);
                m.AdditionalInformation = re.GetString(entity.Fields["AdditionalInformation"].ColumnAlias);

                rval.Add(m);
            }

            return rval;
        }


        /// <summary>Clears a list.</summary>
        /// <param name="list">List.</param>
        public virtual void ClearList(__IFactoryListTarget list)
        {
            if(list.PersistentItems == null) { list.PersistentItems = new List<IProducible>((IEnumerable<IProducible>) list.Items); }
        }


        /// <summary>Assigns an object to a target in a relation.</summary>
        /// <param name="list">Relation list.</param>
        /// <param name="parent">Parent object.</param>
        /// <param name="child">Child object.</param>
        /// <exception cref="ObjectLockedException">Thrown when the object is locked.</exception>
        public virtual void Assign(__IFactoryListTarget list, IProducible parent, IProducible child)
        {
            if(list.Items.Contains(child)) return;

            if(list.Relation == null)
            {
                list.Items.Add(child);
                if(list.Index != null) { list.Index.Add(child.ID, child); }
            }
            else if(list.Relation is __1ToNRelationDescriptor)
            {
                Lock plck = parent.GetLock();
                parent.Lock();

                try
                {
                    child.Lock();
                }
                catch(Exception)
                {
                    if(plck == null) { parent.GetLock().Release(); }
                    throw;
                }

                __IFactoryTarget p = (__IFactoryTarget) Factory.Produce((string) ((__IFactoryTarget) child).Fields[((__1ToNRelationDescriptor) list.Relation).TargetField.FieldName].Value);
                if(p.Relations[list.Relation.Name].GetList() != null)
                {
                    if((p.Relations[list.Relation.Name].GetList().Items != null) && (p.Relations[list.Relation.Name].GetList().Items.Contains(child))) { p.Relations[list.Relation.Name].GetList().Items.Remove(child); }
                    if((p.Relations[list.Relation.Name].GetList().PersistentItems != null) && (p.Relations[list.Relation.Name].GetList().PersistentItems.Contains(child))) { p.Relations[list.Relation.Name].GetList().PersistentItems.Remove(child); }
                    if(p.Relations[list.Relation.Name].GetList().Index.ContainsKey(child.ID)) { p.Relations[list.Relation.Name].GetList().Index.Remove(child.ID); }
                }

                if(list.PersistentItems == null) { list.PersistentItems = new List<IProducible>(list.Items); }
                list.Items.Add(child);
                if(list.Index != null) { list.Index.Add(child.ID, child); }
            }
            else
            {
                parent.Lock();

                foreach(__RelationData i in ((__IFactoryTarget) child).Relations)
                {
                    if(i.Descriptor is __MToNRelationDescriptor)
                    {
                        if((i.Descriptor.TargetEntity == ((__IFactoryTarget) parent).EntityDescriptor) && (((__MToNRelationDescriptor) i.Descriptor).RelationTable.TableName == ((__MToNRelationDescriptor) list.Relation).RelationTable.TableName))
                        {
                            i.GetList().Load();
                            if(i.GetList().PersistentItems == null) { i.GetList().PersistentItems = new List<IProducible>(i.GetList().Items); }
                            if(!i.GetList().Items.Contains(parent)) { i.GetList().Items.Add(parent); }
                            if(!i.GetList().PersistentItems.Contains(parent)) { i.GetList().PersistentItems.Add(parent); }
                            if((i.GetList().Index != null) && (!i.GetList().Index.ContainsKey(child.ID))) { i.GetList().Index.Add(child.ID, child); }
                            break;
                        }
                    }
                }

                list.Items.Add(child);
                if(list.Index != null) { list.Index.Add(child.ID, child); }
            }
        }


        /// <summary>Unassigns an object from a target in a relation.</summary>
        /// <param name="list">Relation list.</param>
        /// <param name="parent">Parent object.</param>
        /// <param name="child">Child object.</param>
        /// <exception cref="IntegrityViolationException">Thrown when an object is removed from a 1:n relation.</exception>
        public virtual void Unassign(__IFactoryListTarget list, IProducible parent, IProducible child)
        {
            if(!list.Items.Contains(child)) return;

            if(list.Relation == null)
            {
                list.Items.Remove(child);
                if(list.Index != null) { list.Index.Add(child.ID, child); }
            } else if(list.Relation is __1ToNRelationDescriptor)
            {
                throw new IntegrityViolationException("Unable to remove an item from a 1:n relation.");
            }
            else
            {
                parent.Lock();

                foreach(__RelationData i in ((__IFactoryTarget) child).Relations)
                {
                    if(i.Descriptor is __MToNRelationDescriptor)
                    {
                        if((i.Descriptor.TargetEntity == ((__IFactoryTarget) parent).EntityDescriptor) && (((__MToNRelationDescriptor) i.Descriptor).RelationTable.TableName == ((__MToNRelationDescriptor) list.Relation).RelationTable.TableName))
                        {
                            i.GetList().Load();
                            if(i.GetList().PersistentItems == null) { i.GetList().PersistentItems = new List<IProducible>(i.GetList().Items); }
                            if(i.GetList().Items.Contains(parent)) { i.GetList().Items.Remove(parent); }
                            if(i.GetList().PersistentItems.Contains(parent)) { i.GetList().PersistentItems.Remove(parent); }
                            if((i.GetList().Index != null) && (i.GetList().Index.ContainsKey(child.ID))) { i.GetList().Index.Remove(child.ID); }
                            break;
                        }
                    }
                }

                list.Items.Add(child);
                if(list.Index != null) { list.Index.Add(child.ID, child); }
            }
        }
    }
}
