﻿using System;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements a relation descriptor that holds metadata for a m:n relation.</summary>
    public sealed class __MToNRelationDescriptor: __RelationDescriptor
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="entity">Parent entity.</param>
        /// <param name="section">ddp section.</param>
        public __MToNRelationDescriptor(__EntityDescriptor entity, DdpSection section) : base(entity, section)
        {
            SourceColumn = new SQLField(section.GetString("alias", "X"), section.GetString("column"));
            DestinationColumn = new SQLField(section.GetString("alias", "X"), section.GetString("dest"));
            RelationTable = new SQLTable(section.GetString("table"), section.GetString("alias", "X"));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the source column.</summary>
        /// <remarks>This is the column that contains the key of the parent entity (that contains the list).</remarks>
        public SQLField SourceColumn
        {
            get; private set;
        }


        /// <summary>Gets the destination column.</summary>
        /// <remarks>This is the column that contains the key of the queried entity (list members).</remarks>
        public SQLField DestinationColumn
        {
            get; private set;
        }


        /// <summary>Relation table.</summary>
        public SQLTable RelationTable
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] __RelationDescriptor                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a SQL query for this relation for a given producible object.</summary>
        /// <param name="p">Producible object.</param>
        /// <returns>SQL query.</returns>
        public override ISQLQuery CreateQuery(IProducible p)
        {
            ISQLQuery rval = TargetEntity.CreateQuery().Join(RelationTable, TargetEntity.Fields.IDField.ColumnExpression + " = " + DestinationColumn.ColumnExpression);
            rval.And(SourceColumn.ColumnExpression + " = " + TargetEntity.Provider.Parser.ToBindVariableName(SourceColumn.TableAlias + SourceColumn.ColumnName));
            rval.AddParameter(TargetEntity.Provider.Parser.ToBindVariableName(SourceColumn.TableAlias + SourceColumn.ColumnName), p.ID);

            return rval;
        }
    }
}
