﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class represents a relation list.</summary>
    public sealed class __RelationList: IEnumerable<__RelationData>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>List items.</summary>
        private Dictionary<string, __RelationData> _Items;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="p">Parent object.</param>
        public __RelationList(Producible p)
        {
            Parent = p;
            _Items = new Dictionary<string, __RelationData>();

            foreach(__RelationDescriptor i in ((__IFactoryTarget) p).EntityDescriptor.Relations)
            {
                _Items.Add(i.Name, new __RelationData(this, i));
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a field by its name.</summary>
        /// <param name="fieldName">Field name.</param>
        /// <returns>Field data.</returns>
        public __RelationData this[string fieldName]
        {
            get { return _Items[fieldName]; }
        }


        /// <summary>Gets the parent object for this list.</summary>
        public Producible Parent { get; private set; }


        /// <summary>Gets the entity descriptor.</summary>
        public __EntityDescriptor EntityDescriptor
        {
            get { return ((__IFactoryTarget) Parent).EntityDescriptor; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<__RelationData>                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through the list.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<__RelationData> IEnumerable<__RelationData>.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through the list.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }
    }
}
