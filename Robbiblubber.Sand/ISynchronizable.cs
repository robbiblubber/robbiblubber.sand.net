﻿using System;



namespace Robbiblubber.Sand
{
    /// <summary>Objects that can be thread-synchronized implent this interface.</summary>
    public interface ISynchronizable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the synchronization particle for this instance.</summary>
        object __Sync { get; }
    }
}
