﻿using System;

using Robbiblubber.Util.Library;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class is used to access the repository.</summary>
    public sealed class __Repository
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        private __Repository()
        {
            Location = Ddp.Load(PathOp.ApplicationDirectory + @"\locator").Sections["locator"].GetString("repository");
            RepositoryFile = Ddp.Load(Location + @"\repository.config");
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the repository instance.</summary>
        public static __Repository Instance { get; } = new __Repository();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the repository location.</summary>
        public string Location
        {
            get; private set;
        }

        /// <summary>Repository file.</summary>
        public Ddp RepositoryFile { get; private set; }


        /// <summary>Gets the active factory class name.</summary>
        public string FactoryClassName
        {
            get { return RepositoryFile.Sections["factory"].GetString("class"); }
        }
        
        /// <summary>Gets the entity file mode.</summary>
        public string EntityFileMode
        {
            get { return RepositoryFile.Sections["entities"].GetString("mode"); }
        }

        /// <summary>Gets if entitey files are encryted.</summary>
        public bool EntityFileEncrypted
        {
            get { return RepositoryFile.Sections["entities"].GetBoolean("encrypt"); }
        }

        /// <summary>Gets the entitey file encrytion key.</summary>
        public string EntityFileKey
        {
            get { return RepositoryFile.Sections["entities"].GetString("key"); }
        }
    }
}
