﻿using System;
using System.Collections;
using System.Collections.Generic;

using Robbiblubber.Sand.Model.Base;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class provides a lightweight cache mechanism.</summary>
    public class __PrimitiveCache: ICache, IEnumerable<IProducible>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Cache items.</summary>
        protected Dictionary<string, IProducible> _Items = new Dictionary<string, IProducible>();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        protected __PrimitiveCache()
        {
            MaxSize = __Repository.Instance.RepositoryFile.Sections["cache"].GetInteger("max", 20000);
            ChangeTimeout = __Repository.Instance.RepositoryFile.Sections["cache"].GetInteger("timeout", 240);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ICache                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the element with the given ID.</summary>
        /// <param name="ID">ID.</param>
        /// <returns>Returns an element if contained, otherwise returns NULL.</returns>
        public IProducible this[string id]
        {
            get { return Retrieve(id); }
        }


        /// <summary>Gets the current cache size.</summary>
        public int Size
        {
            get { return _Items.Count; }
        }


        /// <summary>Gets or sets the maximum cache size.</summary>
        public int MaxSize { get; protected set; }


        /// <summary>Gets the timeout for object changes in minutes.</summary>
        public int ChangeTimeout { get; protected set; }


        /// <summary>Gets the element with the given ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Returns an element if contained, otherwise returns NULL.</returns>
        public virtual IProducible Retrieve(string id)
        {
            if(_Items.TryGetValue(id, out IProducible rval)) { return rval; }

            return null;
        }


        /// <summary>Adds an element to the cache.</summary>
        /// <param name="p">Element.</param>
        /// <remarks>Returns the added element.</remarks>
        public virtual IProducible Add(IProducible p)
        {
            if(_Items.ContainsKey(p.ID)) { _Items.Remove(p.ID); }

            _Items.Add(p.ID, p);
            return p;
        }


        /// <summary>Removes an element from the cache.</summary>
        /// <param name="p">Element.</param>
        public virtual void Remove(IProducible p)
        {
            if(_Items.ContainsKey(p.ID)) { _Items.Remove(p.ID); }
        }


        /// <summary>Removes an element from the cache.</summary>
        /// <param name="id">Element ID.</param>
        public virtual void Remove(string id)
        {
            if(_Items.ContainsKey(id)) { _Items.Remove(id); }
        }


        /// <summary>Clears the cache and removes all elements.</summary>
        public virtual void Clear()
        {
            _Items.Clear();
        }


        /// <summary>Reduces the size of the cache below <see cref="MaxSize"/>.</summary>
        public virtual void Purge()
        {
            if(Size > MaxSize) { Clear(); }
        }


        /// <summary>Refreshes the cache.</summary>
        public virtual void Refresh()
        {
            foreach(string i in __Factory.Instance.GetChanges())
            {
                Remove(i);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<IProducible>                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<IProducible> IEnumerable<IProducible>.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<IProducible>                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.Values.GetEnumerator();
        }
    }
}
