﻿using System;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This enumeration defines field content types.</summary>
    public enum __ContentType
    {
        /// <summary>Field content represents an ID.</summary>
        ID = 0,
        /// <summary>Field content represents the creation journal.</summary>
        JCREATED = 1,
        /// <summary>Field content represents the modification journal.</summary>
        JMODIFIED = 2,
        /// <summary>Field content represents the modification journal.</summary>
        JTIME = 3,
        /// <summary>Field content represents a data field.</summary>
        DATA = 4,
        /// <summary>Field content represents a referenced entity.</summary>
        REF = 5
    }
}
