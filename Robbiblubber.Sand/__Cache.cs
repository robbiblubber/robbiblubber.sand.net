﻿using System;
using System.Collections;
using System.Collections.Generic;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class provides a standard cache mechanism.</summary>
    public class __Cache: ICache, __IAttachable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        protected __Cache()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected static methods                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a factory instance.</summary>
        /// <returns>Instance.</returns>
        protected static ICache _CreateInstance()
        {
            ICache rval = (ICache) Activator.CreateInstance(Type.GetType(__Repository.Instance.RepositoryFile.Sections["cache"].GetString("class")), true);
            if(rval == null)
            {
                throw new TypeLoadException("Unable to load cache class \"" + __Repository.Instance.RepositoryFile.Sections["cache"].GetString("class") + "\".");
            }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the cache instance.</summary>
        public static ICache Instance
        {
            get; protected set;
        } = _CreateInstance();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __ICache                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // TODO: implement!

        /// <summary>Gets the element with the given ID.</summary>
        /// <param name="ID">ID.</param>
        /// <returns>Returns an element if contained, otherwise returns NULL.</returns>
        public virtual IProducible this[string id]
        {
            get
            {
                throw new NotImplementedException();
            }
        }


        /// <summary>Gets the current cache size.</summary>
        public virtual int Size { get; }


        /// <summary>Gets the maximum cache size.</summary>
        public virtual int MaxSize { get; }


        /// <summary>Gets the timeout for object changes in minutes.</summary>
        public virtual int ChangeTimeout { get; }


        /// <summary>Gets the element with the given ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Returns an element if contained, otherwise returns NULL.</returns>
        public virtual IProducible Retrieve(string id)
        {
            throw new NotImplementedException();
        }


        /// <summary>Adds an element to the cache.</summary>
        /// <param name="p">Element.</param>
        /// <remarks>Returns the added element.</remarks>
        public virtual IProducible Add(IProducible p)
        {
            throw new NotImplementedException();
        }


        /// <summary>Removes an element from the cache.</summary>
        /// <param name="p">Element.</param>
        public virtual void Remove(IProducible p)
        {
            throw new NotImplementedException();
        }


        /// <summary>Removes an element from the cache.</summary>
        /// <param name="id">Element ID.</param>
        public virtual void Remove(string id)
        {
            throw new NotImplementedException();
        }


        /// <summary>Clears the cache and removes all elements.</summary>
        public virtual void Clear()
        {
            throw new NotImplementedException();
        }


        /// <summary>Reduces the size of the cache below <see cref="MaxSize"/>.</summary>
        public virtual void Purge()
        {
            throw new NotImplementedException();
        }


        /// <summary>Refreshes the cache.</summary>
        public virtual void Refresh()
        {
            throw new NotImplementedException();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<IProducible>                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator<IProducible> IEnumerable<IProducible>.GetEnumerator()
        {
            throw new NotImplementedException();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IEnumerable<IProducible>                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an enumerator that iterates through the collection.</summary>
        /// <returns>Enumerator.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            throw new NotImplementedException();
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IAttachable                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Attaches the object to the system.</summary>
        public virtual void Attach()
        {}


        /// <summary>Detaches the object from the system.</summary>
        public virtual void Detach()
        {}
    }
}
