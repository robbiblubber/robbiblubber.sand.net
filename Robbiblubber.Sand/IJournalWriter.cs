﻿using System;

using Robbiblubber.Sand.Security;



namespace Robbiblubber.Sand.Journalization.Base
{
    /// <summary>Log writers implement this interface.</summary>
    public interface __IJournalWriter
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes a journal entry.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="referenced">Referenced object.</param>
        /// <param name="severity">Severity level.</param>
        /// <param name="operation">Operation flag.</param>
        /// <param name="flags">Additional flags.</param>
        /// <param name="reserved">Reserved flags.</param>
        /// <param name="message">Journal entry message.</param>
        /// <param name="tags">Journal entry tags.</param>
        /// <param name="data">Journal entry data.</param>
        /// <param name="additional">Additional data.</param>
        /// <param name="session">Session.</param>
        void Write(IProducible obj, IProducible referenced, Journal.Severity severity, Journal.Operation operation, int flags, int reserved, string message, string tags, string data, string additional, ISession session);
    }
}
