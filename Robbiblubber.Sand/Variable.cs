﻿using System;

using Robbiblubber.Util.Library;



namespace Robbiblubber.Sand.Security
{
    /// <summary>This class represents a system variable.</summary>
    public sealed class Variable: SystemObject, ISystemObject
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a privilege by its name.</summary>
        /// <param name="name">Privilege name.</param>
        /// <returns>Privilege.</returns>
        public static Variable ByName(string name)
        {
            return Factory.Produce<Variable>(m => (m.Name == name));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets if the variable is global.</summary>
        public bool Global
        {
            get { return _GetValue<bool>("Global"); }
            set { _SetValue("Global", value); }
        }


        /// <summary>Gets or sets the variable value.</summary>
        public string StringValue
        {
            get { return _GetValue<string>("Value"); }
            set { _SetValue("Value", value); }
        }


        /// <summary>Gets or sets the variable value as a boolean.</summary>
        public bool BooleanValue
        {
            get { return StringValue.ToBoolean(); }
            set { StringValue = (value ? "true" : "false"); }
        }


        /// <summary>Gets or sets the variable value as a boolean.</summary>
        public int IntegerValue
        {
            get { return StringValue.ToInteger(); }
            set { StringValue = value.ToString(); }
        }


        /// <summary>Gets or sets the variable value as a DateTime.</summary>
        public DateTime DateTimeValue
        {
            get { return StringValue.ParseTimestamp(); }
            set { StringValue = value.ToTimestamp(); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the variable value.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <returns>Value.</returns>
        public T GetValue<T>()
        {
            if(typeof(T) == typeof(string)) { return (T) Convert.ChangeType(StringValue, typeof(T)); }
            if(typeof(T) == typeof(bool)) { return (T) Convert.ChangeType(BooleanValue, typeof(T)); }
            if(typeof(T) == typeof(int)) { return (T) Convert.ChangeType(StringValue, typeof(T)); }
            if(typeof(T) == typeof(DateTime)) { return (T) Convert.ChangeType(DateTimeValue, typeof(T)); }

            throw new InvalidCastException("Variable implementation does not support type \"" + typeof(T).Name + "\".");
        }
    }
}
