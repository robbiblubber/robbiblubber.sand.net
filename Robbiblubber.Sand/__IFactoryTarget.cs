﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>Factory targets implement this interface for information exchange.</summary>
    public interface __IFactoryTarget
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the factory target entity descriptor.</summary>
        __EntityDescriptor EntityDescriptor { get; }


        /// <summary>Gets the fields list.</summary>
        __FieldList Fields { get; }


        /// <summary>Gets the relations list.</summary>
        __RelationList Relations { get; }


        /// <summary>Gets or sets the lock object for this instance.</summary>
        Lock LockObject { get; set; }
    }
}
