﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robbiblubber.Sand.Journalization;
using Robbiblubber.Sand.Model;

namespace Robbiblubber.Sand.Security
{
    public class Session: ISession, IProducible, IAtom
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // protected members                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Synchronization particle.</summary>
        protected object _Sync = new object();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // construtors                                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        protected internal Session()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="token">Token.</param>
        /// <remarks>Represents an anonymous session.</remarks>
        protected internal Session(string token)
        {
            Token = token;
            User = null;
            StartTime = DateTime.Now;
            EndTime = DateTime.Now;
            UserName = "anonymous";
            Data = "un=anonymous;";
            Active = false;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISession                                                                                             //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the session token for this session.</summary>
        public string Token { get; internal protected set; }


        /// <summary>Gets the user object for this session.</summary>
        public User User { get; internal protected set; }


        /// <summary>Gets the session start time.</summary>
        public DateTime StartTime { get; internal protected set; }


        /// <summary>Gets the session end time.</summary>
        public DateTime EndTime { get; internal protected set; }


        /// <summary>Gets the session user name.</summary>
        public string UserName { get; internal protected set; }


        /// <summary>Gets the session data for this session.</summary>
        public string Data { get; internal protected set; }


        /// <summary>Gets a value that determines if the session is active.</summary>
        public bool Active { get; internal protected set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IAtom                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the object ID.</summary>
        string IAtom.ID
        {
            get { return Token; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISynchronizable                                                                                      //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the object's synchronization particle.</summary>
        object ISynchronizable.__Sync
        {
            get { return _Sync; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IProducible                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the operations for this object.</summary>
        public virtual Operations Ops
        {
            get { return new Operations(this); }
        }

        /// <summary>Gets the inline journal for this object.</summary>
        InlineJournal IProducible.InlineJournal
        {
            get { throw new InvalidOperationException("Operation not allowed for a session object."); }
        }


        /// <summary>Locks the object for a given context.</summary>
        /// <param name="context">Context.</param>
        void IProducible.Lock(Context context)
        {
            throw new InvalidOperationException("Operation not allowed for a session object.");
        }


        /// <summary>Locks the object.</summary>
        void IProducible.Lock()
        {
            throw new InvalidOperationException("Operation not allowed for a session object.");
        }


        /// <summary>Returns the lock object for this instance.</summary>
        Lock IProducible.GetLock()
        {
            return new Lock(this, null);
        }


        /// <summary>Saves the object.</summary>
        /// <param name="context">Context.</param>
        void IProducible.Save(Context context)
        {
            throw new InvalidOperationException("Operation not allowed for a session object.");
        }


        /// <summary>Saves the object.</summary>
        void IProducible.Save()
        {
            throw new InvalidOperationException("Operation not allowed for a session object.");
        }


        /// <summary>Creates a clone of the object.</summary>
        /// <returns>Cloned object.</returns>
        IProducible IProducible.Clone()
        {
            throw new InvalidOperationException("Operation not allowed for a session object.");
        }


        /// <summary>Deletes the object.</summary>
        /// <param name="context">Context.</param>
        void IProducible.Delete(Context context)
        {
            throw new InvalidOperationException("Operation not allowed for a session object.");
        }


        /// <summary>Deletes the object.</summary>
        void IProducible.Delete()
        {
            throw new InvalidOperationException("Operation not allowed for a session object.");
        }


        /// <summary>Refreshes the object.</summary>
        public virtual void Refresh()
        {
            // TODO: implement.
        }
    }
}
