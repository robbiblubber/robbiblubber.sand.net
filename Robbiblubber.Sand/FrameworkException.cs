﻿using System;



namespace Robbiblubber.Sand.Exceptions
{
    /// <summary>This class provides a base implementation for all Sand framework exceptions.</summary>
    public class FrameworkException: Exception
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public FrameworkException(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        public FrameworkException(string message): base(message)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">The exception that caused the exception.</param>
        public FrameworkException(string message, Exception innerException): base(message, innerException)
        {}
    }
}
