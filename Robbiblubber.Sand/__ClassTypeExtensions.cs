﻿using System;



namespace Robbiblubber.Sand.Model.Base
{
    public static class __ClassTypeExtensions
    {
        /// <summary>Converts a content type to a string constant.</summary>
        /// <param name="c">Content type.</param>
        /// <returns>String.</returns>
        public static string ToConstant(this __ClassType c)
        {
            switch(c)
            {
                case __ClassType.MATERIAL: return "MATERIAL";
                case __ClassType.VIRTUAL: return "VIRTUAL";
                case __ClassType.INTEGRATED: return "INTEGRATED";
            }

            return "LEAF";
        }


        /// <summary>Converts a string to a content type.</summary>
        /// <param name="s">String.</param>
        /// <returns>Content type.</returns>
        public static __ClassType ToClassType(this string s)
        {
            switch(s.ToUpper())
            {
                case "MATERIAL": return __ClassType.MATERIAL;
                case "VIRTUAL": return __ClassType.VIRTUAL;
                case "INTEGRATED": return __ClassType.INTEGRATED;
            }

            return __ClassType.LEAF;
        }
    }
}
