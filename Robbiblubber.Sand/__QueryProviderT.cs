﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements a query provider for producible object lists.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public class __QueryProvider<T>: IQueryProvider where T: IProducible
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Parent list.</summary>
        private ProducibleList<T> _Parent;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="parent">Parent list object.</param>
        public __QueryProvider(ProducibleList<T> parent)
        {
            _Parent = parent;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IQueryProvider                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Constructs an IQueryable object that can evaluate the query represented by a specified expression tree.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>An IQueryable that can evaluate the query represented by the specified expression tree.</returns>
        public IQueryable CreateQuery(Expression expression)
        {
            return new ProducibleList<T>(__Factory.Instance.Entities[typeof(T)].ExpressionParser.Parse(_Parent._Query, expression));
        }


        /// <summary>Constructs an IQueryable object that can evaluate the query represented by a specified expression tree.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>An IQueryable that can evaluate the query represented by the specified expression tree.</returns>
        public IQueryable<TElement> CreateQuery<TElement>(Expression expression)
        {
            Type t = typeof(ProducibleList<>).MakeGenericType(new Type[] { typeof(TElement) });
            
            if(_Parent._Query == null)
            {
                foreach(ConstructorInfo i in t.GetConstructors(BindingFlags.Public | BindingFlags.Instance))
                {
                    if((i.GetParameters().Length > 0) && (i.GetParameters()[0].ParameterType == typeof(IEnumerable<T>)))
                    {
                        LambdaExpression lambda = (LambdaExpression) ((UnaryExpression) ((MethodCallExpression) expression).Arguments[1]).Operand;
                        return (IQueryable<TElement>) i.Invoke(new object[] { ((IEnumerable<T>) _Parent).Where((Func<T, bool>) lambda.Compile()) });
                    }
                }
            }
            else
            {
                foreach(ConstructorInfo i in t.GetConstructors(BindingFlags.NonPublic | BindingFlags.Instance))
                {
                    if(i.GetParameters()[0].ParameterType == typeof(ISQLQuery))
                    {
                        return (IQueryable<TElement>) i.Invoke(new object[] { __Factory.Instance.Entities[typeof(TElement)].ExpressionParser.Parse(_Parent._Query, expression) });
                    }
                }
                
            }

            return (IQueryable<TElement>) new List<TElement>();
        }


        /// <summary>Executes the query represented by a specified expression tree.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>The value that results from executing the specified query.</returns>
        public object Execute(Expression expression)
        {
            return __Factory.Instance.ProduceObject<T>(__Factory.Instance.Entities[typeof(T)].ExpressionParser.Parse(_Parent._Query, expression));
        }


        /// <summary>Executes the query represented by a specified expression tree.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>The value that results from executing the specified query.</returns>
        public TResult Execute<TResult>(Expression expression)
        {
            return __Factory.Instance.ProduceObject<TResult>(__Factory.Instance.Entities[typeof(TResult)].ExpressionParser.Parse(_Parent._Query, expression));
        }
    }
}
