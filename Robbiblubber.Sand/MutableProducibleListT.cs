﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Robbiblubber.Sand.Exceptions;
using Robbiblubber.Sand.Model.Base;



namespace Robbiblubber.Sand
{
    /// <summary>This class provides a mutable list of producible objects.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public class MutableProducibleList<T>: ProducibleList<T>, IMutableProducibleList<T>, IProducibleList<T>, IEnumerable<T>, IQueryable<T>, IRefreshable, __IFactoryListTarget where T : IProducible
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public MutableProducibleList(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="items">Items.</param>
        public MutableProducibleList(IEnumerable<T> items): base(items)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="items">Items.</param>
        protected internal MutableProducibleList(IDictionary<string, IProducible> items): base(items)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="relation">Relation descriptor.</param>
        protected internal MutableProducibleList(IProducible parent, __RelationDescriptor relation): base(parent, relation)
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] IMutableProducibleList<T>                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Adds an item to the list.</summary>
        /// <param name="item">Item.</param>
        /// <exception cref="ObjectLockedException">Thrown when the object could not be locked.</exception>
        public virtual void Add(T item)
        {
            __Factory.Instance.Assign(this, Parent, item);
        }


        /// <summary>Removes an item from the list.</summary>
        /// <param name="item">Item.</param>
        public virtual void Remove(T item)
        {
            __Factory.Instance.Unassign(this, Parent, item);
        }


        /// <summary>Removes an item from the list.</summary>
        /// <param name="id">Item ID.</param>
        /// <exception cref="IntegrityViolationException">Thrown when an object is removed from a 1:n relation.</exception>
        public virtual void Remove(string id)
        {
            Remove(Factory.Produce<T>(id));
        }


        /// <summary>Removes all items from the list.</summary>
        public virtual void Clear()
        {
            List<IProducible> items = new List<IProducible>(_Items);

            foreach(IProducible i in items) { Remove((T) i); }
        }
    }
}
