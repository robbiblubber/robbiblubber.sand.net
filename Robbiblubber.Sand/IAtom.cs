﻿using System;



namespace Robbiblubber.Sand
{
    /// <summary>All framework objects implement this interface.</summary>
    public interface IAtom
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the object ID.</summary>
        string ID { get; }
    }
}
