﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq.Expressions;
using Robbiblubber.Sand.Exceptions;
using Robbiblubber.Sand.Journalization;
using Robbiblubber.Sand.Model.Base;
using Robbiblubber.Sand.Security;
using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model
{
    /// <summary>Factory implementations must implement this interface.</summary>
    public interface IFactory
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the system provider.</summary>
        IProvider SystemProvider { get; }


        /// <summary>Gets a list of entity descriptors.</summary>
        __EntityDescriptorList Entities { get; }


        /// <summary>Gets the provider cache.</summary>
        __ProviderCache Providers { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the IDs of recently changed objects.</summary>
        /// <returns>List of strings.</returns>
        IEnumerable<string> GetChanges();


        /// <summary>Loads database data into an object.</summary>
        /// <param name="p">Producible object.</param>
        /// <param name="re">Database reader.</param>
        /// <param name="force">Forces refresh for modified objects.</param>
        void RefreshObject(IProducible p, IDataReader re, bool force);


        /// <summary>Produces an object.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="id">ID.</param>
        /// <returns>Producible object.</returns>
        IProducible ProduceObject(string id);


        /// <summary>Produces an object from a database reader.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="re">Database reader.</param>
        /// <returns>Producible object.</returns>
        T ProduceObject<T>(IDataReader re);


        /// <summary>Produces an object from an SQL query.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="query">SQL Query.</param>
        /// <returns>Producible object.</returns>
        T ProduceObject<T>(ISQLQuery query);


        /// <summary>Produces a single instance from an object comparison.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="expression">Expression.</param>
        /// <returns>Instance. Returns NULL when no object matches.</returns>
        T ProduceObject<T>(Expression<Func<T, bool>> expression) where T: IProducible;

        
        /// <summary>Produces a list of objects from a SQL query.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="query">SQL query.</param>
        /// <param name="bindVariables">Bind variables.</param>
        /// <returns>List.</returns>
        List<IProducible> Populate<T>(ISQLQuery query) where T: IProducible;


        /// <summary>Sets a user password.</summary>
        /// <param name="user">User.</param>
        /// <param name="password">New password.</param>
        /// <param name="context">Context.</param>
        void SetPassword(User user, string password, Context context);


        /// <summary>Unsets a user password.</summary>
        /// <param name="user">User.</param>
        /// <param name="context">Context.</param>
        void UnsetPassword(User user, Context context);


        /// <summary>Creates an authenticed context with user name and password.</summary>
        /// <param name="userName">User name.</param>
        /// <param name="password">Password.</param>
        /// <returns>Context.</returns>
        /// <remarks>Returns NULL when credentials could not be verified.</remarks>
        Context CreateContext(string userName, string password);


        /// <summary>Creates an authenticed context with system credentials.</summary>
        /// <returns>Context.</returns>
        /// <remarks>Returns NULL when credentials could not be verified.</remarks>
        Context CreateContext();


        /// <summary>Saves an object.</summary>
        /// <param name="p">Producible object.</param>
        /// <param name="context">Context.</param>
        void SaveObject(IProducible p, Context context);


        /// <summary>Marks an object as modified.</summary>
        /// <param name="p">Producible object.</param>
        /// <param name="context">Context.</param>
        void TouchObject(IProducible p, Context context);


        /// <summary>Creates a lock on an object.</summary>
        /// <param name="p">Producible object.</param>
        /// <param name="context">Context.</param>
        /// <exception cref="ObjectLockedException">Thrown when the object is already locked.</exception>
        void CreateLock(IProducible p, Context context);


        /// <summary>Releases a lock on an object.</summary>
        /// <param name="p">Producible object.</param>
        /// <param name="context">Context.</param>
        /// <param name="force">Allows administrative contextes to forcefully remove locks held by other user contextes.</param>
        void ReleaseLock(IProducible p, Context context, bool force);


        /// <summary>Gets a lock object that represents a lock on a specific object.</summary>
        /// <param name="p">Producible object</param>
        /// <returns>Lock object.</returns>
        Lock GetLock(IProducible p);


        /// <summary>Removes timed-out contextes from the system.</summary>
        void PurgeSessions();


        /// <summary>Releases orphan and timed out locks.</summary>
        void CleanUpLocks();


        /// <summary>Loads operation descriptions from database.</summary>
        /// <param name="entity">Entity descriptor.</param>
        /// <returns>Operations dictionary.</returns>
        Dictionary<Op, string> LoadOps(__EntityDescriptor entity);


        /// <summary>Returns the required privilege for a specific operation on an object.</summary>
        /// <param name="p">Producible object.</param>
        /// <param name="op">Operation.</param>
        /// <returns>Privilege.</returns>
        Privilege GetObjectPrivilege(IProducible p, Op op);


        /// <summary>Returns all privileges held by a user object.</summary>
        /// <param name="u">User.</param>
        /// <returns>List of privileges.</returns>
        IProducibleList<Privilege> GetPrivileges(User u);


        /// <summary>Gets a user variable.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="user">User.</param>
        /// <param name="variable">Variable.</param>
        /// <param name="context">Context.</param>
        /// <returns>Variable value.</returns>
        T GetUserVariable<T>(User user, Variable variable, Context context);


        /// <summary>Sets a user variable.</summary>
        /// <param name="user">User.</param>
        /// <param name="variable">Variable.</param>
        /// <param name="value">Value.</param>
        /// <param name="context">Context.</param>
        void SetUserVariable(User user, Variable variable, object value, Context context);


        /// <summary>Writes a journal entry.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="referenced">Referenced object.</param>
        /// <param name="severity">Severity level.</param>
        /// <param name="operation">Operation flag.</param>
        /// <param name="flags">Additional flags.</param>
        /// <param name="reserved">Reserved flags.</param>
        /// <param name="message">Journal entry message.</param>
        /// <param name="tags">Journal entry tags.</param>
        /// <param name="data">Journal entry data.</param>
        /// <param name="additional">Additional data.</param>
        /// <param name="session">Session.</param>
        void WriteEntry(IProducible obj, IProducible referenced, Journal.Severity severity, Journal.Operation operation, int flags, int reserved, string message, string tags, string data, string additional, ISession session);


        /// <summary>Gets a set of journal entries.</summary>
        /// <param name="exp">Expression.</param>
        /// <returns>Journal entries.</returns>
        IEnumerable<JournalEntry> GetEntries(Expression<Func<JournalEntry, bool>> exp);


        /// <summary>Assigns an object to a target in a relation.</summary>
        /// <param name="list">Relation list.</param>
        /// <param name="parent">Parent object.</param>
        /// <param name="child">Child object.</param>
        void Assign(__IFactoryListTarget list, IProducible parent, IProducible child);


        /// <summary>Unassigns an object from a target in a relation.</summary>
        /// <param name="list">Relation list.</param>
        /// <param name="parent">Parent object.</param>
        /// <param name="child">Child object.</param>
        /// <exception cref="IntegrityViolationException">Thrown when an object is removed from a 1:n relation.</exception>
        void Unassign(__IFactoryListTarget list, IProducible parent, IProducible child);
    }
}
