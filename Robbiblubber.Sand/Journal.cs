﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Robbiblubber.Sand.Journalization.Base;
using Robbiblubber.Sand.Model.Base;
using Robbiblubber.Sand.Security;

namespace Robbiblubber.Sand.Journalization
{
    /// <summary>This class provides access to the system journal.</summary>
    public static class Journal
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes a journal entry.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="referenced">Referenced object.</param>
        /// <param name="severity">Severity level.</param>
        /// <param name="operation">Operation flag.</param>
        /// <param name="flags">Additional flags.</param>
        /// <param name="reserved">Reserved flags.</param>
        /// <param name="message">Journal entry message.</param>
        /// <param name="tags">Journal entry tags.</param>
        /// <param name="data">Journal entry data.</param>
        /// <param name="additional">Additional data.</param>
        /// <param name="session">Session.</param>
        public static void Write(IProducible obj, IProducible referenced, Severity severity, Operation operation, int flags, int reserved, string message, string tags, string data, string additional, ISession session = null)
        {
            __JournalWriter.Instance.Write(obj, referenced, severity, operation, flags, reserved, message, tags, data, additional, session);
        }


        /// <summary>Writes a journal entry.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="referenced">Referenced object.</param>
        /// <param name="severity">Severity level.</param>
        /// <param name="operation">Operation flag.</param>
        /// <param name="message">Journal entry message.</param>
        /// <param name="tags">Journal entry tags.</param>
        /// <param name="data">Journal entry data.</param>
        /// <param name="additional">Additional data.</param>
        /// <param name="session">Session.</param>
        public static void Write(IProducible obj, IProducible referenced, Severity severity = Severity.INFORMATION, Operation operation = Operation.UNDEFINED, string message = "", string tags = "", string data = "", string additional = "", ISession session = null)
        {
            __JournalWriter.Instance.Write(obj, referenced, severity, operation, 0, 0, message, tags, data, additional, session);
        }


        /// <summary>Writes a journal entry.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="severity">Severity level.</param>
        /// <param name="operation">Operation flag.</param>
        /// <param name="message">Journal entry message.</param>
        /// <param name="tags">Journal entry tags.</param>
        /// <param name="data">Journal entry data.</param>
        /// <param name="additional">Additional data.</param>
        /// <param name="session">Session.</param>
        public static void Write(IProducible obj, Severity severity = Severity.INFORMATION, Operation operation = Operation.UNDEFINED, string message = "", string tags = "", string data = "", string additional = "", ISession session = null)
        {
            __JournalWriter.Instance.Write(obj, null, severity, operation, 0, 0, message, tags, data, additional, session);
        }


        /// <summary>Writes a journal entry.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="severity">Severity level.</param>
        /// <param name="operation">Operation flag.</param>
        /// <param name="message">Journal entry message.</param>
        /// <param name="tags">Journal entry tags.</param>
        /// <param name="data">Journal entry data.</param>
        /// <param name="additional">Additional data.</param>
        /// <param name="session">Session.</param>
        public static void Write(Severity severity = Severity.INFORMATION, Operation operation = Operation.UNDEFINED, string message = "", string tags = "", string data = "", string additional = "", ISession session = null)
        {
            __JournalWriter.Instance.Write(null, null, severity, operation, 0, 0, message, tags, data, additional, session);
        }


        /// <summary>Returns all journal entries that match a given expression.</summary>
        /// <param name="exp">Expression.</param>
        /// <returns>Set of journal entries.</returns>
        public static IEnumerable<JournalEntry> All(Expression<Func<JournalEntry, bool>> exp)
        {
            return __Factory.Instance.GetEntries(exp);
        }


        /// <summary>Returns all journal entries.</summary>
        /// <returns>Set of journal entries.</returns>
        public static IEnumerable<JournalEntry> All()
        {
            return __Factory.Instance.GetEntries(m => true);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static properties                                                                                         //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the journal settings.</summary>
        public static Settings Settings
        {
            get;
        } = new Settings();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [enum] Severity                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This enumeration defines journal entry severity levels.</summary>
        public enum Severity: int
        {
            /// <summary>The entry denotes a debug message.</summary>
            DEBUG = -1,
            /// <summary>The entry denotes an informational message.</summary>
            INFORMATION = 0,
            /// <summary>The entry denotes a system notification.</summary>
            NOTIFICATION = 1,
            /// <summary>The entry denotes a warning condition.</summary>
            WARNING = 2,
            /// <summary>The entry denotes an error condition.</summary>
            ERROR = 3,
            /// <summary>The entry denotes a critical condition.</summary>
            CRITICAL = 4,
            /// <summary>The entry denotes a system alert.</summary>
            ALERT = 5,
            /// <summary>The entry denotes a system emergency.</summary>
            EMERGENCY = 6
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [enum] Operation                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>This enumeration defines journal entry operations.</summary>
        public enum Operation: int
        {
            /// <summary>The operation is undefined.</summary>
            UNDEFINED = -1,
            /// <summary>The entry describes a general operation.</summary>
            GENERAL = 0,
            /// <summary>The entry describes an object creation.</summary>
            CREATE = 1,
            /// <summary>The entry describes an object modification.</summary>
            MODIFY = 2,
            /// <summary>The entry describes an object deletion.</summary>
            DELETE = 3,
            /// <summary>The entry describes an assign operation.</summary>
            ASSIGN = 4,
            /// <summary>The entry describes an object access.</summary>
            ACCESS = 5,
            /// <summary>The entry describes an object execution.</summary>
            EXECUTE = 6,
            /// <summary>The entry describes a user authentication.</summary>
            AUTHENTICATION = 7
        }
    }
}
