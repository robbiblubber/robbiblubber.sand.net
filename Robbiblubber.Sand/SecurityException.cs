﻿using System;

using Robbiblubber.Sand.Exceptions;



namespace Robbiblubber.Sand.Security.Exceptions
{
    /// <summary>This class provides a base implementation for all Sand security exceptions.</summary>
    public class SecurityException: FrameworkException
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public SecurityException(): base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        public SecurityException(string message): base(message)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">The exception that caused the exception.</param>
        public SecurityException(string message, Exception innerException) : base(message, innerException)
        {}
    }
}
