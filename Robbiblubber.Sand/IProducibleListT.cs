﻿using System;
using System.Collections.Generic;
using System.Linq;



namespace Robbiblubber.Sand
{
    /// <summary>Producible object lists implement this interface.</summary>
    /// <typeparam name="T">Type.</typeparam>
    public interface IProducibleList<T>: IEnumerable<T>, IQueryable<T>, IRefreshable where T: IProducible
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the number of items in this list.</summary>
        int Count { get; }


        /// <summary>Gets the object with a given index from the list.</summary>
        /// <param name="n">Index.</param>
        /// <returns>Producible object.</returns>
        T this[int n] { get; }


        /// <summary>Gets the object with a given ID from the list.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Producible object. Returns NULL if the list doesn't contain an object with the given ID.</returns>
        T this[string id] { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a value indicating if the list contains an object.</summary>
        /// <param name="item">Object.</param>
        /// <returns>Returns TRUE if the list contains the item, otherwise returns FALSE.</returns>
        bool Contains(T item);


        /// <summary>Returns a value indicating if the list contains an object with the specified ID.</summary>
        /// <param name="item">Object.</param>
        /// <returns>Returns TRUE if the list contains the item, otherwise returns FALSE.</returns>
        bool Contains(string id);
    }
}
