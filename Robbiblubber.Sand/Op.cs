﻿using System;



namespace Robbiblubber.Sand.Security
{
    /// <summary>This class represents a system operation.</summary>
    public sealed class Op
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static readonly members                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>This denotes a create (new) operation.</summary>
        public static readonly Op NEW = new Op("n", "new");


        /// <summary>This denotes a read operation.</summary>
        public static readonly Op READ = new Op("r", "read");


        /// <summary>This denotes a write operation.</summary>
        public static readonly Op WRITE = new Op("w", "write");


        /// <summary>This denotes a delete operation.</summary>
        public static readonly Op DELETE = new Op("d", "delete");


        /// <summary>This denotes an execute operation.</summary>
        public static readonly Op EXECUTE = new Op("x", "execute");



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of the class.</summary>
        /// <param name="code">Operation code.</param>
        /// <param name="description">Operation description.</param>
        public Op(string code, string description)
        {
            Code = code;
            Description = description;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets an operation from a string.</summary>
        /// <param name="op">Operation string.</param>
        /// <returns>Operation object.</returns>
        public static Op FromString(string op)
        {
            switch(op)
            {
                case "n": return NEW;
                case "r": return READ;
                case "w": return WRITE;
                case "d": return DELETE;
                case "x": return EXECUTE;
            }

            return new Op(op, op);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the operation code.</summary>
        public string Code
        {
            get; private set;
        }


        /// <summary>Gets the operation description.</summary>
        public string Description
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // operators                                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Compares two instances of the class.</summary>
        /// <param name="a">First instance.</param>
        /// <param name="b">Second instance.</param>
        /// <returns>Returns TRUE if both instances are logically identical.</returns>
        public static bool operator ==(Op a, Op b)
        {
            if(((object) a == null) || ((object) b == null))
            {
                return ((object) a == null) && ((object) b == null);
            }

            return (a.Code == b.Code);
        }


        /// <summary>Compares two instances of the class.</summary>
        /// <param name="a">First instance.</param>
        /// <param name="b">Second instance.</param>
        /// <returns>Returns FALSE if both instances are logically identical.</returns>
        public static bool operator !=(Op a, Op b)
        {
            if(((object) a == null) || ((object) b == null))
            {
                return !((object) a == null) && ((object) b == null);
            }

            return (a.Code != b.Code);
        }


        /// <summary>Compares Op with string.</summary>
        /// <param name="a">First instance.</param>
        /// <param name="b">String.</param>
        /// <returns>Returns TRUE if both instances are logically identical.</returns>
        public static bool operator ==(Op a, String b)
        {
            if(((object) a == null) || ((object) b == null))
            {
                return ((object) a == null) && ((object) b == null);
            }

            return (a.Code == b);
        }


        /// <summary>Compares Op with string.</summary>
        /// <param name="a">First instance.</param>
        /// <param name="b">String.</param>
        /// <returns>Returns FALSE if both instances are logically identical.</returns>
        public static bool operator !=(Op a, String b)
        {
            if(((object) a == null) || ((object) b == null))
            {
                return !((object) a == null) && ((object) b == null);
            }

            return (a.Code != b);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [overrides] object                                                                                               //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a hash code for the object.</summary>
        /// <returns>Hash code.</returns>
        public override int GetHashCode()
        {
            return Code.GetHashCode();
        }


        /// <summary>Determins whether the current instance is logically identical to a given object.</summary>
        /// <param name="obj">Object.</param>
        /// <returns>Returns TRUE if the object is identical.</returns>
        public override bool Equals(object obj)
        {
            if(obj is Op)
            {
                return (Code == ((Op) obj).Code);
            }
            else if(obj is string)
            {
                return Code == ((string) obj);
            }

            return false;
        }


        /// <summary>Returns a string representation of the object.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return Code;
        }
    }
}
