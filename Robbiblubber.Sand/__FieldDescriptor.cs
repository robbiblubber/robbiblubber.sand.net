﻿using System;

using Robbiblubber.Util.Library;
using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements a field descriptor that holds metadata for a single field.</summary>
    public sealed class __FieldDescriptor: __IDescriptor, ISQLColumn
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="entity">Parent entity.</param>
        /// <param name="section">ddp section.</param>
        public __FieldDescriptor(__EntityDescriptor entity, DdpSection section)
        {
            Entity = entity;

            if(section != null)
            {
                FieldName = section.GetString("name");
                ColumnName = section.GetString("column");
                ColumnAlias = section.GetString("alias", ColumnName);
                DataType = SQLDataType.Parse(section.GetString("type"));

                Length = section.GetInteger("length");
                Map = section.GetString("map");
                Access = section.GetString("access").ToFieldAccess();

                ContentType = section.GetString("cont").ToContentType();
                Table = Entity.Tables[section.GetString("from", Entity.Tables.RootTable.TableName)];

                Table.Fields.Add(FieldName, this);
            }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent entity for this field descriptor.</summary>
        public __EntityDescriptor Entity { get; private set; }


        /// <summary>Gets the database table that defines the field.</summary>
        public __TableDescriptor Table { get; private set; }
        

        /// <summary>Gets the field column name.</summary>
        public string FieldName { get; private set; }


        /// <summary>Gets the field length.</summary>
        public int Length { get; private set; }


        /// <summary>Gets the field access mode.</summary>
        public __FieldAccess Access { get; private set; }


        /// <summary>Gets the field mapping directive.</summary>
        public string Map { get; private set; }
        

        /// <summary>Gets the field data type.</summary>
        public SQLDataType DataType { get; private set; }


        /// <summary>Gets the field content type.</summary>
        public __ContentType ContentType { get; private set; }


        /// <summary>Gets the field bind variable name.</summary>
        public string BindVariableName
        {
            get { return Entity.Provider.Parser.ToBindVariableName(ColumnAlias.ToLower()); }
        }
        


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal methods                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Appends entity data to a ddp section.</summary>
        /// <param name="section">ddp section.</param>
        internal void __AppendDdp(DdpSection section)
        {
            DdpSection s = section.Sections.Add(FieldName);

            s.SetString("name", FieldName);
            s.SetString("column", ColumnName);
            s.SetString("alias", ColumnAlias);

            s.SetString("type", DataType.Constant);

            if(Length > 0) { s.SetInteger("length", Length); }
            if(!string.IsNullOrWhiteSpace(Map)) { s.SetString("map", Map); }

            s.SetString("cont", ContentType.ToConstant());
            s.SetString("from", Table.TableName);
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] ISQLField                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the column name.</summary>
        public string ColumnName { get; private set; }


        /// <summary>Gets the field alias.</summary>
        public string ColumnAlias { get; private set; }


        /// <summary>Gets the table alias.</summary>
        string ISQLColumn.TableAlias
        {
            get { return Table.TableAlias; }
        }

        /// <summary>Gets the column expression as required in a where clause.</summary>
        public string ColumnExpression
        {
            get
            {
                if(string.IsNullOrWhiteSpace(Table.TableAlias)) { return ColumnName; }

                return Table.TableAlias + "." + ColumnName;
            }
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IDescriptor                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the descriptor key.</summary>
        /// <returns>Key.</returns>
        string __IDescriptor.GetKey()
        {
            return FieldName;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            Ddp rval = new Ddp();
            __AppendDdp(rval);

            return rval.ToString();
        }
    }
}
