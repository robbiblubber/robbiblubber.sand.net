﻿using System;



namespace Robbiblubber.Sand.Security
{
    /// <summary>This class represents a privilege.</summary>
    public sealed class Privilege: SystemObject, ISystemObject
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets a privilege by its name.</summary>
        /// <param name="name">Privilege name.</param>
        /// <returns>Privilege.</returns>
        public static Privilege ByName(string name)
        {
            return Factory.Produce<Privilege>(m => (m.Name == name));
        }
    }
}
