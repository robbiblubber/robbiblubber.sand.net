﻿using System;

using Robbiblubber.Sand.Model.Base;



namespace Robbiblubber.Sand.Security
{
    /// <summary>This class implements a user object.</summary>
    public class User: SystemObject, IProducible
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public static methods                                                                                            //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets a privilege by its name.</summary>
        /// <param name="name">Privilege name.</param>
        /// <returns>Privilege.</returns>
        public static User ByName(string name)
        {
            return Factory.Produce<User>(m => (m.Name == name));
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets or sets the user's full name.</summary>
        public string FullName
        {
            get { return _GetValue<string>("FullName"); }
            set { _SetValue("FullName", value); }
        }


        /// <summary>Gets or sets the user's e-mail address.</summary>
        public string EMail
        {
            get { return _GetValue<string>("EMail"); }
            set { _SetValue("EMail", value); }
        }


        /// <summary>Gets or sets the user's OS domain name.</summary>
        public string OSDomain
        {
            get { return _GetValue<string>("OSDomain"); }
            set { _SetValue("OSDomain", value); }
        }


        /// <summary>Gets or sets the user's OS user name.</summary>
        public string OSUserName
        {
            get { return _GetValue<string>("OSUserName"); }
            set { _SetValue("OSUserName", value); }
        }


        /// <summary>Gets or sets if the user is active.</summary>
        public bool Active
        {
            get { return _GetValue<bool>("Active"); }
            set { _SetValue("Active", value); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Sets the user password.</summary>
        /// <param name="password">New password.</param>
        public void SetPassword(string password, Context context)
        {
            __Factory.Instance.SetPassword(this, password, context);
        }


        /// <summary>Sets the user password.</summary>
        /// <param name="password">New password.</param>
        public void SetPassword(string password)
        {
            SetPassword(password, Context.Current);
        }


        /// <summary>Unsets the user password.</summary>
        public void UnsetPassword(Context context)
        {
            __Factory.Instance.UnsetPassword(this, context);
        }


        /// <summary>Unsets the user password.</summary>
        public void SetPassword()
        {
            UnsetPassword(Context.Current);
        }


        /// <summary>Gets a user variable.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="name">Variable name.</param>
        /// <param name="context">Context.</param>
        /// <returns>Variable value.</returns>
        public T GetVariable<T>(string name, Context context)
        {
            return GetVariable<T>(Variable.ByName(name), context);
        }


        /// <summary>Gets a user variable.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="name">Variable name.</param>
        /// <param name="context">Context.</param>
        /// <returns>Variable value.</returns>
        public T GetVariable<T>(string name)
        {
            return GetVariable<T>(Variable.ByName(name), Context.Current);
        }


        /// <summary>Gets a user variable.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="name">Variable name.</param>
        /// <param name="context">Context.</param>
        /// <returns>Variable value.</returns>
        public T GetVariable<T>(Variable variable, Context context)
        {
            return __Factory.Instance.GetUserVariable<T>(this, variable, context);
        }


        /// <summary>Gets a user variable.</summary>
        /// <typeparam name="T">Type.</typeparam>
        /// <param name="name">Variable name.</param>
        /// <returns>Variable value.</returns>
        public T GetVariable<T>(Variable variable)
        {
            return __Factory.Instance.GetUserVariable<T>(this, variable, Context.Current);
        }


        /// <summary>Sets a user variable.</summary>
        /// <param name="name">Variable name.</param>
        /// <param name="value">Value.</param>
        /// <param name="context">Context.</param>
        public void SetVariable(string name, object value, Context context)
        {
            SetVariable(Variable.ByName(name), value, context);
        }


        /// <summary>Sets a user variable.</summary>
        /// <param name="name">Variable name.</param>
        /// <param name="value">Value.</param>
        public void SetVariable(string name, object value)
        {
            SetVariable(Variable.ByName(name), value, Context.Current);
        }


        /// <summary>Sets a user variable.</summary>
        /// <param name="variable">Variable.</param>
        /// <param name="value">Value.</param>
        /// <param name="context">Context.</param>
        public void SetVariable(Variable variable, object value, Context context)
        {
            __Factory.Instance.SetUserVariable(this, variable, value, context);
        }


        /// <summary>Sets a user variable.</summary>
        /// <param name="variable">Variable.</param>
        /// <param name="value">Value.</param>
        public void SetVariable(Variable variable, object value)
        {
            __Factory.Instance.SetUserVariable(this, variable, value, Context.Current);
        }
    }
}
