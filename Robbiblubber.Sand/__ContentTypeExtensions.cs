﻿using System;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements extension methods for content types.</summary>
    public static class __ContentTypeExtensions
    {
        /// <summary>Converts a content type to a string constant.</summary>
        /// <param name="c">Content type.</param>
        /// <returns>String.</returns>
        public static string ToConstant(this __ContentType c)
        {
            switch(c)
            {
                case __ContentType.ID: return "ID";
                case __ContentType.JCREATED: return "JCREATED";
                case __ContentType.JMODIFIED: return "JMODIFIED";
                case __ContentType.JTIME: return "JTIME";
                case __ContentType.DATA: return "DATA";
                case __ContentType.REF: return "REF";
            }

            return null;
        }


        /// <summary>Converts a string to a content type.</summary>
        /// <param name="s">String.</param>
        /// <returns>Content type.</returns>
        public static __ContentType ToContentType(this string s)
        {
            switch(s.ToUpper())
            {
                case "ID": return __ContentType.ID;
                case "JCREATED": return __ContentType.JCREATED;
                case "JMODIFIED": return __ContentType.JMODIFIED;
                case "JTIME": return __ContentType.JTIME;
                case "REF": return __ContentType.REF;
            }

            return __ContentType.DATA;
        }
    }
}
