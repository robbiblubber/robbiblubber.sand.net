﻿using System;

using Robbiblubber.Sand.Journalization;
using Robbiblubber.Sand.Security;



namespace Robbiblubber.Sand.Journalization
{
    /// <summary>This class represents a journal entry.</summary>
    public sealed class JournalEntry
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the primary object for this journal entry.</summary>
        public IProducible Object
        {
            get; internal set;
        }


        /// <summary>Gets the referenced object for this journal entry.</summary>
        public IProducible ReferencedObject
        {
            get; internal set;
        }


        /// <summary>Gets the severity of this journal entry.</summary>
        public Journal.Severity Severity
        {
            get; internal set;
        }


        /// <summary>Gets the operation of this journal entry.</summary>
        public Journal.Operation Operation
        {
            get; internal set;
        }


        /// <summary>Gets the flags for this journal entry.</summary>
        public int Flags
        {
            get; internal set;
        }


        /// <summary>Gets the reserved flags field for this journal entry.</summary>
        public int Reserved
        {
            get; internal set;
        }


        /// <summary>Gets the tags of this journal entry.</summary>
        public string Tags
        {
            get; internal set;
        }


        /// <summary>Gets the message text of this journal entry.</summary>
        public string Message
        {
            get; internal set;
        }


        /// <summary>Gets the data of this journal entry.</summary>
        public string Data
        {
            get; internal set;
        }


        /// <summary>Gets the additional information of this journal entry.</summary>
        public string AdditionalInformation
        {
            get; internal set;
        }


        /// <summary>Gets the session that created this journal entry.</summary>
        public ISession Session
        {
            get; internal set;
        }


        /// <summary>Gets the timestamp of this journal entry.</summary>
        public DateTime Time
        {
            get; internal set;
        }
    }
}
