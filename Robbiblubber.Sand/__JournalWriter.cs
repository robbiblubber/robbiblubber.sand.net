﻿using System;

using Robbiblubber.Sand.Model.Base;
using Robbiblubber.Sand.Security;



namespace Robbiblubber.Sand.Journalization.Base
{
    /// <summary>This class provides a basic journal writer.</summary>
    public class __JournalWriter: __IJournalWriter, __IAttachable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        protected __JournalWriter()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static __IJournalWriter Instance
        {
            get; protected set;
        } = _CreateInstance();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a journal writer instance.</summary>
        /// <returns>Journal writer.</returns>
        private static __IJournalWriter _CreateInstance()
        {
            __IJournalWriter rval = (__IJournalWriter) Activator.CreateInstance(Type.GetType(__Repository.Instance.RepositoryFile.Sections["journal"].GetString("class")), true);
            if(rval == null)
            {
                throw new TypeLoadException("Unable to load journal writer class \"" + __Repository.Instance.RepositoryFile.Sections["journal"].GetString("class") + "\".");
            }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IAttachable                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Attaches the object to the system.</summary>
        public virtual void Attach()
        {}


        /// <summary>Detaches the object from the system.</summary>
        public virtual void Detach()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IAttachable                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Writes a journal entry.</summary>
        /// <param name="obj">Object.</param>
        /// <param name="referenced">Referenced object.</param>
        /// <param name="severity">Severity level.</param>
        /// <param name="operation">Operation flag.</param>
        /// <param name="flags">Additional flags.</param>
        /// <param name="reserved">Reserved flags.</param>
        /// <param name="message">Journal entry message.</param>
        /// <param name="tags">Journal entry tags.</param>
        /// <param name="data">Journal entry data.</param>
        /// <param name="additional">Additional data.</param>
        /// <param name="session">Session.</param>
        public virtual void Write(IProducible obj, IProducible referenced, Journal.Severity severity, Journal.Operation operation, int flags, int reserved, string message, string tags, string data, string additional, ISession session)
        {
            __Factory.Instance.WriteEntry(obj, referenced, severity, operation, flags, reserved, message, tags, data, additional, session);
        }
    }
}
