﻿using System;
using System.Collections.Generic;

using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements a table descriptor list.</summary>
    public sealed class __TableDescriptorList: __DescriptorList<__TableDescriptor>, IEnumerable<__TableDescriptor>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the primary table.</summary>
        public __TableDescriptor RootTable
        {
            get; internal set;
        }


        /// <summary>Gets a list of joint tables.</summary>
        public __TableDescriptorList JointTables
        {
            get; internal set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Joins all joint tables in an inner join.</summary>
        /// <param name="query">SQL query.</param>
        public void InnerJoin(ISQLQuery query)
        {
            foreach(__TableDescriptor i in JointTables) { query.Join(i, i.JoinCondition); }
        }


        /// <summary>Joins all joint tables in an outer join.</summary>
        /// <param name="query">SQL query.</param>
        public void OuterJoin(ISQLQuery query)
        {
            foreach(__TableDescriptor i in JointTables) { query.LeftOuterJoin(i, i.JoinCondition); }
        }
    }
}
