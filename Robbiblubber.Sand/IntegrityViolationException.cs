﻿using System;



namespace Robbiblubber.Sand.Exceptions
{
    /// <summary>This class implements an exception that occurs when an operation violates database integrity constraints.</summary>
    public class IntegrityViolationException: Exception
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        public IntegrityViolationException() : base()
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        public IntegrityViolationException(string message) : base(message)
        {}


        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="message">Exception message.</param>
        /// <param name="innerException">The exception that caused the exception.</param>
        public IntegrityViolationException(string message, Exception innerException) : base(message, innerException)
        {}
    }
}
