﻿using System;

using Robbiblubber.Sand.Model;
using Robbiblubber.Sand.Exceptions;
using Robbiblubber.Sand.Security;
using Robbiblubber.Sand.Security.Exceptions;
using Robbiblubber.Sand.Journalization;



namespace Robbiblubber.Sand
{
    /// <summary>Objects produced by factories implement this interface.</summary>
    public interface IProducible: IAtom, IRefreshable, ISynchronizable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the operations for this object.</summary>
        Operations Ops { get; }


        /// <summary>Gets the inline journal for this object.</summary>
        InlineJournal InlineJournal { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // methods                                                                                                          //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Locks the object for a given context.</summary>
        /// <param name="context">Context.</param>
        /// <exception cref="ObjectLockedException">Thrown when the object is already locked.</exception>
        void Lock(Context context);


        /// <summary>Locks the object.</summary>
        /// <exception cref="ObjectLockedException">Thrown when the object is already locked.</exception>
        void Lock();


        /// <summary>Returns the lock object for this instance.</summary>
        Lock GetLock();


        /// <summary>Saves the object.</summary>
        /// <param name="context">Context.</param>
        /// <exception cref="InsufficientPrivilegesException">Thrown if the context lacks privileges for the desired operation.</exception>
        void Save(Context context);


        /// <summary>Saves the object.</summary>
        /// <exception cref="InsufficientPrivilegesException">Thrown if the context lacks privileges for the desired operation.</exception>
        void Save();


        /// <summary>Creates a clone of the object.</summary>
        /// <returns>Cloned object.</returns>
        IProducible Clone();


        /// <summary>Deletes the object.</summary>
        /// <param name="context">Context.</param>
        void Delete(Context context);


        /// <summary>Deletes the object.</summary>
        void Delete();
    }
}
