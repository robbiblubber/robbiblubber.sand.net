﻿using System;
using System.Collections.Generic;



namespace Robbiblubber.Sand.Model
{
    /// <summary>Cache implementations must implement this interface.</summary>
    public interface ICache: IEnumerable<IProducible>
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // properties                                                                                                       //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Gets the element with the given ID.</summary>
        /// <param name="ID">ID.</param>
        /// <returns>Returns an element if contained, otherwise returns NULL.</returns>
        IProducible this[string id] { get; }


        /// <summary>Gets the current cache size.</summary>
        int Size { get; }


        /// <summary>Gets the maximum cache size.</summary>
        int MaxSize { get; }


        /// <summary>Gets the timeout for object changes in minutes.</summary>
        int ChangeTimeout { get; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // method                                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the element with the given ID.</summary>
        /// <param name="id">ID.</param>
        /// <returns>Returns an element if contained, otherwise returns NULL.</returns>
        IProducible Retrieve(string id);


        /// <summary>Adds an element to the cache.</summary>
        /// <param name="p">Element.</param>
        /// <remarks>Returns the added element.</remarks>
        IProducible Add(IProducible p);


        /// <summary>Removes an element from the cache.</summary>
        /// <param name="p">Element.</param>
        void Remove(IProducible p);


        /// <summary>Removes an element from the cache.</summary>
        /// <param name="id">Element ID.</param>
        void Remove(string id);


        /// <summary>Clears the cache and removes all elements.</summary>
        void Clear();


        /// <summary>Reduces the size of the cache below <see cref="MaxSize"/>.</summary>
        void Purge();


        /// <summary>Refreshes the cache.</summary>
        void Refresh();
    }
}
