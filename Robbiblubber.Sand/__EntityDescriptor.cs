﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Robbiblubber.Sand.Security;
using Robbiblubber.Util.Library;
using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements an entity descriptor that holds metadata for an entity.</summary>
    public sealed class __EntityDescriptor: __IDescriptor
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Expression parser.</summary>
        private __ExpressionParser _ExpressionParser = null;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="sec">ddp section.</param>
        internal __EntityDescriptor(DdpSection sec)
        {
            EntityID = sec.Name;

            EntityName = sec.GetString("name");
            EntityQualifiedName = sec.GetString("qualified");
            SequenceName = sec.GetString("sequence");
            WhereClause = sec.GetString("where");

            EntityType = ClassOp.LoadType(EntityQualifiedName, false);
            ClassType = sec.GetString("type", "LEAF").ToClassType();
            Children = sec.GetStringArray("children");
            IgnoreWhere = sec.GetBoolean("ignorewhere");

            OpDescriptor = new __OpDecriptor(this);

            string[] tables = sec.GetStringArray("tables", "NULL");

            List<string> defaultAliases = new List<string>();
            if(tables.Length > 0)
            {
                defaultAliases.Add(tables[0].Substring(0, 1).ToUpper());
            }
            else { defaultAliases.Add("Q"); }

            if(defaultAliases[0] != "R") { defaultAliases.Add("R"); }
            if(defaultAliases[0] != "S") { defaultAliases.Add("S"); }
            if(defaultAliases[0] != "T") { defaultAliases.Add("T"); }
            if(defaultAliases[0] != "U") { defaultAliases.Add("U"); }
            if(defaultAliases[0] != "V") { defaultAliases.Add("V"); }
            if(defaultAliases[0] != "W") { defaultAliases.Add("W"); }
            if(defaultAliases[0] != "Y") { defaultAliases.Add("Y"); }
            if(defaultAliases[0] != "Z") { defaultAliases.Add("Z"); }

            string[] aliases = sec.GetStringArray("aliases", defaultAliases.ToArray());
            string[] pks = sec.GetStringArray("pks", "ID");

            Tables = new __TableDescriptorList();
            Tables.JointTables = new __TableDescriptorList();
            Tables.JointTables.JointTables = Tables.JointTables;

            for(int i = 0; i < tables.Length; i++)
            {
                __TableDescriptor tab = new __TableDescriptor(this, tables[i], aliases[i], pks[i], (i == 0));
                Tables.Add(tab);

                if(i == 0)
                {
                    Tables.RootTable = tab;
                }
                else { Tables.JointTables.Add(tab); }
            }

            Fields = new __FieldDescriptorList();
            Relations = new __RelationDescriptorList();

            foreach(DdpSection i in sec.Sections)
            {
                switch(i.GetString("cont"))
                {
                    case "ID":
                    case "DATA":
                    case "REF":
                    case "JCREATED":
                    case "JMODIFIED":
                    case "JTIME":
                        Fields.Add(new __FieldDescriptor(this, i)); break;
                    case "REL":
                        Relations.Add(new __1ToNRelationDescriptor(this, i)); break;
                    case "RELX":
                        Relations.Add(new __MToNRelationDescriptor(this, i)); break;
                }
            }

            Provider = __Factory.Instance.Providers[sec.GetString("provider")];
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the entity ID.</summary>
        public string EntityID { get; private set; }


        /// <summary>Gets the entity name.</summary>
        public string EntityName { get; private set; }


        /// <summary>Gets the entity qualified name.</summary>
        public string EntityQualifiedName { get; private set; }


        /// <summary>Gets the entity type.</summary>
        public Type EntityType { get; private set; }


        /// <summary>Gets the entity class type.</summary>
        public __ClassType ClassType { get; private set; }


        /// <summary>Gets the child class IDs for this entity.</summary>
        public string[] Children { get; private set; }


        /// <summary>Gets if child entity where clauses should be ignored.</summary>
        public bool IgnoreWhere { get; private set; }


        /// <summary>Gets the entity sequence name.</summary>
        public string SequenceName { get; private set; }


        /// <summary>Gets the entity specific where clause.</summary>
        public string WhereClause { get; private set; }


        /// <summary>Gets the entity field descriptors.</summary>
        public __FieldDescriptorList Fields { get; private set; }


        /// <summary>Gets the entity relation descriptors.</summary>
        public __RelationDescriptorList Relations { get; private set; }


        /// <summary>Gets the entity table descriptors.</summary>
        public __TableDescriptorList Tables { get; private set; }


        /// <summary>Gets the entity SQL provider.</summary>
        public IProvider Provider { get; private set; }


        /// <summary>Gets an expression parser for this entity.</summary>
        public __ExpressionParser ExpressionParser
        {
            get
            {
                if(_ExpressionParser == null) { _ExpressionParser = new __ExpressionParser(this); }

                return _ExpressionParser;
            }
        }


        /// <summary>Gets the operations descriptor for this entity.</summary>
        public __OpDecriptor OpDescriptor { get; private set; }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a ddp representation of this instance.</summary>
        /// <returns>ddp data.</returns>
        public Ddp ToDdp()
        {
            Ddp rval = new Ddp();
            __AppendDdp(rval);

            return rval;
        }


        /// <summary>Spawns a new instance of the entity type.</summary>
        /// <returns>Instance.</returns>
        public IProducible Spawn()
        {
            if(EntityID == "0") { throw new InvalidOperationException("Unable to create instances of session class."); }

            return (IProducible) Activator.CreateInstance(EntityType);
        }


        /// <summary>Spawns a new instance of the entity type with database data.</summary>
        /// <param name="re">Database reader</param>
        /// <returns>Instance.</returns>
        public IProducible Spawn(IDataReader re)
        {
            if(EntityID == "0") { return _SpawnSession(re); }

            IProducible rval = null;
            if(ClassType == __ClassType.LEAF)
            {
                rval = (IProducible) Activator.CreateInstance(EntityType);
            }
            else
            {
                rval = (IProducible) Activator.CreateInstance(__Factory.Instance.Entities[re.GetString(Fields.IDField.ColumnAlias)].EntityType);
            }

            ((__IFactoryTarget) rval).Fields.Read(re);

            return rval;
        }


        /// <summary>Creates a base query for this entity.</summary>
        /// <returns>SQL query.</returns>
        public ISQLQuery CreateQuery()
        {
            ISQLQuery rval = null;

            if(EntityID == "0")
            {
                SQLTable tab = new SQLTable();
                tab.TableName = "(" + Provider.Parser.CreateQuery().From(new SQLTable(Tables[0].TableName)).Select(new SQLField("*")).UnionAll(
                                      Provider.Parser.CreateQuery().From(new SQLTable(Tables[1].TableName)).Select(new SQLField("*"))).Text + ")";
                tab.TableAlias = Tables[0].TableAlias;

                rval = Provider.Parser.CreateQuery().From(tab).Select(Fields.Array);
            }
            else
            {
                bool first = true;
                __EntityDescriptor entity;
                Dictionary<string, __FieldDescriptor> fields;

                switch(ClassType)
                {
                    case __ClassType.LEAF:
                        rval = Provider.Parser.CreateQuery().From(Tables.RootTable);
                        Tables.InnerJoin(rval);
                        rval.Where(WhereClause).Select(Fields.Array);
                        break;
                    case __ClassType.MATERIAL:
                        fields = new Dictionary<string, __FieldDescriptor>();

                        foreach(string i in Children)
                        {
                            entity = __Factory.Instance.Entities[i];
                            if(first)
                            {
                                rval = entity.Provider.Parser.CreateQuery().From(entity.Tables.RootTable);
                                first = false;
                            }

                            entity.Tables.OuterJoin(rval);
                            if(!IgnoreWhere) { rval.Where(entity.WhereClause); }

                            foreach(__FieldDescriptor j in entity.Fields)
                            {
                                if(!fields.ContainsKey(j.FieldName)) { fields.Add(j.FieldName, j); }
                            }
                        }

                        rval.Select(fields.Values.ToArray());
                        break;
                    case __ClassType.VIRTUAL:
                        fields = new Dictionary<string, __FieldDescriptor>();
                        List<__EntityDescriptor> entities = new List<__EntityDescriptor>();

                        foreach(string i in Children)
                        {
                            entity = __Factory.Instance.Entities[i];
                            entities.Add(entity);

                            foreach(__FieldDescriptor j in entity.Fields)
                            {
                                if(!fields.ContainsKey(j.FieldName)) { fields.Add(j.FieldName, j); }
                            }
                        }

                        foreach(__EntityDescriptor i in entities)
                        {
                            ISQLQuery q = i.Provider.Parser.CreateQuery().From(i.Tables.RootTable);
                            i.Tables.InnerJoin(q);
                            if(!IgnoreWhere) { q.Where(i.WhereClause); }

                            foreach(__FieldDescriptor j in fields.Values)
                            {
                                if(i.Fields.Contains(j.FieldName))
                                {
                                    q.Select(i.Fields[j.FieldName]);
                                }
                                else
                                {
                                    q.Select(new SQLField(null, "NULL", j.ColumnAlias));
                                }
                            }

                            if(first)
                            {
                                rval = q;
                                first = false;
                            }
                            else
                            {
                                rval.UnionAll(q);
                            }
                        }

                        break;
                    case __ClassType.INTEGRATED:
                        fields = new Dictionary<string, __FieldDescriptor>();

                        foreach(string i in Children)
                        {
                            entity = __Factory.Instance.Entities[i];

                            if(first)
                            {
                                rval = entity.Provider.Parser.CreateQuery().From(entity.Tables.RootTable);
                                first = false;
                            }

                            foreach(__FieldDescriptor j in entity.Fields)
                            {
                                if(!fields.ContainsKey(j.FieldName)) { fields.Add(j.FieldName, j); }
                            }
                        }

                        foreach(__FieldDescriptor i in fields.Values)
                        {
                            rval.Select(i);
                        }

                        break;
                }
            }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Spawns a new session instance with database data.</summary>
        /// <param name="re">Database reader</param>
        /// <param name="session">Session.</param>
        /// <returns>Instance.</returns>
        private IProducible _SpawnSession(IDataReader re)
        {
            Session rval = new Session();

            rval.Token = re.GetString(Fields["Token"].ColumnAlias);
            rval.Active = re.GetBool(Fields["Active"].ColumnAlias);
            rval.UserName = re.GetString(Fields["UserName"].ColumnAlias);
            rval.Data = re.GetString(Fields["Data"].ColumnAlias);
            rval.User = Factory.Produce<User>(re.GetString(Fields["User"].ColumnAlias));
            rval.StartTime = re.GetTimestamp(Fields["StartTime"].ColumnAlias);
            rval.EndTime = re.GetTimestamp(Fields["EndTime"].ColumnAlias, DateTime.MaxValue);

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // internal methods                                                                                                 //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Appends entity data to a ddp section.</summary>
        /// <param name="section">ddp section.</param>
        internal void __AppendDdp(DdpSection section)
        {
            DdpSection s = section.Sections.Add(EntityID);

            s.SetString("name", EntityName);
            s.SetString("qualified", EntityQualifiedName);
            s.SetString("sequence", SequenceName);
            s.SetString("where", WhereClause);

            List<string> tables = new List<string>();
            List<string> pks = new List<string>();
            List<string> aliases = new List<string>();

            if(Tables.Count > 1)
            {
                foreach(__TableDescriptor i in Tables)
                {
                    tables.Add(i.TableName);
                    pks.Add(i.PrimaryKey);
                    aliases.Add(i.TableAlias);
                }

                s.SetStringArray("tables", tables.ToArray());
                s.SetStringArray("pks", pks.ToArray());
                s.SetStringArray("aliases", aliases.ToArray());
            }

            foreach(__FieldDescriptor i in Fields) { i.__AppendDdp(s); }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IDescriptor                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the descriptor key.</summary>
        /// <returns>Key.</returns>
        string __IDescriptor.GetKey()
        {
            return EntityID;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [override] object                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns a string representation of this instance.</summary>
        /// <returns>String.</returns>
        public override string ToString()
        {
            return ToDdp().ToString();
        }
    }
}
