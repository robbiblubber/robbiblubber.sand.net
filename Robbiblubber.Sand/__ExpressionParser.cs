﻿using System;
using System.Collections;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

using Robbiblubber.Util.SQL.Core;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class provides expression parsing.</summary>
    public sealed class __ExpressionParser
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a new instance of this class.</summary>
        /// <param name="entity">Parent entity descriptor.</param>
        public __ExpressionParser(__EntityDescriptor entity)
        {
            Entity = entity;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public properties                                                                                                //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Gets the parent entity descriptor.</summary>
        public __EntityDescriptor Entity
        {
            get; private set;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // public methods                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Parses an expression to an SQL query.</summary>
        /// <param name="expression">Expression.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery Parse(Expression expression)
        {
            return Parse(Entity.CreateQuery(), expression);
        }


        /// <summary>Parses an expression to an SQL query.</summary>
        /// <param name="query">SQL query.</param>
        /// <param name="expression">Expression.</param>
        /// <returns>SQL query.</returns>
        public ISQLQuery Parse(ISQLQuery query, Expression expression)
        {
            while(!(expression is LambdaExpression))
            {
                if(expression is MethodCallExpression)
                {
                    expression = ((MethodCallExpression) expression).Arguments[1];
                }
                else if(expression is InvocationExpression)
                {
                    expression = ((InvocationExpression) expression).Expression;
                }
                else { expression = ((UnaryExpression) expression).Operand; }
            }

            int n = 0; bool strf = false;
            return query.And(_Parse(((LambdaExpression) expression).Body, query, ref n, true, ref strf));
        }


        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private methods                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Returns an SQL operator for an expression.</summary>
        /// <param name="type">Expression type.</param>
        /// <param name="isNull">Determines if the operands are of string type.</param>
        /// <param name="isNull">Determines if the right hand operand is NULL.</param>
        /// <returns>Operator string.</returns>
        private string _ToOperator(ExpressionType type, bool isString, bool isNull)
        {
            switch(type)
            {
                case ExpressionType.Add:
                    return isString ? Entity.Provider.Parser.ConcatOperator : Entity.Provider.Parser.AddOperator;
                case ExpressionType.And:
                    return Entity.Provider.Parser.BinaryAndOperator;
                case ExpressionType.AndAlso:
                    return Entity.Provider.Parser.AndOperator;
                case ExpressionType.Divide:
                    return Entity.Provider.Parser.DivideOperator;
                case ExpressionType.Equal:
                    return isNull ? Entity.Provider.Parser.EqualNullOperator : Entity.Provider.Parser.EqualOperator;
                case ExpressionType.ExclusiveOr:
                    return Entity.Provider.Parser.ExOrOperator;
                case ExpressionType.GreaterThan:
                    return Entity.Provider.Parser.GreaterOperator;
                case ExpressionType.GreaterThanOrEqual:
                    return Entity.Provider.Parser.GreaterOrEqualOperator;
                case ExpressionType.LessThan:
                    return Entity.Provider.Parser.LessOperator;
                case ExpressionType.LessThanOrEqual:
                    return Entity.Provider.Parser.LessOrEqualOperator;
                case ExpressionType.Modulo:
                    return Entity.Provider.Parser.ModuloOperator;
                case ExpressionType.Multiply:
                    return Entity.Provider.Parser.MultiplyOperator;
                case ExpressionType.Negate:
                    return Entity.Provider.Parser.NegateOperator;
                case ExpressionType.Not:
                    return Entity.Provider.Parser.NotOperator;
                case ExpressionType.NotEqual:
                    return isNull ? Entity.Provider.Parser.NotEqualNullOperator : Entity.Provider.Parser.NotEqualOperator;
                case ExpressionType.Or:
                    return Entity.Provider.Parser.BinaryOrOperator;
                case ExpressionType.OrElse:
                    return Entity.Provider.Parser.OrOperator;
                case ExpressionType.Subtract:
                    return Entity.Provider.Parser.SubtractOperator;
            }

            throw new ArgumentException("Expression of type \"" + type.ToString() + "\" could not be resolved.");
        }


        /// <summary>Evaluates the value of an expression.</summary>
        /// <param name="exp">Expression</param>
        /// <returns>Expression value.</returns>
        private static object _GetValue(Expression exp)
        {
            return Expression.Lambda<Func<object>>(Expression.Convert(exp, typeof(object))).Compile()();
        }


        /// <summary>Returns a bind variable.</summary>
        /// <param name="n">Variable index.</param>
        /// <returns>Bind variable name.</returns>
        private string _ToVar(int n)
        {
            return Entity.Provider.Parser.ToBindVariableName("p" + n.ToString());
        }


        /// <summary>Parses an expression.</summary>
        /// <param name="exp">Expression.</param>
        /// <param name="query">SQL query.</param>
        /// <param name="n">Variable index.</param>
        /// <param name="isUnary">Determines if the evaluated expression is the argument of an unary expression.</param>
        /// <param name="isString">String operation flag.</param>
        /// <returns>Where clause part.</returns>
        private string _Parse(Expression exp, ISQLQuery query, ref int n, bool isUnary, ref bool isString)
        {
            bool strf = false;

            if(exp is UnaryExpression)
            {
                if(((UnaryExpression) exp).NodeType == ExpressionType.Convert)
                {
                    return _Parse(((UnaryExpression) exp).Operand, query, ref n, true, ref strf);
                }

                string r = _Parse(((UnaryExpression) exp).Operand, query, ref n, true, ref strf);
                return "(" + _ToOperator(((UnaryExpression) exp).NodeType, false, r == "NULL") + " " + r + ")";
            }

            if(exp is BinaryExpression)
            {
                string r = _Parse(((BinaryExpression) exp).Right, query, ref n, false, ref strf);
                return "(" + _Parse(((BinaryExpression) exp).Left, query, ref n, false, ref strf) + " " + _ToOperator(((BinaryExpression) exp).NodeType, strf, r == "NULL") + " " + r + ")";
            }

            if(exp is ConstantExpression)
            {
                query.AddParameter(_ToVar(n), ((ConstantExpression) exp).Value);
                if(((ConstantExpression) exp).Value is string) { isString = true; }

                return _ToVar(n++);
            }

            if(exp is MemberExpression)
            {
                if(((MemberExpression) exp).Member is PropertyInfo)
                {
                    if(isUnary && ((MemberExpression) exp).Type == typeof(bool))
                    {
                        if(Entity.Fields[((PropertyInfo) ((MemberExpression) exp).Member).Name].DataType == SQLDataType.BOOLEAN_INTEGER)
                        {
                            return "(" + Entity.Fields[((PropertyInfo) ((MemberExpression) exp).Member).Name].ColumnExpression + Entity.Provider.Parser.NotEqualOperator + "0)";
                        }
                        else
                        {
                            query.AddParameter(_ToVar(n), true);
                            return "(" + Entity.Fields[((PropertyInfo) ((MemberExpression) exp).Member).Name].ColumnExpression + Entity.Provider.Parser.EqualOperator + _ToVar(n++) + ")";
                        }                        
                    }

                    if(((MemberExpression) exp).Type == typeof(string)) { isString = true; }

                    return Entity.Fields[((PropertyInfo) ((MemberExpression) exp).Member).Name].ColumnExpression;
                }

                if(((MemberExpression) exp).Member is FieldInfo)
                {
                    object v = _GetValue(exp);
                    if(v is string) { isString = true; }

                    query.AddParameter("p" + n.ToString(), v);
                    return _ToVar(n++);
                }

                throw new ArgumentException("Expression \"" + exp + "\" could not be parsed.");
            }

            if(exp is NewExpression)
            {
                object v = _GetValue(exp);
                if(v is string) { isString = true; }

                query.AddParameter("p" + n.ToString(), v);
                return _ToVar(n++);
            }

            if(exp is MethodCallExpression)
            {
                if(((MethodCallExpression) exp).Method == typeof(string).GetMethod("ToLower", new Type[0]))
                {
                    return Entity.Provider.Parser.Lowercase(_Parse(((MethodCallExpression) exp).Object, query, ref n, false, ref strf));
                }

                if(((MethodCallExpression) exp).Method == typeof(string).GetMethod("ToUpper", new Type[0]))
                {
                    return Entity.Provider.Parser.Uppercase(_Parse(((MethodCallExpression) exp).Object, query, ref n, false, ref strf));
                }

                if(((MethodCallExpression) exp).Method == typeof(string).GetMethod("Contains", new Type[] { typeof(string) }))
                {
                    query.AddParameter(_ToVar(n), "%" + _GetValue(((MethodCallExpression) exp).Arguments[0]).ToString() + "%");
                    return "(" + _Parse(((MethodCallExpression) exp).Object, query, ref n, false, ref strf) + " LIKE " + _ToVar(n++) + ")";
                }

                if(((MethodCallExpression) exp).Method == typeof(string).GetMethod("StartsWith", new[] { typeof(string) }))
                {
                    query.AddParameter(_ToVar(n), _GetValue(((MethodCallExpression) exp).Arguments[0]).ToString() + "%");
                    return "(" + _Parse(((MethodCallExpression) exp).Object, query, ref n, false, ref strf) + " LIKE " + _ToVar(n++) + ")";
                }

                if(((MethodCallExpression) exp).Method == typeof(string).GetMethod("EndsWith", new[] { typeof(string) }))
                {
                    query.AddParameter(_ToVar(n), "%" + _GetValue(((MethodCallExpression) exp).Arguments[0]).ToString());
                    return "(" + _Parse(((MethodCallExpression) exp).Object, query, ref n, false, ref strf) + " LIKE " + _ToVar(n++) + ")";
                }
                
                if(((MethodCallExpression) exp).Method.Name == "Contains")
                {
                    Expression col, prop;

                    if(((MethodCallExpression) exp).Method.IsDefined(typeof(ExtensionAttribute)) && ((MethodCallExpression) exp).Arguments.Count == 2)
                    {
                        col = ((MethodCallExpression) exp).Arguments[0];
                        prop = ((MethodCallExpression) exp).Arguments[1];
                    }
                    else if(!((MethodCallExpression) exp).Method.IsDefined(typeof(ExtensionAttribute)) && ((MethodCallExpression) exp).Arguments.Count == 1)
                    {
                        col = ((MethodCallExpression) exp).Object;
                        prop = ((MethodCallExpression) exp).Arguments[0];
                    }
                    else { throw new ArgumentException("Method call \"" + ((MethodCallExpression) exp).Method.Name + "\" could not be resolved."); }

                    IEnumerable values = (IEnumerable) _GetValue(col);
                    string list = "";
                    bool first = true;
                    
                    foreach(object i in values)
                    {
                        if(first) { first = false; } else { list += ", "; }

                        query.AddParameter(_ToVar(n), i);
                        list += _ToVar(n++);
                    }

                    return "(" + _Parse(prop, query, ref n, false, ref strf) + " IN (" + list + "))";
                }

                throw new ArgumentException("Method call: \"" + ((MethodCallExpression) exp).Method.Name + "\" could not be resolved.");
            }

            throw new ArgumentException("Expression of type \"" + exp.GetType().Name + "\" could not be resolved.");
        }
    }
}
