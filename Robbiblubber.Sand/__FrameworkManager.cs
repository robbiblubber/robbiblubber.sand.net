﻿using System;
using System.Timers;



namespace Robbiblubber.Sand.Model.Base
{
    /// <summary>This class implements the standard framework manager.</summary>
    public class __FrameworkManager: __IFrameworkManager, __IAttachable
    {
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private members                                                                                                  //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Timer.</summary>
        private Timer _T;



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /// <summary>Creates a new instance of this class.</summary>
        private __FrameworkManager()
        {}



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // constructors                                                                                                     //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static __IFrameworkManager Instance
        {
            get; protected set;
        } = _CreateInstance();



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // private static methods                                                                                           //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Creates a factory instance.</summary>
        /// <returns>Instance.</returns>
        private static __IFrameworkManager _CreateInstance()
        {
            __IFrameworkManager rval = (__IFrameworkManager) Activator.CreateInstance(Type.GetType(__Repository.Instance.RepositoryFile.Sections["framework"].GetString("class")), true);
            if(rval == null)
            {
                throw new TypeLoadException("Unable to load framework manager class \"" + __Repository.Instance.RepositoryFile.Sections["journal"].GetString("class") + "\".");
            }

            return rval;
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // event handlers                                                                                                   //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Handles the timer event and performs system maintenance tasks.</summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void _DoTasks(object sender, ElapsedEventArgs e)
        {
            _T.Enabled = false;

            __Factory.Instance.PurgeSessions();

            if(sender != null) { _T.Enabled = true; }
        }



        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // [interface] __IAttachable                                                                                        //
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>Attaches the object to the system.</summary>
        public void Attach()
        {
            if(_T != null) return;

            _T = new Timer(__Repository.Instance.RepositoryFile.Sections["framework"].GetInteger("interval") * 1000);
            _T.AutoReset = true;
            _T.Elapsed += _DoTasks;
            _T.Start();
        }


        /// <summary>Detaches the object from the system.</summary>
        public void Detach()
        {
            _T.Elapsed -= _DoTasks;
            _DoTasks(null, null);
            _T.Stop();

            _T.Dispose();
            _T = null;
        }
    }
}
